import axios from "axios";
import {addToLocalStorage, getSValue, removeFromLocalStorage} from "../helpers/localhost";
import qs from 'qs';

function setHeadersToken() {
    axios.defaults.headers.common['token'] = getSValue('token') || null;
}
setHeadersToken();

// asynchronous action creator
export const loginHandler = (payload) => {

    payload.setSubmitting(true);

    return (dispatch) => {

        return axios({
            method: 'post',
            url: payload.url,
            data: payload.values,
            responseType: "json",
        }).then(response => {

            payload.setSubmitting(false);

            if(!response.data.Success) {
                payload.setErrors({username: response.data.Message});
                return;
            }
            addToLocalStorage('token', response.data.Data.Token);

            setHeadersToken();

            return dispatch({
                type: "LOGIN",
                data: response.data
            });

        }).catch(error => {
            payload.setSubmitting(false);
            return dispatch({
                type: 'ERROR',
                payload: error,
            });
        });
    }

};
// asynchronous action creator
export const logoutHandler = (payload) => {

    return (dispatch) => {

        return axios({
            method: 'get',
            url: payload.url,
            responseType: "json",
        }).then(response => {

            removeFromLocalStorage('token');

            return dispatch({
                type: "LOGOUT",
                data: response.data
            });

        }).catch(error => {
            return dispatch({
                type: 'ERROR',
                payload: error,
            });
        });
    }

};

export const getIndividualCustomer = (url) => {

    return (dispatch) => {

        return axios({
            method: 'get',
            url: url,
            responseType: 'json',
        }).then(response => {
            return dispatch({
                type: 'INDIVIDUAL_CLIENT_DATA',
                data: response.data
            });
        }).catch(async error => {

            return dispatch({
                type: 'ERROR',
                payload: {
                    error,
                    redirect: '/',
                    authenticated: false
                },
            });
        });
    }
};

export const getClients = (obj) => {

    return (dispatch) => {
        dispatch(fetchingData());

        return axios({
            method: 'get',
            url: obj.url,
            responseType: 'json',
            params: obj.formValues,
            paramsSerializer: (params => qs.stringify(params, {arrayFormat: 'repeat'}))
        }).then(response => {
            return dispatch({
                type: 'CLIENTS_DATA',
                data: response.data
            });
        }).catch(async error => {

            return dispatch({
                type: 'ERROR',
                payload: {
                    error,
                    redirect: '/',
                    authenticated: false
                },
            });
        });
    }
};

export const getContractAccounts = (obj) => {

    return (dispatch) => {
        dispatch(fetchingContractAccountsData());

        return axios({
            method: 'get',
            url: obj.url,
            responseType: 'json',
            params: obj.formValues,
            paramsSerializer: (params => qs.stringify(params, {arrayFormat: 'repeat'}))
        }).then(response => {
            return dispatch({
                type: 'CONTRACT_ACCOUNTS_DATA',
                data: response.data
            });
        }).catch(async error => {

            return dispatch({
                type: 'ERROR',
                payload: {
                    error,
                    redirect: '/',
                    authenticated: false
                },
            });
        });
    }
};

export const getClassifications = (url) => {

    return (dispatch) => {
        return axios({
            method: 'get',
            url: url,
            responseType: 'json',
        }).then(response => {
            return dispatch({
                type: 'CLASSIFICATIONS',
                data: response.data,
            });
        }).catch(error => {
            return dispatch({
                type: 'ERROR',
                payload: error,
            });
        });
    }
};

export const getEditableFields = (url) => {

    return (dispatch) => {
        return axios({
            method: 'get',
            url: url,
            responseType: 'json',
        }).then(response => {
            return dispatch({
                type: 'EDITABLE_FIELDS',
                data: response.data,
            });
        }).catch(error => {
            return dispatch({
                type: 'ERROR',
                payload: error,
            });
        });
    }
};

export const getApplications = (obj) => {

    return (dispatch) => {

        dispatch(fetchingData());

        return axios({
            method: 'get',
            url: obj.url,
            responseType: 'json',
            params: obj.formValues,
            paramsSerializer: (params => qs.stringify(params, {arrayFormat: 'repeat'}))
        }).then(response => {
            return dispatch({
                type: 'APPLICATIONS_FETCHED',
                data: response.data,
            });
        }).catch(error => {
            return dispatch({
                type: 'ERROR',
                payload: error,
            });
        });
    }
};

export const getNewCustomers = (obj) => {

    return (dispatch) => {

        dispatch(fetchingData());

        return axios({
            method: 'get',
            url: obj.url,
            responseType: 'json',
            params: obj.formValues,
            paramsSerializer: (params => qs.stringify(params, {arrayFormat: 'repeat'})),
        }).then(response => {
            return dispatch({
                type: 'NEW_CUSTOMERS_FETCHED',
                data: response.data,
            });
        }).catch(error => {
            return dispatch({
                type: 'ERROR',
                payload: error,
            });
        });
    }
};

export const getIndividualApplicant = (url) => {
    fetchingData();

    return (dispatch) => {

        return axios({
            method: 'get',
            url: url,
            responseType: 'json',
        }).then(response => {
            return dispatch({
                type: 'INDIVIDUAL_APPLICANT_DATA',
                data: response.data,
            });
        }).catch(error => {
            return dispatch({
                type: 'ERROR',
                payload: error,
            });
        });
    }
};

export const fetchingContractAccountsData = () => {
    return {
        type: 'FETCHING_CONTRACT_ACCOUNTS_DATA',
    }
};

export const fetchingData = () => {
    return {
        type: 'FETCHING_DATA',
    }
};

export const fetchingEditableFields = () => {
    return {
        type: 'FETCHING_EDITABLE_FIELDS',
    }
};
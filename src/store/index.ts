import {applyMiddleware, compose, createStore} from "redux";
import reducer from "../reducers";
import thunk from "redux-thunk";

const index = createStore(
    reducer,
    compose(applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f));

export default index;
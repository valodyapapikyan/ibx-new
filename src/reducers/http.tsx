import transformData from "../components/brokerage/customers/profile/helpers/transform";
import {
    transformApplications,
    transformClients,
    transformContractAccounts,
    transformNewCustomers
} from "../helpers/table-transform";

let table;
let transformedData;

const httpRequests = (state = {rootUrl: 'https://brokerstest.ameriabank.am/v1/api'}, action) => {
    switch (action.type) {
        case "LOGIN":
            return {
                ...state,
                loginResponse: action.data,
                authenticated: true
            };
        case "LOGOUT":
            return {
                ...state,
                logoutResponse: action.data,
                authenticated: false
            };
        case "ERROR":
            return {
                ...state,
                error: action.payload
            };
        case "INDIVIDUAL_CLIENT_DATA":

            const data = action.data.Data;

            transformedData = {
                ...action.data,
                Data: transformData(data),
                initialData: data,
            };

            return {
                ...state,
                individualClientResponse: transformedData,
                clientFetched: true,
            };

        case "CLASSIFICATIONS":

            return {
                ...state,
                classifications: action.data
            };

        case "EDITABLE_FIELDS":

            return {
                ...state,
                editableFields: action.data,
                editableFieldsFetched: true,
            };

        case "FETCHING_DATA":

            return {
                ...state,
                fetched: false,
                clientFetched: false,
            };

        case "FETCHING_EDITABLE_FIELDS":

            return {
                ...state,
                editableFieldsFetched: false,
            };

        case "FETCHING_CONTRACT_ACCOUNTS_DATA":

            return {
                ...state,
                contractAccountsFetched: false,
            };

        case "APPLICATIONS_FETCHED":

            table = action.data.Data;

            transformedData = {
                ...action.data,
                Data: transformApplications(table),
            };

            return {
                ...state,
                fetchedData: transformedData,
                fetched: true,
            };

        case "NEW_CUSTOMERS_FETCHED":

            table = action.data.Data;

            transformedData = {
                ...action.data,
                Data: transformNewCustomers(table),
            };

            return {
                ...state,
                fetchedData: transformedData,
                fetched: true,
            };

        case "CLIENTS_DATA":

            table = action.data.Data;

            transformedData = {
                ...action.data,
                Data: transformClients(table),
            };

            return {
                ...state,
                fetchedData: transformedData,
                fetched: true,
            };

        case "CONTRACT_ACCOUNTS_DATA":

            table = action.data.Data;

            transformedData = {
                ...action.data,
                Data: transformContractAccounts(table),
            };

            return {
                ...state,
                fetchedData: transformedData,
                contractAccountsFetched: true,
            };

        case "INDIVIDUAL_APPLICANT_DATA":

            return {
                ...state,
                individualApplicant: action.data,
                fetched: true,
            };


        default:
            return state
    }
};

export default httpRequests;
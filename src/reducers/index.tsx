import { combineReducers } from 'redux'
import httpRequests from "./http";

const rootReducer = combineReducers({
  httpRequests,
});

export default rootReducer

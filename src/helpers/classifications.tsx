import React from "react";
import { connect } from 'react-redux';

export default function (Component) {

    class ChildClass extends React.Component<{classifications: any[]}> {
        constructor(props) {
            super(props)
        }

        method = (classificator, val) => {
            for (let i = 0; i < classificator.length; i++) {
                if(classificator[i].value === val) {
                    return classificator[i].text;
                }
            }
            return false;
        };

        betterMethod = (classificatorKey, val) => {
            return ((this.props.classifications[classificatorKey] || [])
                .filter(item => item.value === val)[0] || {}).text || val || '-';
        };

        render() {
            return <Component getClassificationVal={this.method} getBetterClassificationVal={this.betterMethod} {...this.props}/>
        }
    }

    const mapStateToProps = (state) => {
        return {classifications: state.httpRequests.classifications};
    };

    return connect(
        mapStateToProps, null
    )(ChildClass);

};
import { dateFilterObj } from "./filters";

export const transformApplications = (data: any) => {

    let header = {
        Id: 'Application ID',
        Name: "Name",
        ClientCode: "Client Code",
        Type: "Services requested",
        CreatedDate: "Reg date",
        UpdatedDate: "Last Updated",
        Status: "Status",
        ClientId: "ID",
        Email: "Email",
        Phone: "Phone",
    };

    return {
        header,
        body: data.length > 0 ? dateFilterObj(data) : [],
    };

};

export const transformNewCustomers = (data: any) => {

    let header = {
        Email: "Email",
        ClientId: "ID",
        Name: "Name",
        Phone: "Phone",
        Code: "Customer Code",
        RegistrationDate: "Reg date",
        // ActivationDate: "Activation Date",
        // ClosingDate: 'Closing Data',
        // Status: "Status",
        // Type: "Services requested",
    };

    return {
        header,
        body: data.length > 0 ? dateFilterObj(data) : [],
    };
};

// TODO
export const transformApplicantsHistoryOfChanges = (data: any) => {
console.log(data);
    let header = {
        Email: "Date Of Change",
        ClientId: "ID",
        Name: "Name",
        Phone: "Phone",
        Code: "Customer Code",
        RegistrationDate: "Reg date",
        // ActivationDate: "Activation Date",
        // ClosingDate: 'Closing Data',
        // Status: "Status",
        // Type: "Services requested",
    };

    return {
        header,
        body: data.length > 0 ? dateFilterObj(data) : [],
    };
};


export const transformClients = (data: any) => {

    let header = {
        Email: "Email",
        ClientId: "ID",
        Name: "Name",
        Phone: "Phone",
        Code: "Customer Code",
        TerminalCode: "Terminal Code",
        Type: "Type",
        RegistrationDate: "DOB/Reg. date",
        ActivationDate: "Opening Date",
        Status: "Status",
    };

    return {
        header,
        body: data.length > 0 ? dateFilterObj(data) : [],
    };
};


export const transformContracts = (data: any) => {

    let header = {
        ContractNumber: "Contract Number",
        Type: "Type",
        // hasTerminal: '',
        ClientRegistrationCode: "Terminal Code",
        DepositSize: "Deposit Size (USD)",
        Status: "Status",
    };

    return {
        header,
        body: data,
        // body: data.length > 0 ? dateFilterObj(data) : [],
    };
};

export const transformContractAccounts = (data: any) => {

    let header = {
        AccountNumber: "Account Number",
        Currency: "Currency",
        Balance: "Balance",
        // Volume: "Volume",
        Quantity: "Quantity",
        ISIN: "ISIN",
        Ticker: "Ticker",
        OpeningDate: "Opening Date",
        ClosingDate: "Closing Date",
    };

    return {
        header,
        // body: data,
        body: data.length > 0 ? dateFilterObj(data) : [],
    };
};


const messages = {
    required: 'This field is required',
    number: 'Please enter a valid number',
    phone: 'Invalid phone number'
};

export default messages;
import React from "react";
import ReactDOM from "react-dom";
import IbxNotification from "../components/brokerage/shared/notification";

export const ArrayErrors = ({name, errors, touched, index, className = 'error-message'}) => {
    return errors[name] && touched[name] && typeof errors[name][index] === 'string' ?
        <span className={className}>{errors[name][index]}</span> : null
};

export function dateToYMD(date) {
    let d = date.getDate();
    let m = date.getMonth() + 1; //Month from 0 to 11
    let y = date.getFullYear();
    return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}

Number.prototype.toNumberFormatString = function() {
    return this.toString().toNumberFormatString();
};

String.prototype.toNumberFormatString = function(): string {

    //@ts-ignore
    if(!/^\d+(\.)?\d*$/.test(this)) {
        //@ts-ignore
        return this;
    }
    //@ts-ignore
    const num = (Math.round(parseFloat(this) * 100) / 100).toString();
    const parts = num.split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    if(parts[1]) {
        parts[1] = parts[1].length === 1 ? parts[1] + '0' : parts[1]
    } else {
        parts[1] = '00'
    }
    return parts.join(".");
};

export function isSet(e) {

    if (e === 0 || !!e) {

        if (typeof e === "object") {
            if (isSet(e.toArray))
                e = e.toArray();
            return !!(Object.keys(e).length);
        }

        return true;
    }
    else
        return false;
}

export const overrideEventDefaults = (event: Event) => {
    event.preventDefault();
    event.stopPropagation();
};

export const generateContractPassword = () => {
    const letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    const numbers = [0,1,2,3,4,5,6,7,8,9];
    let password = '';

    for (let i = 0; i < 6; i++) {
        password += letters[Math.floor(Math.random() * 26)]
    }
    for (let i = 0; i < 2; i++) {
        password += numbers[Math.floor(Math.random() * 10)]
    }
    return password
};

export const detectData = (response) => {
    console.log(response);
    // IbxNotification({message: 'Application was successfully saved', className: 'success'});
    /*ReactDOM.render(
        <IbxNotification className='success'
                         message='Application was successfully saved'
        />,
        document.querySelector('section'),
    );*/
    if(!response.Success) {
        alert(response.Message);
        return false;
    }
    alert('All Good');
    return true;
};
export const addToLocalStorage = (key: string, value: any) => {
    localStorage.setItem(key, value);
};

export const removeFromLocalStorage = (key: string) => {
    localStorage.setItem(key, '');
};

export const clearLocalStorage = () => {
    localStorage.clear();
};

export const getSValue = (key: string) => {
    return localStorage.getItem(key)
};

export const keyIsSet = (key: string) => {
    return localStorage.getItem(key) && localStorage.getItem(key) !== '';
};

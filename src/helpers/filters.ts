export const dateFilterObj = (data: any) => {
    let keys = Object.keys(Array.isArray(data) ? data[0] : data);

    let filterObj = (obj) => {
        for (let i = 0; i < keys.length; i++) {
            if(~keys[i].toLowerCase().indexOf('date') && obj[keys[i]]) {
                obj[keys[i]] = dateFilter(obj[keys[i]]);
            }
        }
        return obj;
    };

    data = Array.isArray(data) ? data.map(item => filterObj(item)) : filterObj(data)

    return data
};

export const dateFilter = (item: string): string => {
    return item.substring(0, item.indexOf('T'));
};

export const convertToArray = (value) => {

    let obj: number[] = [];
    let i = 1;

    while(i < 1025) {
        obj.push(i);
        i*=2;
    }

    return obj.filter(item => item & value);

};

export const convertToText = ({value}, classificators) => {

    let arr = classificators.filter(item => item.value & value).map(item => item = item.text);

    return arr.join(', ');

};

export const isCurrency = (fieldName) => {
    const currencyFields = ['Assets', 'AverageMonthlyIncome', 'AverageMonthlyProfit', 'DepositSize', 'Balance'];

    return currencyFields.includes(fieldName);
};
import React from "react";
import { connect } from 'react-redux';
import { PropsInterface } from "../models/props.inteface";

interface stateInterface {
    url?: string;
    form: {
        activePage: number;
        pageSize: number;
        pageNumber: number;
        sortType?: string;
        sortBy?: string;
    };
}

interface childParams {
    url: string;
    actionToDispatch(payload: any): any,
    screen: string;
    filters?: {
        status?: string;
        sortType?: string;
        sortBy?: string;
    }
}

export default function (Component, params: childParams) {

    class ChildClass extends React.Component<PropsInterface, stateInterface> {
        constructor(props) {
            super(props);
            this.state = {
                form: {
                    pageSize: 10,
                    activePage: 1,
                    pageNumber: 1,
                    ...params.filters || {}
                }
            };
        }

        filterFormValues(obj) {
            let filteredKeys = Object.keys(obj).filter(item => obj[item].length > 0);
            let filteredObject = {};

            for (let i = 0; i < filteredKeys.length; i++) {
                filteredObject = {
                    ...filteredObject,
                    [filteredKeys[i]]: obj[filteredKeys[i]],
                };
            }

            return filteredObject;
        }

        formSubmit(obj = {form: {}}) {

            // obj.form = this.filterFormValues(obj.form);

            this.setState(() => (
                {
                    form: {...this.state.form, ...obj.form}
                }),
                () => {
                    this.props.getTableData({url:`${this.props.rootUrl}/${params.url}`, formValues: this.state.form});
                });
        }

        /**
         * BE EXTRA CAREFUL HERE AS WE ARE CHANGING A PARAMETER VALUE
         * FIND A BETTER WAY LATER
         * @param parameter
         */
        //addParamsToUrl(parameter: string): void {
        //    params.url = params.url + parameter;
        //}

        handlePageChange(pageNumber) {
            if (pageNumber === this.state.form.activePage) {
                return;
            }
            this.formSubmit({form: {activePage: pageNumber, pageNumber: pageNumber}})
        }

        handlePageSize(pageSize) {
            pageSize = pageSize.target ? pageSize.target.value : pageSize.value;
            if (pageSize === this.state.form.pageSize) {
                return;
            }
            this.formSubmit({form: {pageSize: pageSize, pageNumber: 1, activePage: 1}})
        }

        temp = params.filters && params.filters.sortBy;

        handleSort(key) {
            if(key !== this.temp) {
                this.temp = key;
                this.formSubmit({form: {sortBy: key, sortType: 'ASC', pageNumber: 1, activePage: 1}});
            } else {
                this.formSubmit({form: {sortBy: key, sortType: this.toggleSort(), pageNumber: 1, activePage: 1}});
            }
        }

        toggleSort() {
            return this.state.form.sortType === 'ASC' ? 'DESC' : 'ASC';
        }

        render() {
            return <Component handlePageChange={this.handlePageChange.bind(this)}
                              formSubmit={this.formSubmit.bind(this)}
                              handlePageSize={this.handlePageSize.bind(this)}
                              handleSort={this.handleSort.bind(this)}
                              //addParamsToUrl={this.addParamsToUrl.bind(this)}
                              activePage={this.state.form.activePage}
                              pageSize={this.state.form.pageSize}
                              sortBy={this.state.form.sortBy}
                              sortType={this.state.form.sortType}
                              screen={params.screen}
                              {...this.props}/>
        }
    }

    const mapStateToProps = (state) => {
        return {
            rootUrl: state.httpRequests.rootUrl,
        }
    };

    const mapDispatchToProps = (dispatch) => {
        return {
            getTableData: (payload) => dispatch(params.actionToDispatch(payload)),
        }
    };

    return connect(
        mapStateToProps, mapDispatchToProps
    )(ChildClass);

};
import React from 'react';
import { Link } from "react-router-dom";

function Error() {

    return (
        <div>
            <h1>
                404
            </h1>
            <span>
                Page Not Found...
            </span>
            <Link to='/'>
                Go Back Home
            </Link>
        </div>
    )
}

export default Error;
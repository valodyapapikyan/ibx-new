import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import Error from '../error';
import { connect } from 'react-redux'
import {getClassifications, logoutHandler} from "../../actions";
import Customers from "./customers";
import Applications from "./applications";
import Ui from './ui-kits';
import IbxHeader from "./shared/fragments/header";
import IbxSidebar from "./shared/fragments/sidebar";
import Orders from "./orders";
import individualCustomer from "./customers/individual";

function Brokerage({match, logOut, rootUrl, getClassifications, classifications}) {

    const [isLoading, setIsLoading] = useState( true);

    const fetchData = async () => {

        await getClassifications(`${rootUrl}/Classificator/GetClassificators?lang=eng`);

        setIsLoading(false);
    };

    useEffect(() => {
        fetchData();
    },[]);

    return isLoading ? (
        <div className="loading-box">
            <span> Loading... </span>
        </div>
    ) : (

        <Router>
            <div className="page-container">
                <IbxHeader/>
                <IbxSidebar match={match}/>
                <Switch>
                    <Route path={match.path} exact component={() => <h2>Adminka</h2>}/>
                    <Route path={`${match.url}/applications`} component={ Applications }/>
                    <Route path={`${match.url}/customers/:clientId/:screen/:contractNumber`} component={individualCustomer}/>
                    <Route path={`${match.url}/customers/:clientId/:screen`} component={individualCustomer}/>
                    <Route path={`${match.url}/customers`} component={ Customers }/>
                    <Route path={`${match.url}/orders`} component={Orders}/>
                    <Route path={`${match.url}/reference-book`} component={() => <h2>Reference book</h2>}/>
                    <Route path={`${match.url}/reports`} component={() => <h2>Reports Page</h2>}/>
                    <Route path={`${match.url}/ui-kits`} component={ Ui }/>
                    <Route path={`${match.url}/settings`} component={() => <h2>Settings</h2>}/>
                    <Route path={`${match.url}/help`} component={() => <h2>Help</h2>}/>
                    <Route path='*' component={ Error } />
                </Switch>
            </div>
        </Router>

    )
}

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
        classifications: state.httpRequests.classifications,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: (payload) => dispatch(logoutHandler(payload)),
        getClassifications: (payload) => dispatch(getClassifications(payload)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Brokerage);
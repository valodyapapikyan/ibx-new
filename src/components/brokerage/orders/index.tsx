import React, {Component} from "react";
import Modal from 'react-modal';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import { connect } from 'react-redux'
import IbxButton from "../shared/button";
import IbxInput from "../shared/input";
import {Form, Formik} from "formik";
import IbxCheckbox from "../shared/checkbox";


const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};


const handleSubmit = (values) => {
    // console.log(values);
};

class Orders extends Component<null,{modalIsOpen: boolean}> {

    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false
        };
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }


    render() {
        const {modalIsOpen} = this.state;
        return (
            <div>
                <button onClick={this.openModal}>Open Modal</button>
                <Modal
                    isOpen={modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <Formik
                        enableReinitialize
                        initialValues={''}
                        validationSchema={''}

                        onSubmit={(values, { setSubmitting }) => {

                            setTimeout( () => {
                                handleSubmit({
                                    values,
                                    // url: `${rootUrl}/Account/Login`
                                });
                                setSubmitting(false);
                            }, 500)
                            //     .then(() => {
                            //     props.history.push('/brokerage');
                            // });

                        }}>

                        {/*Advanced search modal*/}

                        {/*<div className="ibx-modal advanced-modal">*/}
                            {/*<div className="modal-top flex align-items-center">*/}
                                {/*<span className="modal-title">Advanced Search</span>*/}
                                {/*/!*<span>*!/*/}
                                      {/*/!*<button onClick={this.closeModal}>close</button>*!/*/}
                                {/*/!*</span>*!/*/}
                            {/*</div>*/}
                            {/*<Form>*/}
                                {/*<div className="modal-content">*/}
                                    {/*<div>*/}
                                        {/*<IbxInput label="ID number" placeholder="Enter id number" />*/}
                                    {/*</div>*/}
                                    {/*<div>*/}
                                        {/*<IbxInput label="Contact phone number" placeholder="Enter phone number" />*/}
                                    {/*</div>*/}
                                    {/*<div>*/}
                                        {/*<IbxInput label="Contact email" placeholder="Enter email" />*/}
                                    {/*</div>*/}
                                    {/*<div>*/}
                                        {/*<IbxInput label="DOB/Registration date" type="date" />*/}
                                    {/*</div>*/}
                                {/*</div>*/}
                                {/*<div className="modal-bottom flex space-between">*/}
                                    {/*<IbxButton class="btn-outline btn-small" btnText="Cancel" />*/}
                                    {/*<IbxButton class="btn-grey-dark btn-small" btnText="Search"/>*/}
                                {/*</div>*/}
                            {/*</Form>*/}
                        {/*</div>*/}



                        {/*History  of changes modal*/}

                        {/*<div className="ibx-modal history-modal">*/}
                            {/*<div className="modal-top flex align-items-center">*/}
                                {/*<span className="modal-title">History  of change</span>*/}
                                {/*/!*<span>*!/*/}
                                      {/*/!*<button onClick={this.closeModal}>close</button>*!/*/}
                                {/*/!*</span>*!/*/}
                            {/*</div>*/}
                            {/*<Form>*/}
                                {/*<div className="modal-content">*/}
                                    {/*<div className="flex-table with-head">*/}
                                        {/*<div className="flex-row flex-head flex align-items-center">*/}
                                            {/*<div className="flex-item"><span>Date of change</span></div>*/}
                                            {/*<div className="flex-item large"><span>Name</span></div>*/}
                                            {/*<div className="flex-item"><span>Field</span></div>*/}
                                            {/*<div className="flex-item"><span>Old value</span></div>*/}
                                            {/*<div className="flex-item"><span>New value</span></div>*/}
                                        {/*</div>*/}
                                        {/*<div className="flex-row flex align-items-center">*/}
                                            {/*<div className="flex-item"><span>14.04.2019</span></div>*/}
                                            {/*<div className="flex-item large"><span>Armine Karapetyan</span></div>*/}
                                            {/*<div className="flex-item"><span>Deposit size</span></div>*/}
                                            {/*<div className="flex-item"><span>1 000 000</span></div>*/}
                                            {/*<div className="flex-item"><span>500 000</span></div>*/}
                                        {/*</div>*/}
                                        {/*<div className="flex-row flex align-items-center">*/}
                                            {/*<div className="flex-item"><span>14.04.2019</span></div>*/}
                                            {/*<div className="flex-item large"><span>Armine Karapetyan</span></div>*/}
                                            {/*<div className="flex-item"><span>Deposit size</span></div>*/}
                                            {/*<div className="flex-item"><span>1 000 000</span></div>*/}
                                            {/*<div className="flex-item"><span>500 000</span></div>*/}
                                        {/*</div>*/}
                                    {/*</div>*/}
                                {/*</div>*/}
                            {/*</Form>*/}
                        {/*</div>*/}


                        {/*Available accounts modal*/}

                        <div className="ibx-modal available-account-modal">
                            <div className="modal-top flex align-items-center">
                                <span className="mardotoBold uppercase">Available accounts</span>
                                <span className="closeBtn">
                                    <IbxButton handleClick={this.closeModal}
                                               class="btn-icon simple" iconClass="icon-close"/>
                                </span>
                            </div>
                            <Form>
                                <div className="modal-content">
                                    <div className="simple-table simple-table">
                                        <div className="table-row table-row-head">
                                            <div className="table-cell small"></div>
                                            <div className="table-cell large"><span>Account Number</span></div>
                                            <div className="table-cell"><span>Currency</span></div>
                                        </div>
                                        <div className="table-row">
                                            <div className="table-cell small">
                                               {/*<IbxCheckbox/>*/}
                                            </div>
                                            <div className="table-cell large"><a href="" className="green-link">18923981729837</a></div>
                                            <div className="table-cell"><span>USD</span></div>
                                        </div>
                                        <div className="table-row">
                                            <div className="table-cell small">
                                               {/*<IbxCheckbox/>*/}
                                            </div>
                                            <div className="table-cell large"><a href="" className="green-link">18923981729837</a></div>
                                            <div className="table-cell"><span>USD</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-bottom text-right inline-elements">
                                    <span>
                                        <IbxButton class="btn-outline btn-small" btnText="Cancel" />
                                    </span>
                                    <span>
                                        <IbxButton class="btn-grey-dark btn-small min-width-164 margin-left-10" btnText="Assign Selected"/>
                                    </span>
                                </div>
                            </Form>
                        </div>
                    </Formik>
                </Modal>
            </div>
        );
    }
}

export default Orders;
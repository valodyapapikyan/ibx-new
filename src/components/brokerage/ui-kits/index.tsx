import React, {useState} from "react";
import IbxSelect from "../shared/select";
import IbxInput from "../shared/input";
import IbxTextarea from "../shared/textarea";
import IbxCheckbox from "../shared/checkbox";
import IbxRadio from "../shared/radio";
import IbxSwitch from "../shared/switch";
import IbxStatus from "../shared/status";
import IbxButton from "../shared/button";
import {Formik} from "formik";
import IbxUploader from "../shared/uploader";


export default function () {
    const handleSelectChange = (e) => {
    };
    const allOptions = [
        {label: "All Options", value: 1},
        {label: "Option 1", value: 2},
        {label: "Option 2", value: 3},
        {label: "Option 3", value: 4},
    ];

    return (
        <Formik initialValues={{}} onSubmit={()=> console.log()}>
        <div>
            <IbxButton btnText="Button" class="btn-grey-dark"/>
            <IbxButton btnText="Button green" class="btn-green"/>
            <IbxButton btnText="Button outline" class="btn-outline"/>
            <br/>
            <br/>
            <IbxSelect options={allOptions}
                       changeHandler={handleSelectChange}/>
            <br/>
            <IbxSelect options={allOptions}
                       multiSelect={true}
                       changeHandler={handleSelectChange}/>
            <br/>
            <IbxSelect options={allOptions}
                       isDisabled={true}
                       changeHandler={handleSelectChange}/>
            <br/>
           {/* <IbxInput label="Input" placeholder="Placeholder"/>
            <IbxInput label="Disabled Input" placeholder="Disabled Input" isDisabled={true}/>
*/}
            <br/>
            <IbxTextarea label="Textarea" placeholder="kdkkdk"/>

            <br/>
            <br/>
            <IbxCheckbox label="Checkbox"/>

            <br/><br/>
            <IbxRadio label="Radio1"/>
            <IbxRadio label="Radio2"/>

            <br/>
            <br/>
            <IbxSwitch label="Switch" class="sw-left"/>
            <IbxSwitch label="Switch" class="sw-right"/>

            <br/>
            <br/>
            <IbxStatus class="bg-blue" text="Active" />
            <IbxStatus class="bg-green" text="Approved" />
            <IbxStatus class="st-new" text="New" />

            <br/>
            <br/>
            {/*<IbxUploader/>*/}
        </div>
        </Formik>
    )
}

import tableMethods from '../../../helpers/table-methods';
import Table from './shared';
import {getNewCustomers} from "../../../actions";

export default tableMethods(Table,
    {
        actionToDispatch: getNewCustomers,
        url: 'Client/GetClientsGridView',
        screen: 'new-customers',
        filters: {
            status: 'New',
            sortBy: 'RegistrationDate',
            sortType: 'ASC',
        }
    });
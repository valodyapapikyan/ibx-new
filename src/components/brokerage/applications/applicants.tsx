import tableMethods from '../../../helpers/table-methods';
import Table from './shared';
import { getApplications } from "../../../actions";

export default tableMethods(Table,
    {
        actionToDispatch: getApplications,
        url: 'Application/GetApplicationsGridView',
        screen: 'applicants',
        filters: {
            sortType: 'DESC',
            sortBy: 'Id',
        }
    });
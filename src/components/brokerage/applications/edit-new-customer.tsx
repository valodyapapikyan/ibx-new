import React, {Component} from 'react';
import ApplicantsForm from "./shared/applicants-form";
import {PropsInterface} from "../../../models/props.inteface";
import { connect } from 'react-redux';
import axios from "axios";

class EditApplicant extends Component<PropsInterface, {isLoading: boolean, data: any,}> {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            data: '',
        };
    }

    componentDidMount() {

        axios({
            method: 'get',
            url: `${this.props.rootUrl}/Client/GetClientsGridView?code=${this.props.match.params.clientId}`,
            responseType: 'json',
        }).then(response => {
            this.setState({
                isLoading: false,
                data: response.data.Data[0],
            });
        });
    }

    render() {

        if (this.state.isLoading) {
            return  <div className="loading-box">
                        <span> Loading... </span>
                    </div>
        }

        const { data } = this.state;

        return (
            <div>
                <ApplicantsForm selectedApplicant={data} action='/Application/InsertApplication' newApplicant rootUrl={this.props.rootUrl}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
    }
};

export default connect(
    mapStateToProps, null
)(EditApplicant);
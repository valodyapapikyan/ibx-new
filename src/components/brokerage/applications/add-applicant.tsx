import React, {Component} from 'react';
import ApplicantsForm from "./shared/applicants-form";
import { connect } from 'react-redux';
import axios from "axios";
import {PropsInterface} from "../../../models/props.inteface";

class AddApplicant extends Component<PropsInterface, {applicantsLoading: boolean, applicants: any,}> {
    constructor(props) {
        super(props);

        this.state = {
            applicantsLoading: true,
            applicants: '',
        };
    }

    componentDidMount() {

        axios({
            method: 'get',
            url: `${this.props.rootUrl}/Client/GetClientsGridView?limit=-1`,
            responseType: 'json',
        }).then(response => {
            this.setState({
                applicantsLoading: false,
                applicants: response.data.Data,
            });
        });
    }

    render() {

        if (this.state.applicantsLoading) {
            return <div className="loading-box">
                <span> Loading... </span>
            </div>
        }

        const { applicants } = this.state;

        return (
            <div>
                <ApplicantsForm newApplicant action='/Application/InsertApplication' applicants={applicants} rootUrl={this.props.rootUrl}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
    }
};

export default connect(
    mapStateToProps, null
)(AddApplicant);
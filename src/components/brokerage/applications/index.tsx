import React, {Component} from "react";
import {Route, Link, Redirect, Switch} from "react-router-dom";
import {PropsInterface} from "../../../models/props.inteface";
import Applicants from './applicants';
import NewCustomers from './new-customers';
import Error from "../../error";
import AddApplicant from "./add-applicant";
import EditNewCustomer from "./edit-new-customer";
import EditApplicant from "./edit-applicant";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import IbxSearchAndSort from "./shared/search-and-sort";

interface stateInterface {
    visible_content: string;
}

class ApplicationsIndex extends Component<PropsInterface, stateInterface> {

    constructor(props) {
        super(props);
    }

    render() {

        const Shared = ({screen}) => {
            return (
                <React.Fragment>
                    <div className="page-top flex align-items-center space-between">
                        <h2 className="page-title">{ screen }</h2>
                        <Link className="ibx-button btn-grey-dark btn-left-icon with-shadow max-width-194 selenium-add-application" to={`${this.props.match.url}/add-applicant`}>
                            <i className="icon-plus"></i>
                            <span className="btn-text">Add application</span>
                        </Link>
                    </div>
                    <div className="inner-container">
                        <div className="tabs">
                            <Link className={`tab-link ${screen === 'Applicants' && 'tab-link--selected prev-default'}`}
                                  to={`${this.props.match.url}/applicants-list`}>
                                Applicants
                            </Link>
                            <Link className={`tab-link ${screen === 'New Customers' && 'tab-link--selected prev-default'}`}
                                  to={`${this.props.match.url}/new-customers`}>
                                New Customers
                            </Link>
                        </div>
                        {/*<IbxSearchAndSort/>*/}
                    </div>
                </React.Fragment>
            )
        };
        return (
            <React.Fragment>
                <Switch>
                    <Route path={this.props.match.path} exact component={() =>
                        <Redirect to={`${this.props.match.url}/applicants-list`}/>
                    }/>

                    <Route path={`${this.props.match.url}/add-applicant`} component={AddApplicant}/>

                    <Route path={`${this.props.match.path}/applicants-list/:clientId`} component={EditApplicant}/>

                    <Route path={`${this.props.match.url}/applicants-list`} component={()=>
                        <div>
                            <Shared screen='Applicants'/>
                            <Applicants/>
                        </div>}/>
                    <Route path={`${this.props.match.path}/new-customers/:clientId`} component={EditNewCustomer}/>

                    <Route path={`${this.props.match.url}/new-customers`} component={()=>
                        <div>
                            <Shared screen='New Customers'/>
                            <NewCustomers allowEdit={true}/>
                        </div>}/>

                    <Route path={`${this.props.match.url}/*`} exact component={ Error } />
                </Switch>
            </React.Fragment>
        )
    }
}

export default ApplicationsIndex;
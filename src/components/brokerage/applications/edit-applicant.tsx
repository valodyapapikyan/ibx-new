import React, {Component} from 'react';
import ApplicantsForm from "./shared/applicants-form";
import {PropsInterface} from "../../../models/props.inteface";
import { connect } from 'react-redux';
import axios from "axios";

class EditApplicant extends Component<PropsInterface, {isLoading: boolean, data: any,}> {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            data: '',
        };
    }

    getData() {
        axios({
            method: 'get',
            url: `${this.props.rootUrl}/Application/GetApplication/${this.props.match.params.clientId}`,
            responseType: 'json',
        }).then(response => {
            if(!response.data.Success) {
                alert(response.data.Message);
                return;
            }
            axios({
                method: 'get',
                url: `${this.props.rootUrl}/Client/GetClientsGridView?code=${response.data.Data.ClientCode}`,
                responseType: 'json',
            }).then(response2 => {
                this.setState({
                    isLoading: false,
                    data: {...response2.data.Data[0], ...response.data.Data, ClientType: response2.data.Data[0].Type},
                });
            });
        });
    }

    componentDidMount() {
        this.getData();
    }

    render() {

        if (this.state.isLoading) {
            return  <div className="loading-box">
                        <span> Loading... </span>
                    </div>
        }

        const { data } = this.state;

        return (
            <div>
                <ApplicantsForm key={data.UpdatedDate} getData={this.getData.bind(this)} action='/Application/UpdateApplication' selectedApplicant={data} rootUrl={this.props.rootUrl}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
    }
};

export default connect(
    mapStateToProps, null
)(EditApplicant);
export default function (type) {

    let status = [
        {text: "New", value: 1},
        {text: "Approved", value: 2},
        {text: "In Review", value: 3},
        {text: "Waiting for Agreement", value: 4},
        {text: "Rejected", value: 5},
        {text: "Closed", value: 6},
    ];

    if(type == 1) {
        status = status.filter(item => item.value !== 3 && item.value !== 4);
    }

    return status;
}
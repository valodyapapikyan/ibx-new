import React from "react";
import Modal from 'react-modal';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

export default function({closeModal, modalIsOpen, data}) {
    // console.log(data);
    return (

        <Modal
            ariaHideApp={false}
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Example Modal"
        >
            <div className="ibx-modal history-modal">
                <div className="modal-top flex align-items-center">
                    <span className="modal-title">History  of change</span>
                    {/*<span>*/}
                    {/*<button onClick={props.closeModal}>close</button>*/}
                    {/*</span>*/}
                </div>

                <div className="modal-content">
                    <div className="flex-table with-head">
                        <div className="flex-row flex-head flex align-items-center">
                            <div className="flex-item"><span>Date of change</span></div>
                            <div className="flex-item large"><span>Name</span></div>
                            <div className="flex-item"><span>Field</span></div>
                            <div className="flex-item"><span>Old value</span></div>
                            <div className="flex-item"><span>New value</span></div>
                        </div>
                        <div className="flex-row flex align-items-center">
                            <div className="flex-item"><span>14.04.2019</span></div>
                            <div className="flex-item large"><span>Armine Karapetyan</span></div>
                            <div className="flex-item"><span>Deposit size (USD)</span></div>
                            <div className="flex-item"><span>1 000 000</span></div>
                            <div className="flex-item"><span>500 000</span></div>
                        </div>
                        <div className="flex-row flex align-items-center">
                            <div className="flex-item"><span>14.04.2019</span></div>
                            <div className="flex-item large"><span>Armine Karapetyan</span></div>
                            <div className="flex-item"><span>Deposit size (USD)</span></div>
                            <div className="flex-item"><span>1 000 000</span></div>
                            <div className="flex-item"><span>500 000</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>

    );

}
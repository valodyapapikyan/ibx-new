import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from 'react-redux'
import { PropsInterface } from "../../../../models/props.inteface";
import DefApiCallsProperties from "../../../../models/def-api-calls-properties";
import IbxStatus from "../../shared/status";
import IbxTableSort from "../../shared/table-sort";
import IbxResultPPSelect from "../../shared/results-pp-select";
import {Form} from "formik";
import IbxCheckbox from "../../shared/checkbox";
import IbxPagination from "../../shared/pagination";
import IbxSearchAndSort from "./search-and-sort";

class Applicants extends Component<PropsInterface & DefApiCallsProperties> {

    constructor(props) {
        super(props);
    }

    render() {

        let { Total, Data } = this.props.fetchedData || {Total: 0, Data: {body: []}};

        return (
            <div className="inner-container">

                <IbxSearchAndSort screen={this.props.screen} handleSubmit={this.props.formSubmit}/>

                {!this.props.fetched ?
                    <div className="loading-box">
                        <span> Loading... </span>
                    </div>
                    :
                    <>
                        <div  className="ibx-table-container">
                            <div className="ibx-table-row head">
                                {Object.keys(Data.header).filter(item => item !== 'ClientId' && item !== 'Phone' && item !== 'Email')
                                    .map((item, index) => (
                                        <div className={`ibx-table-cell ${item === 'Name' && 'large user-info'}`} key={index}>
                                            {item === 'Name' ?
                                                <span>{Data.header[item]} / {Data.header['ClientId']} / {Data.header['Phone']} / {Data.header['Email']}</span>
                                                :
                                                <span>{Data.header[item]}</span>
                                            }
                                            <IbxTableSort sort={{key: this.props.sortBy, value: this.props.sortType}} _key={item} handleSort={this.props.handleSort}/>
                                        </div>
                                    ))}
                            </div>
                            {Data.body && Data.body.length ?
                                Data.body.map((item, index) => (
                                    <div className="ibx-table-row" key={index}>
                                        {Object.keys(Data.header).filter(item => item !== 'ClientId' && item !== 'Phone' && item !== 'Email')
                                            .map((_item, _index) => (
                                                <div className={`ibx-table-cell ${_item === 'Name' && 'large user-info'}`} key={_index}>
                                                    { _item === 'Name' ?
                                                        <React.Fragment>
                                                            <p className="name">{item[_item]}</p>
                                                            <p>ID: {item['ClientId']}</p>
                                                            <p> {item['Phone']} / {item['Email']}</p>
                                                        </React.Fragment>

                                                        :

                                                        _item === 'Code' || _item === 'Id' ?
                                                            <Link className="green-link" to={`${this.props.match.path}/${item[_item]}`}>{item[_item]}</Link>
                                                            :
                                                            _item === 'Status' ?
                                                                <IbxStatus text={item[_item]}
                                                                           class={item[_item] === 'New' ? 'st-new' :
                                                                                   item[_item] === 'Approved' ? 'approved' :
                                                                                       item[_item] === 'Waiting for agreement' ? 'waiting' :
                                                                                           item[_item] === 'In review' ? 'in-review':
                                                                                               item[_item] === 'Rejected' ? 'rejected':
                                                                                                   item[_item] === 'Closed' ? 'closed' : 'active'}/>
                                                                :
                                                                item[_item]
                                                    }
                                                </div>
                                            ))}
                                    </div>
                                ))
                            :
                            ''
                            }
                        </div>
                        {Data.body && Data.body.length ?
                            <IbxPagination
                                activePage={this.props.activePage}
                                pageSize={this.props.pageSize}
                                rows={Data.body.length}
                                total={Total}
                                handlePageChange={this.props.handlePageChange}
                                handleResultsPP={this.props.handlePageSize}
                            />
                            :
                            ''
                        }

                        {Data.body && !Data.body.length &&
                            <div className="no-data">
                                <img src="/assets/images/no-items.png"/>
                                <p>Sorry, No result found :(</p>
                            </div>
                        }

                    </>
                }

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        fetchedData: state.httpRequests.fetchedData,
        fetched: state.httpRequests.fetched,
    }
};

export default withRouter(connect(mapStateToProps, null)(Applicants));
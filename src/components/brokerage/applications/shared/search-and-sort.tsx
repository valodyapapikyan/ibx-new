import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {Form, Formik} from "formik";
import IbxInput from "../../shared/input";
import IbxRadio from "../../shared/radio";
import IbxButton from "../../shared/button";
import IbxCheckbox from "../../shared/checkbox";


export default function({screen, handleSubmit}) {

    const filters = screen === 'applicants' ? {
        status: ['New', 'In Review', 'Waiting for Agreement'],
        type: [] as string[],
        id: '',
        phone: '',
        email: '',
        createdDateFrom: '',
        createdDateTo: '',
        search: '',
    } : {
        id: '',
        phone: '',
        email: '',
        createdDateFrom: '',
        createdDateTo: '',
        search: '',
    };

    useEffect(() => {
        handleSubmit({form: filters});
    }, []);

    const [state, setState] = useState({
        filtersShown: false,
    });

    const toggleFilters = () => {
        setState({filtersShown: !state.filtersShown})
    };

    return (
        <Formik
            enableReinitialize
            initialValues={filters}
            validationSchema={''}
            onSubmit={(values, {setSubmitting}) => {
                handleSubmit({
                    form: {...values, pageNumber: 1, activePage: 1},
                });
                setSubmitting(false);
            }}>
            {({errors, touched, values, handleChange, isSubmitting, handleReset}) => (

                <Form>
                    <div className="ibx-search-sort-container flex align-items-center space-between">

                        <div className="flex left-part align-items-center">
                            <div className="search-box">
                                <IbxInput class="int-left-icon" name="search" icon="icon-search"
                                          placeholder="Type a name, Customer code "/>
                            </div>
                            {/*<div>
                                <span className="green-link">Advanced Search</span>
                            </div>*/}
                        </div>
                        <div className="right-part text-right">
                            <span onClick={toggleFilters}
                                  className={`btn-icon btn-green ${state.filtersShown && 'active'}`}>
                                <i className="icon-filter"></i></span>
                        </div>
                    </div>
                    <div className={`filter-popup ${state.filtersShown && 'show'}`}>

                        <div className="row-md clearfix">
                            <div className="col-3">
                                <IbxInput name='id' label='ID number' placeholder='Enter id number'/>
                            </div>
                            <div className="col-3">
                                <IbxInput name='phone' label='Contact phone number' placeholder='Enter phone number'/>
                            </div>
                            <div className="col-3">
                                <IbxInput name='email' label='Contact Email' placeholder='Enter email'/>
                            </div>
                            <div className="col-3">
                                <span className="label">DOB/Registration Date</span>
                                <div className="inputs-group flex align-bottom">
                                    <IbxInput type="date" name='createdDateFrom'/>
                                    <IbxInput type="date" name='createdDateTo'/>
                                </div>
                            </div>
                        </div>

                        {screen === 'applicants' &&
                            <div className="row-md clearfix">
                                <div className="col-12">
                                    <span className="label">Status</span>
                                    <div className="checkboxes">
                                        {/*<IbxRadio class="custom-radio" label="All" value='all' name="Status"/>*/}
                                        <IbxCheckbox class="custom-checkbox" label="New" value='New' name="status"/>
                                        <IbxCheckbox class="custom-checkbox" label="Approved" value='Approved' name="status"/>
                                        <IbxCheckbox class="custom-checkbox" label="In Review" value='In Review' name="status"/>
                                        <IbxCheckbox class="custom-checkbox" label="Closed" value='Closed' name="status"/>
                                        <IbxCheckbox class="custom-checkbox" label="Rejected" value='Rejected' name="status"/>
                                        <IbxCheckbox class="custom-checkbox" label="Waiting for Agreement" value='Waiting for Agreement' name="status"/>
                                    </div>
                                </div>
                            </div>
                        }

                        <div className="row-md clearfix">
                            <div className="col-7">
                            {screen === 'applicants' &&
                                <React.Fragment>
                                    <span className="label">Services Requested</span>
                                    <div className="checkboxes">
                                        {/*<IbxRadio class="custom-radio" label="All" value='all' name="type"/>*/}
                                        <IbxCheckbox class="custom-checkbox" label="Brokerage" value='Brokerage'
                                                     name="type"/>
                                        <IbxCheckbox class="custom-checkbox" label="Custody" value='Custody' name="type"/>
                                        <IbxCheckbox class="custom-checkbox" label="Terminal" value='Terminal' name="type"/>
                                        <IbxCheckbox class="custom-checkbox" label="Investment advisory"
                                                     value='Investment advisory' name="type"/>
                                    </div>
                                </React.Fragment>
                            }
                            </div>

                            <div className="col-5 text-right last">
                                <a onClick={handleReset} className="green-link font-14">Reset filter</a>
                                <IbxButton class="btn-grey-dark selenium-apply-filter-button-id max-width-170" btnText="Apply filter"/>
                            </div>
                        </div>

                    </div>
                </Form>
            )}
        </Formik>
    )
}
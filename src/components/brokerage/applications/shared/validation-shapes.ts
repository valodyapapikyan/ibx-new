import * as Yup from 'yup';
import messages from "../../../../helpers/validation-constants";


const buildRequiredForServices = (item) => {
    return {
        is: (value) => {
            return item.indexOf(value.toString()) > -1
        },
        then: Yup.string().required(messages.required)
    }
};

export const buildRequiredCheckbox = (item) => {
    return {
        is: (value) => {
            value = value.map(item => item.toString());
            return value.indexOf(item.toString()) > -1
        },
        then: Yup.string().required(messages.required)
    }
};
const buildRequiredObject = (item: string | string[]) => {
    return {
        is: (value) => value.indexOf(item) !== -1,
        then: Yup.string().required(messages.required)
    }
};

export const addEditApplicantsSchema = {
    custody: Yup.object().shape({
        MobilePhone: Yup.number().typeError(messages.number).required(messages.required),
        Type: Yup.string().required(messages.required),
        DepoAccount: Yup.mixed().required(messages.required),
        ClientManager: Yup.string().required(messages.required),
        Comments: Yup.array().of(Yup.string().required(messages.required)),
        ClosingRejectionReason: Yup.string().when('Status', buildRequiredForServices(['5', '6'])),
        Files: Yup.string(),
        ContactPerson: Yup.string(),
        ContactPersonId: Yup.string(),
    }),
    brokerage: Yup.object().shape({
        MobilePhone: Yup.number().typeError(messages.number).required(messages.required),
        Type: Yup.string().required(messages.required),
        IncludingTerminal: Yup.mixed().required(messages.required),
        DepositSize: Yup.number().typeError(messages.number).required(messages.required),
        BrokerageAccountCurrency: Yup.array().of(Yup.mixed()).test('BrokerageAccountCurrency', messages.required, item => {
            return item.length > 0
        }),
        TerminalTariffs: Yup.string().test('TerminalTariffs', messages.required, item => item > 0),
        Class: Yup.string().test('Class', messages.required, item => item > 0),
        Scenario: Yup.string().test('Scenario', messages.required, item => item > 0),
        DepoAccount: Yup.string().required(messages.required),
        Margin: Yup.string().required(messages.required),
        InstrumentsType: Yup.array().of(Yup.mixed()).when('IncludingTerminal', {
            is: item => {
                return item && item.toString() === 'true'
            },
            then: Yup.array().of(Yup.mixed()).test('BrokerageAccountCurrency', messages.required, item => {
                return item.length > 0
            }),
            otherwise: Yup.array().of(Yup.mixed())
        }),
        InstrumentsOther: Yup.string().when('InstrumentsType', buildRequiredCheckbox(1024)),
        ClientManager: Yup.string().required(messages.required),
        Comments: Yup.array().of(Yup.string().required(messages.required)),
        ClosingRejectionReason: Yup.string().when('Status', buildRequiredForServices(['5', '6'])),
        Files: Yup.string(),
        ContactPerson: Yup.string(),
        ContactPersonId: Yup.string(),
    }),
    terminal: Yup.object().shape({
        MobilePhone: Yup.number().typeError(messages.number).required(messages.required),
        Type: Yup.string().required(messages.required),
        DepositSize: Yup.number().typeError(messages.number).required(messages.required),
        TerminalTariffs: Yup.string().test('TerminalTariffs', messages.required, item => item > 0),
        Class: Yup.string().test('Class', messages.required, item => item > 0),
        Scenario: Yup.string().test('Scenario', messages.required, item => item > 0),
        Margin: Yup.string().required(messages.required),
        InstrumentsType: Yup.array().of(Yup.string()).required(messages.required),
        InstrumentsOther: Yup.string().when('InstrumentsType', buildRequiredCheckbox('1024')),
        ContractNumber: Yup.string().required(messages.required),
        ClientManager: Yup.string().required(messages.required),
        Comments: Yup.array().of(Yup.string().required(messages.required)),
        ClosingRejectionReason: Yup.string().when('Status', buildRequiredForServices(['5', '6'])),
        Files: Yup.string(),
        ContactPerson: Yup.string(),
        ContactPersonId: Yup.string(),
    }),
    investment: Yup.object().shape({
        MobilePhone: Yup.number().typeError(messages.number).required(messages.required),
        Type: Yup.string().required(messages.required),
        TerminalTariffs: Yup.string().test('TerminalTariffs', messages.required, item => item > 0),
        Class: Yup.string().test('Class', messages.required, item => item > 0),
        InstrumentsType: Yup.array().of(Yup.string()).required(messages.required),
        InstrumentsOther: Yup.string().when('InstrumentsType', buildRequiredCheckbox('1024')),
        ClientManager: Yup.string().required(messages.required),
        Comments: Yup.array().of(Yup.string().required(messages.required)),
        ClosingRejectionReason: Yup.string().when('Status', buildRequiredForServices(['5', '6'])),
        Files: Yup.string(),
        ContactPerson: Yup.string(),
        ContactPersonId: Yup.string(),
    }),
    default: Yup.object().shape({
        MobilePhone: Yup.number().typeError(messages.number).required(messages.required),
        Type: Yup.string().required(messages.required),
        ClientManager: Yup.string().required(messages.required),
        Comments: Yup.array().of(Yup.string().required(messages.required)),
        ClosingRejectionReason: Yup.string().when('Status', buildRequiredForServices(['5', '6'])),
        Files: Yup.string(),
        ContactPerson: Yup.string(),
        ContactPersonId: Yup.string(),
    }),
};

export const legalClient = Yup.object().shape({
    ContactPerson: Yup.string().required(messages.required),
    ContactPersonId: Yup.string().required(messages.required),
});

export const shape = (type) => {
    switch (type.toString()) {
        case '1':
            return addEditApplicantsSchema['custody'];
        case '2':
            return addEditApplicantsSchema['brokerage'];
        case '3':
            return addEditApplicantsSchema['terminal'];
        case '4':
            return addEditApplicantsSchema['investment'];
        default:
            return addEditApplicantsSchema['default'];
    }
};
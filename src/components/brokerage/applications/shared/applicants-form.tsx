import React, {useEffect, useState} from 'react';
import IbxSelect from "../../shared/select";
// import { isEmpty } from "../../../../helpers/methods";

import {Formik, Form, Field, ErrorMessage, FieldArray} from 'formik';
import {legalClient, shape} from "./validation-shapes";
import { Link, withRouter} from "react-router-dom";
import IbxButton from "../../shared/button";
import IbxRadio from "../../shared/radio";
import IbxInput from "../../shared/input";
import IbxCheckbox from "../../shared/checkbox";
import IbxUploader from "../../shared/uploader";
import Comments from "../../shared/notes";
import {ApplicantClass} from "../../../../models/applicant.interface";
import axios from "axios";
import {convertToArray, dateFilter} from "../../../../helpers/filters";
import HistoryModal from '../shared/history-modal';
import statusOptions from '../shared/application-status';
import {transformApplicantsHistoryOfChanges} from "../../../../helpers/table-transform";
import {detectData} from "../../../../helpers/methods";

function ApplicantsForm({newApplicant = false, applicants = [], selectedApplicant = new ApplicantClass(), getData = () => void 0, rootUrl, action, history}) {

    const initialValues = newApplicant ? {
        MobilePhone: '',
        Type: '',
        IncludingTerminal: '',
        DepositSize: '',
        BrokerageAccountCurrency: [] as (string|number)[],
        TerminalTariffs: '',
        Class: '',
        Scenario: '',
        DepoAccount: '',
        ContractNumber: '',
        Margin: '',
        InstrumentsType: [] as (string|number)[],
        InstrumentsOther: '',
        Files: [] as any[],
        ClientManager: '',
        Comments: [] as string[],
        ContactPerson: '',
        ContactPersonId: '',
        ClientCode: '',
        UpdatedBy: '',
        UpdatedDate: '',
        ClosingRejectionReason: "",
        CreatedBy: '',
        CreatedDate: '',
        Status: '' as any,
    } : {
        ...selectedApplicant as any,
    };
    const [state, setState] = useState({
        selectedApplicant,
        formDisabled: selectedApplicant.Status == '2' || selectedApplicant.Status == '5' || selectedApplicant.Status == '6',
        structures: {} as any,
        modalIsOpen: false,
        historyData: {} as any,
        isLoading: true,
        statusOptions: [] as {} as any[],
        schema: {},
        cms: [] as any[],
        contractsList: [] as any[],
        initialValues: {...initialValues},
    });

    const _default = {
            MobilePhone: '',
            Type: '',
            ClientManager: '',
            Comments: [] as string[],
            Files: [] as string[],
            ContactPerson: '',
            ContactPersonId: '',
    };

    const handleClientType = (applicant, service = '-1') => {

        if (applicant && (applicant['Type'] === 'Individual' || applicant['ClientType'] === 'Individual')) {
            setState(prevState => {
                    return {
                        ...prevState,
                        schema: shape(service),
                        selectedApplicant: applicant,
                        isLoading: false,
                        initialValues: {
                            ...prevState.initialValues,
                            ...prevState.structures[service],
                            BrokerageAccountCurrency: convertToArray(prevState.structures[service] || prevState.initialValues.BrokerageAccountCurrency),
                            InstrumentsType: convertToArray(prevState.structures[service] || prevState.initialValues.InstrumentsType),
                            Status: newApplicant ? 1 : prevState.initialValues.Status,
                            ClientCode: applicant.Code || applicant.ClientCode,
                        }
                    }
                }
            );
            setState(prevState => ({...prevState, statusOptions: statusOptions(prevState.initialValues.Type)}))
        } else {
            setState((prevState => {
                    return {
                        ...prevState,
                        schema: legalClient.concat(shape(service)),
                        selectedApplicant: applicant,
                        isLoading: false,
                        initialValues: {
                            ...prevState.initialValues,
                            ...prevState.structures[service],
                            BrokerageAccountCurrency: convertToArray(prevState.structures[service] || prevState.initialValues.BrokerageAccountCurrency),
                            InstrumentsType: convertToArray(prevState.structures[service] || prevState.initialValues.InstrumentsType),
                            Status: newApplicant ? 1 : prevState.initialValues.Status,
                            ClientCode: applicant.Code || applicant.ClientCode,
                        }
                    }
                }
            ));
            setState(prevState => ({...prevState, statusOptions: statusOptions(prevState.initialValues.Type)}))
        }
    };

    const openModal = () => {
        setState((prevState => ({...prevState, modalIsOpen: true})));
        /*axios({
            method: 'get',
            url: `${rootUrl}/Application/GetApplicationHistory/${state.selectedApplicant.Id}`,
            // responseType: 'json',
        }).then(response => {
            console.log(response.data.Data);
            setState((prevState => ({...prevState, modalIsOpen: true, historyData: transformApplicantsHistoryOfChanges(response.data.Data), isLoading: false})));
        });*/
    };

    const closeModal = () => {
        setState((prevState => ({...prevState, modalIsOpen: false})));
    };

    useEffect(() => {
        axios({
            method: 'get',
            url: `${rootUrl}/Common/GetClientManagers`,
            responseType: 'json',
        }).then(response => {
            setState((prevState) => ({
                ...prevState,
                cms: response.data,
            }));
        });

        if(newApplicant) {
            setState((prevState => ({...prevState, isLoading: true})));

            axios({
                method: 'get',
                url: `${rootUrl}/Application/GetApplicationsStructurs`,
                responseType: 'json',
            }).then(response => {
                // console.log(response.data);
                let data = {...response.data, '-1': _default};
                setState((prevState => ({...prevState, structures: data, isLoading: false})));
            });
        }

        if (newApplicant) {
            if(selectedApplicant && selectedApplicant.Code) {
                getContracts(selectedApplicant.Code);
            }
        } else {
            // for existing application: only get contracts list if the service type is "terminal"
            if(selectedApplicant['Type'] == '3') {
                getContracts(selectedApplicant.Code);
            }
        }

        handleClientType(state.selectedApplicant, state.selectedApplicant.Type);
    },[]);

    const getContracts = (clientCode) => {
        axios({
            method: 'get',
            url: `${rootUrl}/Common/GetBrokerageContractsList?clientCode=${clientCode}`,
            responseType: 'json',
        }).then(response => {
            setState((prevState) => ({
                ...prevState,
                contractsList: response.data,
            }));
        });
    };

    const handleSelectChange = (resetHandler, applicant) => {
        getContracts(applicant.Code);
        handleClientType(applicant);
        resetHandler();
    };

    const handleServiceTypeChange = (service: string, valuesToKeep = {
        MobilePhone: '',
        ContactPerson: '',
        ContactPersonId: '',
        Files: [] as any[],
        ClientManager: '',
        Comments: [] as string[],
    }) => {
        // console.log(state.structures);
        handleClientType(state.selectedApplicant, service);
        // shape(service);
        setState((prevState => ({
            ...prevState,
            initialValues: {
                ...prevState.initialValues,
                ...valuesToKeep,
            },
        })));
    };

    const handleSubmit = ({values}) => {
        let BrokerageAccountCurrency = values.BrokerageAccountCurrency.reduce((all, item) => parseInt(all) + parseInt(item), 0);
        let InstrumentsType = values.InstrumentsType.reduce((all, item) => parseInt(all) + parseInt(item), 0);
        values = {...values, InstrumentsType, BrokerageAccountCurrency};
        // console.log(values);
        axios({
            method: 'post',
            url: rootUrl + action,
            data: values
        }).then(response => {

            if(!detectData(response.data)) {
                return;
            }


            //when you get a number as a response then this is a new applicant
            // and that number is the newly created id for that user
            if(newApplicant) {
                history.push(`/brokerage/applications/applicants-list/${response.data.Data}`)
            } else {
                getData();
            }
            /*else if(values.Status == '2' || values.Status == '5' || values.Status == '6') {
                setState(prevState => (
                    {
                        ...prevState,
                        formDisabled: true,
                    }
                ))
            }*/
        }).catch(error => {
            alert('Something went wrong!!!<br/>' + error);
        })
    };

    return state.isLoading ? (
        <div className="loading-box">
            <span> Loading... </span>
        </div>
    ) : (

            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{...state.initialValues, Files: state.initialValues.Files.slice()}}
                    validationSchema={state.schema}
                    // validate={(values => {
                    //     console.log('validate', values, state.schema);
                    // })}
                    onSubmit={(values, { setSubmitting }) => {
                        handleSubmit({values});
                        setSubmitting(false);
                    }}
                >
                    {({errors, touched, values, handleChange, isSubmitting, handleReset}) => (
                        <Form>
                            {/*<fieldset>
                                <legend>Values</legend>
                                <div>{JSON.stringify(values, null, 2)}</div>
                            </fieldset>
                            <fieldset>
                                <legend>Errors</legend>
                                <div>{JSON.stringify(errors, null, 2)}</div>
                            </fieldset>*/}

                            {/*
                             Disable all fields when the status of the application is one of the following:
                             "Approved = 2, Rejected = 5, Closed = 6"
                             */}
                            <fieldset
                                disabled={state.formDisabled}
                                style={{textDecoration: 'none', outline: 'none', border: 'none'}}
                            >

                            {newApplicant && (!!applicants.length &&
                            <div className="top-select-box">
                                <div className="row-md clearfix">
                                    <div className="col-3">
                                        <IbxSelect options={applicants}
                                                   name='selectedApplicant'
                                                   labelKey='Name'
                                                   valueKey='Code'
                                                   selectedOptions={state.selectedApplicant.Code}
                                                   key={state.selectedApplicant.Code}
                                                   changeHandler={handleSelectChange.bind(null, handleReset)}/>

                                    </div>
                                </div>
                            </div>

                            )}
                            {state.selectedApplicant && state.selectedApplicant.Code &&
                                <>
                            <div className="page-top with-buttons">
                                <Link to="/brokerage/applications" className="green-link with-icon back-link">
                                    <i className="icon-arrow-left font-8"></i>
                                    <span>Back to Applications</span>
                                </Link>
                                <div className=" flex align-items-center space-between">
                                    <h2 className="page-title">Application #{state.selectedApplicant && state.selectedApplicant['Id']}</h2>
                                    <div className="buttons flex align-items-center">
                                        <Link to="/brokerage/applications">
                                            <IbxButton class="btn-outline" type='button' btnText="Cancel"/>
                                        </Link>
                                        <IbxButton class="btn-grey-dark btn-left-icon selenium-save-button" disabled={isSubmitting} btnText="Save" iconClass="icon-save"/>
                                    </div>
                                </div>
                            </div>
                            <div className="inner-container">
                                <div className="row clearfix flex align-items-stretch">
                                    <div className="col-9">
                                        <div className="block-container">

                                            <div className="block-row">
                                                <p className="block-title">Application</p>

                                                {state.selectedApplicant && (state.selectedApplicant['Type'] === 'Individual' || state.selectedApplicant['ClientType'] === 'Individual') ?

                                                <div className="flex-table">
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Full Name</span></div>
                                                        <div className="flex-item"><span>{state.selectedApplicant['Name']}</span></div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Customer Code</span></div>
                                                        <div className="flex-item"><span>{state.selectedApplicant['Code']}</span></div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Mobile Number</span></div>
                                                        <div className="flex-item">
                                                            <IbxInput  class={`int-small`}
                                                                       errors={errors}
                                                                       touched={touched}
                                                                       name={`MobilePhone`}
                                                                       placeholder="Mobile Number" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>ID</span></div>
                                                        <div className="flex-item"><span>Passport {state.selectedApplicant['ClientId']}</span></div>
                                                    </div>
                                                </div>
                                                :
                                                <div className="flex-table">
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Company Name</span></div>
                                                        <div className="flex-item"><span>{state.selectedApplicant['Name']}</span></div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Customer Code</span></div>
                                                        <div className="flex-item"><span>{state.selectedApplicant['Code']}</span></div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Company TIN</span></div>
                                                        <div className="flex-item"><span>{state.selectedApplicant['ClientId'] || '-'}</span></div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Contact Person</span></div>
                                                        <div className="flex-item">
                                                            <IbxInput  class={`int-small`}
                                                                       errors={errors}
                                                                       touched={touched}
                                                                       name={`ContactPerson`}
                                                                       placeholder="Contact Person"/>
                                                        </div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Contact Person ID</span></div>
                                                        <div className="flex-item">
                                                            <IbxInput  class={`int-small`}
                                                                       errors={errors}
                                                                       touched={touched}
                                                                       name={`ContactPersonId`}
                                                                       placeholder="Contact PersonId" />
                                                        </div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Mobile Phone</span></div>
                                                        <div className="flex-item">
                                                            <IbxInput  class={`int-small`}
                                                                       errors={errors}
                                                                       touched={touched}
                                                                       name={`MobilePhone`}
                                                                       placeholder="Mobile Phone" />
                                                        </div>
                                                    </div>
                                                </div>
                                                }
                                            </div>

                                            <div className="block-row">
                                                <div className="block-top flex space-between">
                                                    <p className="block-title">Requested Services</p>
                                                    {!newApplicant &&
                                                    <a className={`green-link with-icon`} onClick={openModal}>
                                                        <img src='/assets/icons/history_active.svg'/>
                                                        <span>History of changes</span>
                                                    </a>
                                                    }
                                                </div>
                                                <div className="flex-table">
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item">
                                                            <span>Services</span>
                                                            <ErrorMessage
                                                                name='Type'
                                                                component='span'
                                                                className='error-message simple'
                                                            />
                                                        </div>
                                                        <div className="flex-item">
                                                            <span>
                                                                <Field value='1'
                                                                       type='radio'
                                                                       name='Type'
                                                                       render={({field, form}) => (
                                                                           <label className={`ibx-radio ${!newApplicant && 'prev-default'}`}>
                                                                               <input
                                                                                   type='radio'
                                                                                   checked={values[field.name] == field.value}
                                                                                   {...field}
                                                                                   onChange={e => {
                                                                                       if(!newApplicant) {
                                                                                           e.preventDefault();
                                                                                           return;
                                                                                       }
                                                                                       handleServiceTypeChange(field.value, {
                                                                                           ContactPerson: values.ContactPerson,
                                                                                           MobilePhone: values.MobilePhone,
                                                                                           ContactPersonId: values.ContactPersonId,
                                                                                           Files: values.Files,
                                                                                           Comments: values.Comments,
                                                                                           ClientManager: values.ClientManager,
                                                                                       });
/*
                                                                                       form.setValues({
                                                                                           ...values,
                                                                                           IncludingTerminal: '',
                                                                                       });
*/
                                                                                       handleChange(e);
                                                                                   }}/>
                                                                               <span><i className="icon-check"></i></span>
                                                                               <span className="item-text">Custody</span>
                                                                           </label>
                                                                       )}
                                                                />
                                                            </span>
                                                            <span>
                                                                <Field value='2'
                                                                       type='radio'
                                                                       name='Type'
                                                                       render={({field, form}) => (
                                                                           <label className={`ibx-radio ${!newApplicant && 'prev-default'}`}>
                                                                           <input
                                                                               type='radio'
                                                                               checked={values[field.name] == field.value}
                                                                               {...field}
                                                                               onChange={e => {
                                                                                   if(!newApplicant) {
                                                                                       e.preventDefault();
                                                                                       return;
                                                                                   }
                                                                                   handleServiceTypeChange(field.value, {
                                                                                       ContactPerson: values.ContactPerson,
                                                                                       MobilePhone: values.MobilePhone,
                                                                                       ContactPersonId: values.ContactPersonId,
                                                                                       Files: values.Files,
                                                                                       Comments: values.Comments,
                                                                                       ClientManager: values.ClientManager,
                                                                                   });
                                                                                   // form.setValues({...values,});
                                                                                   handleChange(e);
                                                                               }}/>
                                                                               <span><i className="icon-check"></i></span>
                                                                               <span className="item-text">Brokerage</span>
                                                                           </label>
                                                                       )}
                                                                />
                                                            </span>
                                                            <span>
                                                                <Field value='3'
                                                                       type='radio'
                                                                       name='Type'
                                                                       render={({field, form}) => (
                                                                           <label className={`ibx-radio ${!newApplicant && 'prev-default'}`}>
                                                                               <input
                                                                                   type='radio'
                                                                                   checked={values[field.name] == field.value}
                                                                                   {...field}
                                                                                   onChange={e => {
                                                                                       if(!newApplicant) {
                                                                                           e.preventDefault();
                                                                                           return;
                                                                                       }
                                                                                       handleServiceTypeChange(field.value, {
                                                                                           ContactPerson: values.ContactPerson,
                                                                                           MobilePhone: values.MobilePhone,
                                                                                           ContactPersonId: values.ContactPersonId,
                                                                                           Files: values.Files,
                                                                                           Comments: values.Comments,
                                                                                           ClientManager: values.ClientManager,
                                                                                       });
/*
                                                                                       form.setValues({
                                                                                           ...values,
                                                                                           IncludingTerminal: ''
                                                                                       });
*/
                                                                                       handleChange(e);
                                                                                   }}/>
                                                                               <span><i className="icon-check"></i></span>
                                                                               <span className="item-text">Terminal</span>
                                                                           </label>
                                                                       )}
                                                                />
                                                            </span>
                                                            <span>
                                                                <Field value='4'
                                                                       type='radio'
                                                                       name='Type'
                                                                       render={({field, form}) => (
                                                                           <label className={`ibx-radio ${!newApplicant && 'prev-default'}`}>
                                                                               <input
                                                                                   type='radio'
                                                                                   checked={values[field.name] == field.value}
                                                                                   {...field}
                                                                                   onChange={e => {
                                                                                       if(!newApplicant) {
                                                                                           e.preventDefault();
                                                                                           return;
                                                                                       }
                                                                                       handleServiceTypeChange(field.value, {
                                                                                           ContactPerson: values.ContactPerson,
                                                                                           MobilePhone: values.MobilePhone,
                                                                                           ContactPersonId: values.ContactPersonId,
                                                                                           Files: values.Files,
                                                                                           Comments: values.Comments,
                                                                                           ClientManager: values.ClientManager,
                                                                                       });

/*
                                                                                       form.setValues({
                                                                                           ...values,
                                                                                           IncludingTerminal: '',
                                                                                       });
*/
                                                                                       handleChange(e);
                                                                                   }}/>
                                                                               <span><i className="icon-check"></i></span>
                                                                               <span className="item-text">Investment Advisory</span>
                                                                           </label>
                                                                       )}
                                                                />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    {/*<ErrorMessage*/}
                                                        {/*name='Type'*/}
                                                        {/*component="div"*/}
                                                        {/*className="error-message"*/}
                                                    {/*/>*/}

                                                    {values.Type == '2' ?
                                                        <React.Fragment>
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Including Terminal</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <Field value='false'
                                                                               type='radio'
                                                                               name='IncludingTerminal'
                                                                               render={({field, form}) => {
                                                                                   return <label className={"ibx-radio"}>
                                                                                       <input
                                                                                           type='radio'
                                                                                           checked={values[field.name].toString() === field.value.toString()}
                                                                                           {...field}
                                                                                           onChange={e => {
                                                                                               handleChange(e);
                                                                                           }}/>
                                                                                       <span><i className="icon-check"></i></span>
                                                                                       <span className="item-text">No</span>
                                                                                   </label>
                                                                               }}
                                                                        />
                                                                    </span>
                                                                    <span>
                                                                        <Field value='true'
                                                                               type='radio'
                                                                               name='IncludingTerminal'
                                                                               render={({field, form}) => (
                                                                                   <label className={"ibx-radio"}>
                                                                                       <input
                                                                                           type='radio'
                                                                                           checked={values[field.name].toString() === field.value.toString()}
                                                                                           {...field}
                                                                                           onChange={e => {
                                                                                               handleChange(e);
                                                                                           }}/>
                                                                                       <span><i className="icon-check"></i></span>
                                                                                       <span className="item-text">Yes</span>
                                                                                   </label>
                                                                               )}
                                                                        />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Deposit Size (USD)</span></div>
                                                                <div className="flex-item">
                                                                    <IbxInput errors={errors}
                                                                              touched={touched}
                                                                              class={`int-small`}
                                                                              name='DepositSize'/>
                                                                </div>
                                                            </div>
                                                            {values.IncludingTerminal.toString() === 'true' &&
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Margin</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                         <IbxRadio label="No" value='false' name='Margin'/>
                                                                    </span>
                                                                    <span>
                                                                         <IbxRadio label="Yes" value='true' name='Margin'/>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            }
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Brokerage Account Currency</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxCheckbox label="AMD" numeric name='BrokerageAccountCurrency' value='1'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="USD" numeric name='BrokerageAccountCurrency' value='2'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="EUR" numeric name='BrokerageAccountCurrency' value='4'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="RUB" numeric name='BrokerageAccountCurrency' value='8'/>
                                                                    </span>

                                                                </div>
                                                            </div>

                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Depo Account</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="No" value='false' name='DepoAccount'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="Yes" value='true' name='DepoAccount'/>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Tariff</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="Standard" value='1' name='TerminalTariffs'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="By volume" value='2' name='TerminalTariffs'/>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            {values.IncludingTerminal.toString() === 'true' &&
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Instruments Types</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxCheckbox label="Stocks" numeric value='1' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Bonds" numeric value='2' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Options" numeric value='4' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Futures" numeric value='8' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Forex" numeric value='16' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span className="inline-elements">
                                                                        <IbxCheckbox label="Other" numeric clearOther='InstrumentsOther' value='1024' name='InstrumentsType'/>
                                                                        {values.InstrumentsType.map(item => item.toString()).indexOf('1024') !== -1 &&
                                                                        <span className="margin-left-10">
                                                                            <IbxInput errors={errors}
                                                                                      touched={touched}
                                                                                      class={`int-small`}
                                                                                      name='InstrumentsOther'/>
                                                                        </span>
                                                                        }
                                                                    </span>

                                                                </div>
                                                            </div>
                                                            }
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Class</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="Professional" value='1' name='Class'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="Non-professional" value='2' name='Class'/>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Scenario</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="Green" value='1' name='Scenario'/>
                                                                    </span>
                                                                    {values.IncludingTerminal.toString() === 'false' &&
                                                                        <span>
                                                                            <IbxRadio label="Red 2" value='4'
                                                                                      name='Scenario'/>
                                                                        </span>
                                                                    }
                                                                    {values.IncludingTerminal.toString() === 'true' &&
                                                                    <>
                                                                        <span>
                                                                            <IbxRadio label="Yellow" value='2'
                                                                                      name='Scenario'/>
                                                                        </span>

                                                                        <span>
                                                                            <IbxRadio label="Red 1" value='3'
                                                                                      name='Scenario'/>
                                                                        </span>
                                                                        <span>
                                                                            <IbxRadio label="Red 2" value='4'
                                                                                      name='Scenario'/>
                                                                        </span>
                                                                    </>
                                                                    }

                                                                </div>
                                                            </div>
                                                        </React.Fragment>
                                                        :
                                                            values.Type == '1' ?

                                                                    <div className="flex-row flex align-items-center">
                                                            <div className="flex-item"><span>Depo account</span></div>
                                                            <div className="flex-item">
                                                                <span>
                                                                    <IbxRadio label="No" value='false' name='DepoAccount'/>
                                                                </span>
                                                                <span>
                                                                    <IbxRadio label="Yes" value='true' name='DepoAccount'/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                            :
                                                            values.Type == '4' ?

                                                                <React.Fragment>
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Tariff</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="Standard" value='1' name='TerminalTariffs'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="By volume" value='2' name='TerminalTariffs'/>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Instruments types</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxCheckbox label="Stocks" numeric value='1' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Bonds" numeric value='2' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Options" numeric value='4' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Futures" numeric value='8' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Forex" numeric value='16' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span className="inline-elements">
                                                                        <IbxCheckbox label="Other" numeric clearOther='InstrumentsOther' value='1024' name='InstrumentsType'/>
                                                                        {values.InstrumentsType.map(item => item.toString()).indexOf('1024') !== -1 &&
                                                                        <span className="margin-left-10">
                                                                            <IbxInput errors={errors}
                                                                                      touched={touched}
                                                                                      class={`int-small`}
                                                                                      name='InstrumentsOther'/>
                                                                        </span>
                                                                        }
                                                                    </span>

                                                                </div>
                                                            </div>

                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Class</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="Professional" value='1' name='Class'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="Non-professional" value='2' name='Class'/>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </React.Fragment>
                                                    :
                                                    values.Type == '3' &&

                                                        <React.Fragment>
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Contract number</span></div>
                                                                <div className="flex-item">
                                                                    <Field name='ContractNumber'
                                                                           render={({form, field}) => (
                                                                               <IbxSelect
                                                                                   class="st-small"
                                                                                   options={state.contractsList}
                                                                                   isDisabled={state.formDisabled}
                                                                                   {...field}
                                                                                   labelKey='text'
                                                                                   // valueKey={''}
                                                                                   selectedOptions={values.ContractNumber}
                                                                                   changeHandler={(item) => form.setFieldValue(field.name, item.value)
                                                                                   }
                                                                               />
                                                                           )}
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Deposit size (USD)</span></div>
                                                                <div className="flex-item">
                                                                    <IbxInput errors={errors}
                                                                              touched={touched}
                                                                              class={`int-small`}
                                                                              name='DepositSize'/>
                                                                </div>
                                                            </div>
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Margin</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="No" value='false' name='Margin'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="Yes" value='true' name='Margin'/>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Tariff</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="Standard" value='1' name='TerminalTariffs'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="By volume" value='2' name='TerminalTariffs'/>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Instruments types</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxCheckbox label="Stocks" numeric value='1' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Bonds" numeric value='2' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Options" numeric value='4' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Futures" numeric value='8' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxCheckbox label="Forex" numeric value='16' name='InstrumentsType'/>
                                                                    </span>
                                                                    <span className="inline-elements">
                                                                        <IbxCheckbox label="Other" numeric clearOther='InstrumentsOther' value='1024' name='InstrumentsType'/>
                                                                        {values.InstrumentsType.map(item => item.toString()).indexOf('1024') !== -1 &&
                                                                        <span className="margin-left-10">
                                                                            <IbxInput errors={errors}
                                                                                      touched={touched}
                                                                                      class={`int-small`}
                                                                                      name='InstrumentsOther'/>
                                                                        </span>
                                                                        }
                                                                    </span>

                                                                </div>
                                                            </div>

                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Class</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="Professional" value='1' name='Class'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="Non-professional" value='2' name='Class'/>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="flex-row flex align-items-center">
                                                                <div className="flex-item"><span>Scenario</span></div>
                                                                <div className="flex-item">
                                                                    <span>
                                                                        <IbxRadio label="Green" value='1'
                                                                                  name='Scenario'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="Yellow" value='2'
                                                                                  name='Scenario'/>
                                                                    </span>

                                                                    <span>
                                                                        <IbxRadio label="Red 1" value='3'
                                                                                  name='Scenario'/>
                                                                    </span>
                                                                    <span>
                                                                        <IbxRadio label="Red 2" value='4'
                                                                                  name='Scenario'/>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </React.Fragment>
                                                    }
                                                </div>
                                            </div>

                                            <div className="block-row">
                                                <p className="block-title">Documents</p>
                                                <div className="flex-table custom-table margin-bottom-24">
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item">
                                                            <span>No documents attached</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <IbxUploader isMultiple accept='.pdf,.png,.jpg,.jpeg,.msg' Files={values.Files} name='Filess'/>
                                            </div>

                                            <div className="block-row">
                                                <p className="block-title">History</p>
                                                <div className="flex-table">
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>CM</span></div>
                                                        <div className="flex-item">
                                                            <Field type="text"
                                                                   name='ClientManager'
                                                                   render={({form, field}) => (
                                                                       <IbxSelect
                                                                           class="st-small"
                                                                           options={state.cms}
                                                                           key={state.selectedApplicant.Code}
                                                                           selectedOptions={values.ClientManager}
                                                                           isDisabled={state.formDisabled}
                                                                           {...field}
                                                                           labelKey='text'
                                                                           // valueKey={''}
                                                                           changeHandler={(item) => form.setFieldValue(field.name, item.value)}
                                                                       />
                                                                   )}
                                                            />


                                                        </div>
                                                    </div>
                                                    {!newApplicant &&
                                                    <>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Created By</span></div>
                                                        <div className="flex-item"><span>{state.selectedApplicant['CreatedBy']}</span></div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Opening Date</span></div>
                                                        <div className="flex-item"><span>{dateFilter(state.selectedApplicant['CreatedDate'])}</span></div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Updated Date</span></div>
                                                        <div className="flex-item"><span>{dateFilter(state.selectedApplicant['UpdatedDate'])}</span></div>
                                                    </div>
                                                    <div className="flex-row flex align-items-center">
                                                        <div className="flex-item"><span>Updated By</span></div>
                                                        <div className="flex-item"><span>{state.selectedApplicant['UpdatedBy']}</span></div>
                                                    </div>
                                                    </>
                                                    }
                                                </div>
                                            </div>

                                            <div className="block-row">
                                                <Comments values={values} editMode={true} errors={errors} touched={touched}/>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="col-3">
                                        <span className="label">Application status</span>
                                        <Field name='Status'
                                               render={({form, field}) => (
                                                   <IbxSelect
                                                       options={state.statusOptions}
                                                       key={state.selectedApplicant.Code}
                                                       selectedOptions={values.Status}
                                                       isDisabled={newApplicant || state.formDisabled}
                                                       {...field}
                                                       labelKey='text'
                                                       // valueKey={''}
                                                       changeHandler={(item) => form.setValues({...values, [field.name]: item.value, ClosingRejectionReason: ''})}
                                                   />
                                               )}
                                        />
                                        {
                                            (values.Status == '5' || values.Status == '6') &&
                                            <IbxInput class={`margin-top-24`}
                                                      errors={errors}
                                                      touched={touched}
                                                      name={`ClosingRejectionReason`}
                                                      placeholder="Reason..."/>
                                        }

                                        {/*Todo for failed add 'failed' class */}
                                        <div className="notification-message">
                                            <span>Your application is successfully saved</span>
                                            <span className="icon"><i className="icon-close"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="page-bottom flex justify-flex-end">
                                <div className="buttons flex align-items-center">
                                    <Link to="/brokerage/applications">
                                        <IbxButton class="btn-outline" type='button' btnText="Cancel"/>
                                    </Link>
                                    <IbxButton class="btn-grey-dark btn-left-icon" disabled={isSubmitting} btnText="Save" iconClass="icon-save"/>
                                </div>
                            </div>
                                </>
                            }
                            </fieldset>
                        </Form>
                    )}
                </Formik>
                <HistoryModal data={state.historyData} modalIsOpen={state.modalIsOpen} closeModal={closeModal}/>
            </React.Fragment>

    );
}

export default withRouter(ApplicantsForm);
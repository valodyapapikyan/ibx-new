import React, {useEffect, useState} from 'react';
import { connect } from 'react-redux'
import {getClassifications, logoutHandler} from "../../../../actions";
import {Route, Link, Switch, NavLink } from "react-router-dom";
import IbxButton from "../button";


function  IbxSidebar({match, logOut, rootUrl}) {
    const [activeRouteName, setActiveRoute] = useState<string>( 'none');

    function HandleLogOut() {
        logOut({url: `${rootUrl}/Account/Logout`});
    }

    function setActive(match) {
        if (!match) {
            return false
        } else {
            setActiveRoute(match.url);
        }
    }
    return (
       <div className="sidebar">
           <div className="top">Ameria Brok</div>
           <div className="inner scroll">
               <ul>
                   <li>
                       <NavLink to={`${match.url}/applications`} isActive={setActive}>
                           <span className="icon">
                               <img src={~(`${match.url}/applications`).indexOf(activeRouteName) ? '/assets/icons/applications_active.svg' : '/assets/icons/applications_inactive.svg'} />
                           </span>
                           <span>Applications</span>
                       </NavLink>
                   </li>
                   <li>
                       <NavLink to={`${match.url}/customers`} isActive={setActive}>
                          <span className="icon">
                               <img src={~(`${match.url}/customers`).indexOf(activeRouteName) ? '/assets/icons/customer_active.svg' : '/assets/icons/customer_inactive.svg'} />
                           </span>
                           <span>Customers</span>
                       </NavLink>
                   </li>
                   <li>
                       <NavLink to={`${match.url}/orders`} isActive={setActive}>
                           <span className="icon">
                               <img src={~(`${match.url}/orders`).indexOf(activeRouteName) ? '/assets/icons/orders_active.svg' : '/assets/icons/orders_inactive.svg'} />
                           </span>
                           <span>Orders</span>
                       </NavLink>
                   </li>
                   <li>
                       <NavLink to={`${match.url}/reference-book`} isActive={setActive}>
                           <span className="icon">
                               <img src={~(`${match.url}/reference-book`).indexOf(activeRouteName) ? '/assets/icons/book_active.svg' : '/assets/icons/book_inactive.svg'} />
                           </span>
                           <span>Reference book</span>
                       </NavLink>
                   </li>
                   <li>
                       <NavLink to={`${match.url}/reports`} isActive={setActive}>
                           <span className="icon">
                               <img src={~(`${match.url}/reports`).indexOf(activeRouteName) ? '/assets/icons/report2_active.svg' : '/assets/icons/report2_inactive.svg'} />
                           </span>
                           <span>Reports</span>
                       </NavLink>
                   </li>
                   <li>
                       <NavLink to={`${match.url}/ui-kits`} isActive={setActive}>
                           <span>Ui</span>
                       </NavLink>
                   </li>
               </ul>
               <ul>
                   <li>
                       <NavLink to={`${match.url}/settings`} isActive={setActive}>
                           <span className="icon">
                               <img src={~(`${match.url}/settings`).indexOf(activeRouteName) ? '/assets/icons/setting_active.svg' : '/assets/icons/setting_inactive.svg'} />
                           </span>
                           <span>Settings</span>
                       </NavLink>
                   </li>
                   <li>
                       <NavLink to={`${match.url}/help`} isActive={setActive}>
                           <span className="icon">
                               <img src={~(`${match.url}/help`).indexOf(activeRouteName) ? '/assets/icons/help_active.svg' : '/assets/icons/help_inactive.svg'} />
                           </span>
                           <span>Help</span>
                       </NavLink>
                   </li>
               </ul>
           </div>
           <div className="bottom">
               <IbxButton class="btn-left-icon btn-simple red selenium-sign-out"  btnText="Sign Out" image='/assets/icons/logout_inactive.svg' handleClick={HandleLogOut} />
           </div>
       </div>
    )
}

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
    }
};

export default connect(mapStateToProps, {logOut: logoutHandler})(IbxSidebar);
import React from 'react';
import IbxInput from "../input";
import {Formik} from "formik";


function  IbxHeader() {
    return (
       <header className="flex align-items-center space-between">
           <div className="search-box">
               <Formik initialValues={{}} onSubmit={()=> console.log()}>
               <IbxInput  class="int-left-icon" name="search"  icon="icon-search" placeholder="Search" />
               </Formik>
           </div>
           <div className="user-box">
               <span className="user-img bg-cover" style={{backgroundImage: 'url(/assets/images/user.jpg)'}}></span>
               <span>Armen Sargsyan</span>
           </div>
       </header>
    )
}

export default IbxHeader;
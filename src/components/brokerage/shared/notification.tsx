import React, {useEffect} from 'react';
import ReactDOM from "react-dom";

function IbxNotification({className, message}) {

    const portalRoot = document.getElementById('notifications') || document.createElement('div');
    const portalContainer = document.createElement('div');
console.log(portalRoot);
    useEffect(() => {
        portalRoot.append(portalContainer);

        return () => {
            portalRoot.removeChild(portalContainer)
        }
    }, []);

    return (
        ReactDOM.createPortal(
            <div className={`notification-message ${className}`}>
                <span>{message}</span>
                <span className="icon"><i className="icon-close"></i></span>
            </div>, portalContainer)

    )
}

export default IbxNotification;
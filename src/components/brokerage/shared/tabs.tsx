import React from 'react';

export default function(props) {
    const { tabs, handleClick, activeTab } = props;
    return (
        <div className="tab-links flex">
            {tabs.map((item, index )=>
                <span className={`tab-link ${item === activeTab && 'tab-link--selected prev-default'}`} key={index} onClick={() => handleClick(item)}>
                    {item}
                </span>
            )}
        </div>
    )
}
import React from 'react';
import Select from 'react-select';

interface SelectInterface {
    options: {}[];
    class?: string;
    labelKey?: string;
    valueKey?: string;
    selectedOptions?: {} | null;
    name?: string,
    disabledOptions?: [];
    isDisabled?: boolean;
    multiSelect?: boolean;
    changeHandler(x: any): any;
}

function IbxSelect(obj: SelectInterface) {

    //Todo Could Have Complications Handle Them Here;
    const value = obj.options.filter(item => item[obj.valueKey || 'value'] === obj.selectedOptions);

    return (
        <Select
            className={`ibx-selectbox ${obj.class} ${obj.isDisabled ? 'is-disabled':''}`}
            name={obj.name}
            getOptionLabel={option => option[obj.labelKey || 'label']}
            getOptionValue={option => option[obj.valueKey || 'value']}
            value={value}
            onChange={(item) => obj.changeHandler(item)}
            options={obj.options}
            isDisabled={obj.isDisabled}
            isMulti={obj.multiSelect}
            defaultMenuIsOpen={false}
        />
    );
}

export default IbxSelect;
import React from 'react';


function  IbxButton(props) {
    return (
        <button type={props.type || "submit"} disabled={props.isDisabled} onClick={props.handleClick}
                className={"ibx-button " +  props.class}>
            {props.iconClass &&
                <i className={props.iconClass}></i>
            }
            {props.image &&
                <img src={props.image}/>
            }
            <span className="btn-text">{ props.btnText }</span>
        </button>
    )
}

export default IbxButton;
import React from 'react';

function  IbxStatus(props) {
    return (
        <span className={"ibx-status " + props.class}>{props.text}</span>
    )
}

export default IbxStatus;
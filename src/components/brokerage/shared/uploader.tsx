import React, {useEffect,useState} from 'react';
import {Field, FieldArray} from "formik";
import IbxButton from "./button";
import axios from "axios";
import { connect } from 'react-redux';
import {overrideEventDefaults} from "../../../helpers/methods";

function  IbxUploader(props) {
    // const [state, setState] = useState({isLoading: false, fileUploaded: false});
    const [state, setState] = useState({
        dragging: false,
    });

    useEffect(() => {

        /**
         * It’s important to note that I opted to override the default behavior of dragover and drop on the entire window.
         * The event listeners for those are added and removed in the component lifecycle hooks.
         * I did this so that if a user accidentally drops the file outside of the UI,
         * it won’t open the file in the browser, since that can be a bit annoying.
         * */
        window.addEventListener("dragover", (event: Event) => {
            overrideEventDefaults(event);
        });
        window.addEventListener("drop", (event: Event) => {
            overrideEventDefaults(event);
        });

        return () => {
            window.removeEventListener("dragover", overrideEventDefaults);
            window.removeEventListener("drop", overrideEventDefaults);
        }

    }, []);

    let dragCounter = 0;

    let arr = props.Files;

    const handleFiles = ({e, form}) => {
        let files = e.target && e.target.files || [];

        for (let i = 0; i < files.length; i++) {
            fileUpload(files[i]).then(response => {
                if(response.data.Success) {
                    arr.push({Name: response.data.Data, State: 2});
                    form.setFieldValue('Files', arr);
                } else {
                    alert(response.data.Message);
                }
            }).catch(() => console.error("Not supported format, Can’t Upload File!!!"));
        }
    };

    const fileUpload = (file) => {
        const url = `${props.rootUrl}/File/UploadFile`;
        const formData = new FormData();
        formData.append('file', file);
        const config = {
            headers: {
                'content-type': 'multipart/form-data',
            }
        };
        return axios.post(url, formData, config)
    };

    const downloadFile = (file) => {
        axios({
            url:`${props.rootUrl}/File/GetFile/${file.Id}`,
            method: 'get',
            responseType: 'blob',
        }).then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', file.Name);
                document.body.appendChild(link);
                link.click();
            });
    };

    const handleDrag = (e) => {
        overrideEventDefaults(e);
    };

    const handleDragIn = (e) => {
        overrideEventDefaults(e);
        dragCounter++;
        if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
            setState((prevState => ({...prevState, dragging: true})))
        }
    };

    const handleDragOut = (e) => {
        overrideEventDefaults(e);
        dragCounter--;
        if (dragCounter === 0) {
            setState((prevState => ({...prevState, dragging: false})))
        }
    };

    const handleDrop = (e, form) => {
        overrideEventDefaults(e);
        setState((prevState => ({...prevState, dragging: false})));
        if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
            handleFiles({e: {target: e.dataTransfer}, form});
            e.dataTransfer.clearData();
            dragCounter = 0
        }
    };

    return (
        <>
            <Field name={props.name}>
                {({field, form}) => {
                    return <div
                        style={{position: 'relative'}}
                        onDrop={(event) => handleDrop(event, form)}
                        onDragEnter={handleDragIn}
                        onDragOver={handleDrag}
                        onDragLeave={handleDragOut}
                    >
                        {state.dragging &&
                        <div
                            style={{
                                border: 'dashed grey 2px',
                                backgroundColor: 'rgba(255,255,255,.8)',
                                position: 'absolute',
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 0,
                                zIndex: 9999
                            }}
                        >
                            <div
                                style={{
                                    position: 'absolute',
                                    top: '50%',
                                    right: 0,
                                    left: 0,
                                    textAlign: 'center',
                                    color: 'grey',
                                    fontSize: 36
                                }}
                            >
                            </div>
                        </div>
                        }
                        <div className="ibx-uploader">
                            <div className='inner'>
                                <div className="flex align-items-center">
                                    <span className="icon">
                                        <i className="icon-upload"></i>
                                    </span>
                                    <span className="text">
                                        Drag and drop document here
                                        or just click for
                                        <label>
                                            <input {...field}
                                                   onChange={e => {
                                                       handleFiles({e, form});
                                                   }}
                                                   multiple={props.isMultiple}
                                                   accept={props.accept}
                                                   type="file"/>
                                            browse
                                        </label>
                                        file
                                     </span>
                                </div>
                            </div>
                        </div>
                    </div>
                }}
            </Field>


            <FieldArray name='Files' render={({remove, push, form}) => (
                <>
                    {props.Files && props.Files.length > 0 && props.Files.map((item, index) => (
                        <div className="uploaded-item flex space-between align-items-center" key={item.State + item.Name}>
                            {item.State === 3 ?
                                <>
                                    <del>
                                        <a className="item-title ellipsis" onClick={() => downloadFile(item)}>
                                            {item.Name}
                                        </a>
                                    </del>
                                    {/* Undo Functionality!!!
                                    <button type='button' onClick={() => {
                                        form.setFieldValue(`Files.${index}`, {...item, State: 1})
                                    }}>
                                        Undo
                                    </button>*/}
                                </>
                            :
                                <>
                                    {item.State !== 2 ?
                                        
                                        <a className="item-title ellipsis" onClick={() => downloadFile(item)}>
                                            {item.Name}
                                        </a>
                                        :
                                        <span className="item-title ellipsis">
                                            {item.Name}
                                        </span>
                                    }

                                    <IbxButton class="btn-icon simple" handleClick={() => {
                                        if(item.State === 2) {
                                            remove(index);
                                        } else {
                                            form.setFieldValue(`Files.${index}`, {...item, State: 3})
                                        }
                                    }} type='button' iconClass="icon-trash"/>
                                </>
                                }

                        </div>
                    ))}
                </>
            )}/>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
    }
};

export default connect(mapStateToProps, null)(IbxUploader);
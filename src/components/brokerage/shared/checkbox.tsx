import React from 'react';
import {Field} from "formik";

function  IbxCheckbox(props) {
    return (
        <Field name={props.name}>
            {({ field, form }) => (
                <label className={"ibx-checkbox " + props.class}>
                    {/*{props.value}*/}
                    <input
                        type="checkbox"
                        {...field}
                        disabled={props.isDisabled}
                        checked={field.value.includes(props.numeric ? parseInt(props.value) : props.value)}
                        onChange={() => {
                            const propsValue = props.numeric ? parseInt(props.value) : props.value;
                            if (field.value.includes(propsValue)) {
                                const nextValue = field.value.filter(
                                    value => value !== propsValue
                                );
                                form.setFieldValue(props.name, nextValue);
                                if(props.clearOther) {
                                    form.setFieldValue(props.clearOther, '');
                                }
                            } else {
                                const nextValue = field.value.concat(propsValue);
                                form.setFieldValue(props.name, nextValue);
                            }
                        }}
                    />
                    <span><i className="icon-check"></i></span>
                    { props.label &&
                    <span className="item-text">{props.label}</span>
                    }
                </label>
            )}
        </Field>
    )
}

export default IbxCheckbox;
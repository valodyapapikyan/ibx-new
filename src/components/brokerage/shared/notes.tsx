import React from "react";
import IbxButton from "./button";
import {Field, FieldArray} from "formik";
import {ArrayErrors, dateToYMD} from "../../../helpers/methods";
import {dateFilter} from "../../../helpers/filters";
import IbxInput from "./input";

export default ({values, errors, touched, editMode}) => (
    <FieldArray name='Comments' render={({remove, push, form}) => (
        <>
            <div className="block-top flex align-items-center space-between">
                <div><p className="block-title">Notes</p></div>
                {editMode && <div>
                    <IbxButton handleClick={() => push({Message: '', State: 2})}
                               type='button' id="Selenium-Add-Note-Button-Id" class="btn-left-icon not-full-width btn-outline"
                               btnText="Add Note" iconClass="icon-plus"/>
                </div>}
            </div>
            <div className="flex-table custom-table notes-table">
                {values.Comments.length > 0 && values.Comments.map((item, index) => (
                    <div className="flex-row flex align-items-center" key={index}>
                        <div className="flex-item">

                            <>
                                {!editMode ?

                                    <span className="comment-text">

                                        {item.Message}

                                    </span>
                                    :
                                    item.State === 3 ?
                                        <del key={item.State}>
                                            <span className="comment-text">

                                                {item.Message}

                                            </span>
                                        </del>

                                        :
                                    <>
                                        <div className="flex align-items-center space-between">
                                        <IbxInput
                                            label="Note"
                                            id="selenium-note-id"
                                            class="max-width-initial"
                                            name={`Comments.${index}.Message`}
                                            placeholder="Type your note"/>
                                        <IbxButton iconClass="icon-trash" handleClick={() => {
                                            if(item.State === 2) {
                                                remove(index);
                                            } else {
                                                form.setFieldValue(`Comments.${index}`, {...item, State: 3})
                                            }
                                        }} class="ibx-button btn-icon simple margin-left-10 margin-top-24" type='button'/>
                                        </div>

                                    </>
                                }
                            </>
                            <p className="grey-middle-light">{item.CreatedDate ? dateFilter(item.CreatedDate) + ' by ' + item.Creator :
                                `${dateToYMD(new Date())}`}</p>

                            <ArrayErrors errors={errors} name='Comments' touched={touched} index={index}/>
                        </div>
                    </div>
                ))}
            </div>
        </>
    )}/>
)
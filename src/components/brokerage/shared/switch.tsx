import React from 'react';

function IbxSwitch(props) {
    return (
        <label className={"ibx-switch flex align-items-center " + props.class}>
            <span className="item-text" >{props.label}</span>
            <input type="checkbox"/>
            <span></span>
        </label>
    )
}

export default IbxSwitch;
import React from 'react';
import {Field} from "formik";


function  IbxTextarea(props) {
    return (
        <Field {...props} render={() => (
            <label className="ibx-textarea">
                { props.label &&
                    <span className="label" >{props.label}</span>
                }
                <textarea placeholder={props.placeholder} disabled={props.isDisabled} />
            </label>
        )} />
    )
}

export default IbxTextarea;
import React from "react";
import IbxSelect from "./select";

export default function ({name,pageSize, handlePageSize}) {
    const options = [
        {
            value: 10,
            label: 10,
        },
        {
            value: 20,
            label: 20,
        },
        {
            value: 50,
            label: 50,
        },
    ];

    return <IbxSelect
               name={name}
               class='small-width'
               selectedOptions={pageSize}
               options={options}
               changeHandler={handlePageSize}/>
}
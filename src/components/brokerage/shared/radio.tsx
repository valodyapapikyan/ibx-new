import React from 'react';
import {Field} from "formik";

function  IbxRadio(props) {
    return (
        <Field name={props.name} value={props.value}>
            {({ field }) => (
                <label className={"ibx-radio " + props.class}>
                    <input
                        type="radio"
                        checked={field.value.toString() === props.value.toString()}
                        {...field}
                        value={props.value}
                    />
                    <span><i className="icon-check"></i></span>
                    { props.label &&
                    <span className="item-text">{props.label}</span>
                    }
                </label>
            )}
        </Field>
    )
}

export default IbxRadio;
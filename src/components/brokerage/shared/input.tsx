import React, {useEffect, useState} from 'react';
import {ErrorMessage, Field} from "formik";
import {isCurrency} from "../../../helpers/filters";

function IbxInput(props) {
    const touched = props.touched || {};
    const errors = props.errors || {};

    const [state, setState] = useState({inputFocused: false, inputTransformedValue: '', initialized: false});

    const formatNumber = (field, form) => {
        let {value} = field;

        value = value.toNumberFormatString();

        if(form) {
            form.setFieldValue(field.name, value.replace(/,/g, ''));
        }

        setState((prevState => {
            return {
                inputFocused: false,
                inputTransformedValue: value,
                initialized: true,
            }
        }))
    };

    const deFormatNumber = () => {
        setState((prevState => {
            return {
                ...prevState,
                inputFocused: true,
            }
        }))
    };


    const input = (field = {name: '', value: '',}, form = {setFieldValue: (name, value) => void 0}) => {

        if(isCurrency(field.name) && field.value && !state.initialized) {
			formatNumber(field, null);
        }

        return <label
            className={`ibx-input ${props.class} ${errors[field.name] && touched[field.name] && 'is-invalid'}`}>
            {props.label &&
            <span className="label">{props.label}</span>
            }
            <div className="inner">
                <i className={props.icon}></i>
                {isCurrency(field.name) ?
                    <>
                        <input type={props.type} placeholder={props.placeholder}
                               disabled={props.isDisabled}
                               id={props.id}
                               className={`currency-input ${!state.inputFocused && 'unVisible'}`}
                               {...field}
                               onBlur={() => formatNumber(field, form)}
                               onFocus={() => deFormatNumber()}
                        />
                        {!state.inputFocused &&
                            <span className='currency'>{state.inputTransformedValue}</span>
                        }
                    </>
                    :
                    <input type={props.type} placeholder={props.placeholder}
                           disabled={props.isDisabled}
                           id={props.id}
                           name={props.name}
                           {...field}
                    />
                }
                <ErrorMessage
                    name={props.name}
                    component="span"
                    className="error-message"
                />
            </div>


        </label>
    };

    return (
        props.withOutFormik ?
            input()
            :
        <Field name={props.name}>
            {({field, form}) => (
                input(field, form)
            )}
        </Field>
    )
}

export default IbxInput;
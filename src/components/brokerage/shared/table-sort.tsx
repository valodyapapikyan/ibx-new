import React from 'react';


function  IbxTableSort({sort, handleSort, _key}) {
    return (
        <span className="table-sort" onClick={() => handleSort(_key)}>
            <span className={(sort.key === _key && sort.value === 'ASC') ? 'active' : ''}><i className="icon-polygon-up"></i></span>
            <span className={(sort.key === _key && sort.value === 'DESC') ? 'active' : ''}><i className="icon-polygon-down"></i></span>
        </span>
    )
}

export default IbxTableSort;
import React from 'react';
import Pagination from "react-js-pagination";
import IbxResultPPSelect from "./results-pp-select";

export default function ({activePage, pageSize, total, rows, handlePageChange, handleResultsPP}) {
    return (
        <div className="pagination-row flex align-items-center space-between">
            <div className="width-percent-50 inline-elements">
                <span>
                   {/*Todo review*/}
                    <IbxResultPPSelect
                        name="PageSize"
                        pageSize={pageSize}
                        handlePageSize={handleResultsPP}/>
                </span>
                <span className="grey-middle margin-left-15">Customers Details (Showing&nbsp;
                    {(activePage - 1) * pageSize + 1} - {
                        (activePage - 1) * pageSize + rows} of {total}
                    &nbsp;records)
                </span>
            </div>
            <div className="width-percent-50 text-right">
                <Pagination
                    activePage={activePage}
                    itemsCountPerPage={pageSize}
                    totalItemsCount={total}
                    pageRangeDisplayed={5}
                    onChange={(pageNumber) => handlePageChange(pageNumber)}
                />
            </div>
        </div>
    )
}
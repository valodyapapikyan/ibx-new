import React, {useEffect, useState} from 'react';
import {confirmAlert} from "react-confirm-alert";

interface IConfirm {
    title: string;
    message: string;
    onBtnConfirm: () => void;
    onBtnCancel?: () => void;
    btnConfirmTxt?: string;
    btnCancelTxt?: string
}

function IbxConfirm({title, message, onBtnConfirm, onBtnCancel = () => void 0, btnConfirmTxt = 'Yes', btnCancelTxt = 'No'}: IConfirm) {
    confirmAlert({
        title,
        message,
        buttons: [
            {
                label: btnConfirmTxt,
                onClick: onBtnConfirm
            },
            {
                label: btnCancelTxt,
                onClick: onBtnCancel
            }
        ]
    })
}

export default IbxConfirm;
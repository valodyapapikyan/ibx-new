import React, {useEffect, useState} from 'react';
import { Link, withRouter} from "react-router-dom";
import axios from "axios";
import {transformContracts} from "../../../../helpers/table-transform";
import IbxTableSort from "../../shared/table-sort";
import IbxStatus from "../../shared/status";
import getClassicationValues from "../../../../helpers/classifications";

export default getClassicationValues(withRouter(function({rootUrl, clientId, match, location, getBetterClassificationVal}) {

    const [state, setState] = useState({
        isLoading: true,
        data: {body: [] as any[]} as any,
        subContent: [] as string[],
    });

    useEffect(() => {
        axios({
            method: 'get',
            url: `${rootUrl}/Contract/GetClientContracts?clientCode=${clientId}`,
        }).then(response => {
            let data = response.data.Data as any[];
            (async () => {
                for (let i = 0; i < data.length; i++) {
                    const datum = data[i];
                    if(datum.Type === 1) {
                        await axios.get(`${rootUrl}/Terminal/GetTerminalsByBrokerageContractNumber?contractNumber=${datum.ContractNumber}`)
                            .then(response2 => {
                                datum.hasTerminal = !!response2.data.Data.length;

                                datum.ClientRegistrationCode = response2.data.Data.filter(item => !!item.ClientRegistrationCode)
                                    .map(item => item = item.ClientRegistrationCode).toString();

                                data.splice(++i, 0, ...(response2.data.Data.map(item => {
                                    item.isSubContent = true;
                                    item.fictionalType = 'Terminal Agreement';
                                    return item;
                                })));
                            });
                    }
                }
                setState((prevState => {
                    return {
                        ...prevState,
                        isLoading: false,
                        data: transformContracts(data),
                    }
                }));
            })();

        }).catch(error => {
            alert(`Something Went Wrong!! Try Again Later!!`);
        });
    }, []);

    const handleSubContent = (contractNumber: string) => {

        if(~state.subContent.indexOf(contractNumber)) {

            setState((prevState => {
                prevState.subContent.splice(prevState.subContent.indexOf(contractNumber), 1);
                return {
                ...prevState,
            }}))

        } else {
            setState((prevState => ({
                ...prevState,
                subContent: [...prevState.subContent, contractNumber],
            })))
        }
    };

    const GetTerminalsByBrokerageContractNumber = (contractNumber) => {
        axios({
            method: 'get',
            url: `${rootUrl}/Terminal/GetTerminalsByBrokerageContractNumber?contractNumber=${contractNumber}`,
        }).then(response2 => {
            console.log(response2);
        })
    };

    return (
        state.isLoading ?
            <div className="loading-box">
                <span> Loading... </span>
            </div>
            :
            <>
            <div className="ibx-table-container">
                <div className="ibx-table-row head">
                    {(Object.keys(state.data.header || {}) || []).map((item, index) => (
                            <div className={`ibx-table-cell`} key={index}>
                                <span>{state.data.header[item]}</span>
                            </div>
                        ))}
                </div>
                {state.data.body && state.data.body.length ?
                    state.data.body.map(item => {
                        item.isVisible = state.subContent.filter(_item => ~item.ContractNumber.indexOf(_item)).length || !item.isSubContent;
                        return item
                    }).filter(item => item.isVisible)
                        .map((item, index) => (
                        <div className={`ibx-table-row ${item.isSubContent ? 'is-sub-row' : ''}`} key={index}>
                            {Object.keys(state.data.header).map((_item, _index) => (
                                    <div className={`ibx-table-cell`} key={_index}>
                                        {
                                            _item === 'ContractNumber' ?
                                                <>
                                                    <div className="accordion flex align-items-center">
                                                        <Link className="green-link" to={
                                                            {
                                                                pathname: `${match.url}/${item[_item].replace(/\//g, '-')}`,
                                                                search: location.search,
                                                            }
                                                        }>{item[_item]}</Link>

                                                        {item['hasTerminal'] &&
                                                        <span className="right-part">
                                                            <button type='button'
                                                                    onClick={() => handleSubContent(item['ContractNumber'])}
                                                                    className={~state.subContent.indexOf(item['ContractNumber']) ? 'active' : ''}>
                                                                <i className={~state.subContent.indexOf(item['ContractNumber']) ? 'icon-polygon-up' : 'icon-polygon-down'}></i>
                                                            </button>
                                                         </span>
                                                        }

                                                    </div>
                                                </>
                                                :
                                                _item === 'Status' ?
                                                    <IbxStatus text={getBetterClassificationVal(item.isSubContent ? 'TERMINALS_STATUS' : 'CONTRACTS_STATUS', item[_item])}
                                                               class={item[_item] === 'New' ? 'st-new' :
                                                                   item[_item] === 'Approved' ? 'approved' :
                                                                       item[_item] === 'Waiting for agreement' ? 'waiting' :
                                                                           item[_item] === 'In review' ? 'in-review':
                                                                               item[_item] === 'Rejected' ? 'rejected':
                                                                                   item[_item] === 'Closed' ? 'closed' : 'active'}/>
                                                    :
                                                _item === 'Type' ?
                                                    item.isSubContent ?
                                                        item.fictionalType :
                                                    getBetterClassificationVal('CONTRACTS_TYPE', item[_item])
                                                    :

                                                _item === 'DepositSize' ?
                                                    item[_item].toNumberFormatString()
                                                    :
                                                    item[_item] || '-'
                                        }
                                    </div>
                                ))}
                        </div>
                        ))
                    :
                    ''
                }
            </div>

            {state.data.body && !state.data.body.length &&
                <div className="no-data">
                    <img src="/assets/images/no-items.png"/>
                    <p>Sorry, No result found :(</p>
                </div>
            }
        </>

        )
    }
))


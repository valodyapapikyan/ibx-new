import React from "react";
import Modal from 'react-modal';
import IbxSelect from "../../../shared/select";
import IbxButton from "../../../shared/button";
import {ErrorMessage, Field, Form, Formik} from "formik";
import IbxCheckbox from "../../../shared/checkbox";

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

export default function({closeModal, modalIsOpen, handleSubmit, data = []}) {

    const [head, body] = [{Account: 'Account Number', CurrencyText: 'Currency'}, data];
    const keys = Object.keys(head);

    const checkDisabled = (valuesBodyRow, bodyRow) => {
        for(let i = 0; i < valuesBodyRow.length; i++) {
            if((valuesBodyRow[i].Currency === bodyRow.Currency) && (valuesBodyRow[i].Account !== bodyRow.Account)) {
                return true;
            }
        }
        return false;
    };

    return (
        <Modal
            ariaHideApp={false}
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Assigning Accounts Modal"
        >
            <Formik
                enableReinitialize
                initialValues={{
                    bodyRow: [],
                    Account: [],
                }}
                onSubmit={(values, { setSubmitting }) => {
                    handleSubmit({
                        values,
                    });
                    setSubmitting(false);
                }}>
                {({values, errors, touched, handleChange, isSubmitting}) => (

                    <div className="ibx-modal available-account-modal">
                <div className="modal-top flex align-items-center">
                    <span className="mardotoBold uppercase">Available accounts</span>
                    <span className="closeBtn">
                        <IbxButton handleClick={closeModal}
                                   class="btn-icon simple" iconClass="icon-close"/>
                    </span>
                </div>
                <Form>
                    <div className="modal-content">
                        <div className="simple-table simple-table">
                            <div className="table-row table-row-head">
                                <div className="table-cell small"></div>
                                {keys.map(item => (
                                    <div key={item} className={`table-cell ${item === 'Account' && 'large'}`}><span>{head[item]}</span></div>
                                ))}
                            </div>
                            {body.length ? body.map(bodyRow => (
                                <div className="table-row" key={bodyRow['Account']}>
                                    <div className="table-cell small">
                                        <Field name={'Account'} value={bodyRow['Account']}>
                                            {({ field, form }) => (
                                                <label className={"ibx-checkbox"}>
                                                    <input
                                                        type="checkbox"
                                                        {...field}
                                                        disabled={checkDisabled(values.bodyRow, bodyRow)}
                                                        checked={field.value.includes(bodyRow[field.name])}
                                                        onChange={() => {
                                                            const propsValue = bodyRow[field.name];

                                                            if (field.value.includes(propsValue)) {
                                                                const nextValue = field.value.filter(
                                                                    value => value !== propsValue
                                                                );

                                                                const _nextValue = values.bodyRow.filter(
                                                                    value => value[field.name] !== propsValue
                                                                );

                                                                form.setFieldValue(field.name, nextValue);
                                                                form.setFieldValue('bodyRow', _nextValue);
                                                            }
                                                            else {
                                                                const nextValue = field.value.concat(propsValue);
                                                                const _nextValue = values.bodyRow.concat(bodyRow);
                                                                form.setFieldValue(field.name, nextValue);
                                                                form.setFieldValue('bodyRow', _nextValue);
                                                            }
                                                        }}
                                                    />
                                                    <span><i className="icon-check"></i></span>
                                                </label>
                                            )}
                                        </Field>
                                    </div>
                                    {keys.map((headKey, index) => (
                                        <div key={bodyRow['Account'] + index} className="table-cell large">{bodyRow[headKey]}</div>
                                    ))}
                                </div>
                            )) : ''}
                        </div>
                    </div>
                    <div className="modal-bottom text-right inline-elements">
                        <span>
                            <IbxButton class="btn-outline btn-small" type='button' btnText="Cancel" handleClick={closeModal}/>
                        </span>

                        <span>
                            <IbxButton class="btn-grey-dark btn-small min-width-164 margin-left-10"
                                       isDisabled={!body.length || isSubmitting || !values.bodyRow.length}
                                       btnText="Assign Selected"/>
                        </span>
                    </div>
                </Form>
            </div>
                    )}
            </Formik>
        </Modal>
    );
}
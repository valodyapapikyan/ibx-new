import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import Modal from 'react-modal';
import IbxSelect from "../../../shared/select";
import IbxButton from "../../../shared/button";
import {ErrorMessage, Field, FieldArray, Form, Formik} from "formik";
import axios from "axios";
import {convertToArray, convertToText, dateFilter} from "../../../../../helpers/filters";
import {ArrayErrors, dateToYMD, detectData, generateContractPassword, isSet} from "../../../../../helpers/methods";
import IbxRadio from "../../../shared/radio";
import IbxCheckbox from "../../../shared/checkbox";
import IbxInput from "../../../shared/input";
import transformData from './transform-communications';
import Tabs from "../../../shared/tabs";
import * as Yup from "yup";
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber } from 'react-phone-number-input'
import messages from "../../../../../helpers/validation-constants";

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
        classifications: state.httpRequests.classifications,
        fetchedData: state.httpRequests.fetchedData,
        contractAccountsFetched: state.httpRequests.contractAccountsFetched,
    }
};

const CommunicationTab = function (
    {
        rootUrl, getBetterClassificationVal, classifications, editableFields,
        initialData, editMode, getInitialValues, isTerminalContract,
        setEditable, setVisibleContent, tabs, visible_content, contractNumber
    }
) {

    const [isLoading, setLoading] = useState(true);

    const [state, setState] = useState({
        isLoading: true,
        transformedData: {},
        initialData: {},
    });

    const handleAddress = (addressObj) => {
        let keys = ['AddressLine1', 'AddressLine2', 'City', 'Region', 'Country', 'ZipCode',];
        let values: string[] = [];
        keys.map(item => {
            if(item === 'Region' && addressObj.value[item]) {
                values.push(getBetterClassificationVal('ADDRESSES_REGION', addressObj.value[item]))
            } else if(item === 'Country' && addressObj.value[item]) {
                values.push(getBetterClassificationVal('ADDRESSES_COUNTRY', addressObj.value[item]))
            } else {
                addressObj.value[item] && values.push(addressObj.value[item]);
            }
            return item
        });
        return values.join(', ');
    };

    const fetchData = () => {
        axios.get(`${rootUrl}/Communication/${isTerminalContract ? 'GetTerminalCommunication' : 'GetContractCommunication'}?contractNumber=${contractNumber.replace(/-/g, '/')}`)
            .then(response => {
                if (response.data.Success) {
                    setState((prevState => ({
                        ...prevState,
                        transformedData: transformData(response.data.Data),
                        initialData: response.data.Data,
                    })));
                }
                setState((prevState => ({
                    ...prevState,
                    isLoading: false,
                })))

            });
    };

    useEffect(() => {
        fetchData();
    }, []);

    let commInitialValues = {
        ...getInitialValues(editableFields, state.initialData),
        ReportsDeliveryChannel: convertToArray(state.initialData['ReportsDeliveryChannel']),
        StatementsDeliveryChannel: convertToArray(state.initialData['StatementsDeliveryChannel']),
        StatementsFrequency: convertToArray(state.initialData['StatementsFrequency']),
    };

    const commHandleSubmit = (values) => {
        let ReportsDeliveryChannel = values.ReportsDeliveryChannel.reduce((all, item) => parseInt(all) + parseInt(item), 0);
        let StatementsDeliveryChannel = values.StatementsDeliveryChannel.reduce((all, item) => parseInt(all) + parseInt(item), 0);
        let StatementsFrequency = values.StatementsFrequency.reduce((all, item) => parseInt(all) + parseInt(item), 0);
        values = {...state.initialData, ...values, StatementsFrequency, ReportsDeliveryChannel, StatementsDeliveryChannel};

        axios({
            method: 'post',
            url: `${rootUrl}/Communication/UpdateCommunication`,
            responseType: 'json',
            data: values,
        }).then(response => {
            setEditable(false);
            if(detectData(response.data)) {
                fetchData();
            }
        }).catch(error => {
            alert(`Something Went Wrong!! Try Again Later!!`);
        });
    };

    let commSchema = Yup.object().shape({
        Mobiles: Yup.array().of(Yup.string().required(messages.required)
            /*.test('Mobiles', messages.phone, val => {
                return isValidPhoneNumber(val);
            })*/
        )
    });

    return (
        state.isLoading ?
            <div className="loading-box">
                <span> Loading... </span>
            </div>
            :

            <Formik
                enableReinitialize
                initialValues={isSet(state.initialData) ? commInitialValues : {}}
                validationSchema={commSchema}

                onSubmit={(values, {setSubmitting}) => {

                    commHandleSubmit(values);
                    setSubmitting(false);

                }}>
                {({errors, touched, values, handleChange, isSubmitting, handleReset}) => (

                    <>
                        {/*{JSON.stringify(errors)}
                                        {JSON.stringify(values)}*/}
                        <Tabs tabs={tabs} activeTab={visible_content}
                              handleClick={setVisibleContent.bind(null, setEditable.bind(null, handleReset, false))}/>
                        {!isSet(state.initialData) ?
                            <div className="no-data">
                                <img src="/assets/images/no-items.png"/>
                                <p>Sorry, No result found :(</p>
                            </div>
                            :
                            <Form className='client-profile'>
                                <div className="sub-tabs flex align-items-center justify-flex-end">
                                    <div className="buttons flex align-items-center">
                                        {editMode ?
                                            <>
                                                <IbxButton class="btn-outline" type='button'
                                                           handleClick={setEditable.bind(null, handleReset, false)}
                                                           btnText="Cancel"/>
                                                <IbxButton class="btn-grey-dark btn-left-icon" btnText="Save"
                                                           iconClass="icon-save"/>
                                            </>
                                            :
                                            initialData['Status'] !== 3 &&
                                            <IbxButton class="btn-grey-dark btn-left-icon" type='button'
                                                       handleClick={setEditable.bind(null, null, true)} btnText="Edit"
                                                       iconClass="icon-edit"/>
                                        }
                                    </div>
                                </div>
                                <div className="tab-panel block-container">
                                    <fieldset
                                        disabled={initialData.Status === 3 && editMode}
                                        style={{textDecoration: 'none', outline: 'none', border: 'none'}}
                                    >
                                        <div className="block-row">
                                            <div className="flex-table">
                                                {Object.keys(state.transformedData).map((s_item, s_index) => {
                                                    const key = state.transformedData[s_item];
                                                    return <div key={s_index}
                                                                className="flex-row flex align-items-center">
                                                        <div className="flex-item">
                                                        <span key={s_index}>
                                                            {s_item}
                                                        </span>
                                                        </div>
                                                        <div className="flex-item mardotoMedium">

                                                            {editableFields.indexOf(key['name']) === -1 ?
                                                                // None Editable Fields!!!
                                                                <span key={key.name}>
                                                                {
                                                                    key && key['classificator_key'] ?
                                                                        getBetterClassificationVal(key['classificator_key'], key.value) || '-' :
                                                                        key.value || '-'
                                                                }
                                                            </span>
                                                                :
                                                                // Editable Fields!!!
                                                                editMode ?
                                                                    (
                                                                        key.name === 'Reporting' ?
                                                                            <fieldset
                                                                                disabled={!isTerminalContract}
                                                                                style={{
                                                                                    textDecoration: 'none',
                                                                                    outline: 'none',
                                                                                    border: 'none'
                                                                                }}
                                                                            >
                                                                                <>
                                                                                    <span className="margin-right-25">
                                                                                         <IbxRadio label="No" value='false'
                                                                                                   name={key.name}/>
                                                                                    </span>
                                                                                    <span>
                                                                                         <IbxRadio label="Yes" value='true'
                                                                                                   name={key.name}/>
                                                                                    </span>
                                                                                </>
                                                                            </fieldset>
                                                                            :
                                                                            key.name === 'Language' ?
                                                                                <>
                                                                                    <span>
                                                                                         <IbxRadio label="Armenian" value='1'
                                                                                                   name={key.name}/>
                                                                                    </span>
                                                                                    <span>
                                                                                         <IbxRadio label="Russian" value='2'
                                                                                                   name={key.name}/>
                                                                                    </span>
                                                                                    <span>
                                                                                         <IbxRadio label="English" value='3'
                                                                                                   name={key.name}/>
                                                                                    </span>
                                                                                </>
                                                                                :
                                                                                key.name === 'ReportsDeliveryChannel' ?
                                                                                    <>
                                                                                        <span>
                                                                                            <IbxCheckbox label="SMS" numeric
                                                                                                         value='1'
                                                                                                         name={key.name}/>
                                                                                        </span>
                                                                                        <span>
                                                                                            <IbxCheckbox label="Email" numeric
                                                                                                         value='2'
                                                                                                         name={key.name}/>
                                                                                        </span>
                                                                                        <span>
                                                                                            <IbxCheckbox label="CBANet" numeric
                                                                                                         value='4'
                                                                                                         name={key.name}/>
                                                                                        </span>
                                                                                        <span>
                                                                                            <IbxCheckbox label="Postal Address"
                                                                                                         numeric
                                                                                                         value='8'
                                                                                                         name={key.name}/>
                                                                                        </span>
                                                                                        <span>
                                                                                            <IbxCheckbox label="Bank Office" numeric
                                                                                                         value='16'
                                                                                                         name={key.name}/>
                                                                                        </span>

                                                                                        <ErrorMessage
                                                                                            name={key.name}
                                                                                            component="span"
                                                                                            className="error-message"
                                                                                        />
                                                                                    </>
                                                                                    :
                                                                                    key.name === 'StatementsDeliveryChannel' ?
                                                                                        <>
                                                                                            <span>
                                                                                                <IbxCheckbox label="Email" numeric
                                                                                                             value='2'
                                                                                                             name={key.name}/>
                                                                                            </span>
                                                                                            <span>
                                                                                                <IbxCheckbox label="CBANet" numeric
                                                                                                             value='4'
                                                                                                             name={key.name}/>
                                                                                            </span>
                                                                                            <span>
                                                                                                <IbxCheckbox label="Postal Address"
                                                                                                             numeric
                                                                                                             value='8'
                                                                                                             name={key.name}/>
                                                                                            </span>
                                                                                            <span>
                                                                                                <IbxCheckbox label="Bank Office" numeric
                                                                                                             value='16'
                                                                                                             name={key.name}/>
                                                                                            </span>

                                                                                            <ErrorMessage
                                                                                                name={key.name}
                                                                                                component="span"
                                                                                                className="error-message"
                                                                                            />
                                                                                        </>
                                                                                        :
                                                                                        key.name === 'StatementsFrequency' ?
                                                                                        <>
                                                                                            <span>
                                                                                                <IbxCheckbox label="Weekly" numeric
                                                                                                             value='1'
                                                                                                             name={key.name}/>
                                                                                            </span>
                                                                                            <span>
                                                                                                <IbxCheckbox label="Monthly" numeric
                                                                                                             value='2'
                                                                                                             name={key.name}/>
                                                                                            </span>
                                                                                            <span>
                                                                                                <IbxCheckbox label="Quarterly" numeric
                                                                                                             value='4'
                                                                                                             name={key.name}/>
                                                                                            </span>

                                                                                                <ErrorMessage
                                                                                                    name={key.name}
                                                                                                    component="span"
                                                                                                    className="error-message"
                                                                                                />
                                                                                            </>
                                                                                            :
                                                                                            key.name === 'Mobiles' ?
                                                                                                <FieldArray
                                                                                                    name={key.name}
                                                                                                    render={({remove, push, form}) => (
                                                                                                        <>
                                                                                                            {values[key.name].length > 0 && values[key.name].map((item, index) => (
                                                                                                                <div className="flex align-items-center space-between sub-table" key={index}>
                                                                                                                    <div>
                                                                                                                        <PhoneInput
                                                                                                                            placeholder="Enter phone number"
                                                                                                                            value={values[key.name][index]}
                                                                                                                            name={key.name}
                                                                                                                            onChange={val => form.setFieldValue(`${key.name}.${index}`, val || '')}/>
                                                                                                                        <ArrayErrors
                                                                                                                            className='error-message simple font-12 margin-left-40'
                                                                                                                            errors={errors}
                                                                                                                            name={key.name}
                                                                                                                            touched={touched}
                                                                                                                            index={index}/>
                                                                                                                    </div>
                                                                                                                    <div>
                                                                                                                        <IbxButton
                                                                                                                            iconClass="icon-trash"
                                                                                                                            handleClick={() => {
                                                                                                                                remove(index);
                                                                                                                            }}
                                                                                                                            class="ibx-button btn-icon simple margin-left-10"
                                                                                                                            type='button'/>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            ))}

                                                                                                            {editMode &&

                                                                                                            <IbxButton
                                                                                                                handleClick={() => push('')}
                                                                                                                type='button'
                                                                                                                id="Selenium-Add-Note-Button-Id"
                                                                                                                class="not-full-width btn-simple btn-link margin-top-10 margin-bottom-5"
                                                                                                                btnText="Add New Mobile number"/>

                                                                                                            }
                                                                                                        </>
                                                                                                    )}/>
                                                                                                :
                                                                                                key.name === 'Emails' ?
                                                                                                    <FieldArray
                                                                                                        name={key.name}
                                                                                                        render={({remove, push, form}) => (
                                                                                                            <>
                                                                                                                 {values[key.name].length > 0 && values[key.name].map((item, index) => (
                                                                                                                        <div className="flex align-items-center space-between sub-table" key={index}>
                                                                                                                            <div>
                                                                                                                                <IbxInput
                                                                                                                                    errors={errors}
                                                                                                                                    touched={touched}
                                                                                                                                    class={`int-small`}
                                                                                                                                    name={`${key.name}.${index}`}/>
                                                                                                                                <ArrayErrors
                                                                                                                                    className='error-message simple font-12 margin-left-40'
                                                                                                                                    errors={errors}
                                                                                                                                    name={key.name}
                                                                                                                                    touched={touched}
                                                                                                                                    index={index}/>
                                                                                                                            </div>
                                                                                                                            <div>
                                                                                                                                <IbxButton
                                                                                                                                    iconClass="icon-trash"
                                                                                                                                    handleClick={() => {
                                                                                                                                        remove(index);
                                                                                                                                    }}
                                                                                                                                    class="ibx-button btn-icon simple margin-left-10"
                                                                                                                                    type='button'/>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    ))}

                                                                                                                 {editMode &&
                                                                                                                 <IbxButton
                                                                                                                     handleClick={() => push('')}
                                                                                                                     type='button'
                                                                                                                     id="Selenium-Add-Note-Button-Id"
                                                                                                                     class="not-full-width btn-simple btn-link margin-top-10 margin-bottom-5"
                                                                                                                     btnText="Add New Email"/>
                                                                                                                 }
                                                                                                            </>
                                                                                                        )}/>
                                                                                                    :
                                                                                                    key.name === 'CbaNets' ?
                                                                                                        <FieldArray
                                                                                                            name={key.name}
                                                                                                            render={({remove, push, form}) => (
                                                                                                                <>
                                                                                                                {values[key.name].length > 0 && values[key.name].map((item, index) => (
                                                                                                                    <div className="flex align-items-center space-between sub-table" key={index}>
                                                                                                                        <div>
                                                                                                                            <IbxInput
                                                                                                                                errors={errors}
                                                                                                                                touched={touched}
                                                                                                                                class={`int-small`}
                                                                                                                                name={`${key.name}.${index}`}/>
                                                                                                                            <ArrayErrors
                                                                                                                                className='error-message simple  font-12 margin-left-40'
                                                                                                                                errors={errors}
                                                                                                                                name={key.name}
                                                                                                                                touched={touched}
                                                                                                                                index={index}/>
                                                                                                                        </div>
                                                                                                                        <div>
                                                                                                                            <IbxButton
                                                                                                                                iconClass="icon-trash"
                                                                                                                                handleClick={() => {
                                                                                                                                    remove(index);
                                                                                                                                }}
                                                                                                                                class="ibx-button btn-icon simple margin-left-10"
                                                                                                                                type='button'/>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                ))}
                                                                                                                {editMode &&
                                                                                                                    <IbxButton
                                                                                                                        handleClick={() => push('')}
                                                                                                                        type='button'
                                                                                                                        id="Selenium-Add-Note-Button-Id"
                                                                                                                        class="not-full-width btn-simple btn-link margin-top-10 margin-bottom-5"
                                                                                                                        btnText="Add New CBANet"/>
                                                                                                                }
                                                                                                                </>
                                                                                                            )}/>
                                                                                                        :
                                                                                                        //key.name === 'Address'
                                                                                                        <>
                                                                                                            <div className="flex align-items-center custom-row">
                                                                                                                <div>
                                                                                                                    <IbxInput
                                                                                                                        errors={errors}
                                                                                                                        placeholder='AddressLine1'
                                                                                                                        touched={touched}
                                                                                                                        class={`int-small`}
                                                                                                                        name={`${key.name}.AddressLine1`}/>
                                                                                                                </div>
                                                                                                                <div>
                                                                                                                    <IbxInput
                                                                                                                        errors={errors}
                                                                                                                        placeholder='AddressLine2'
                                                                                                                        touched={touched}
                                                                                                                        class={`int-small`}
                                                                                                                        name={`${key.name}.AddressLine2`}/>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div className="flex align-items-center custom-row">
                                                                                                                <div>
                                                                                                                    <Field
                                                                                                                        name={`${key.name}.Country`}
                                                                                                                        render={({form, field}) => (
                                                                                                                            <IbxSelect
                                                                                                                                class="st-small"
                                                                                                                                options={classifications['ADDRESSES_COUNTRY']}
                                                                                                                                {...field}
                                                                                                                                labelKey='text'
                                                                                                                                selectedOptions={field.value}
                                                                                                                                changeHandler={(item) => form.setFieldValue(field.name, item.value)
                                                                                                                                }
                                                                                                                            />
                                                                                                                        )}
                                                                                                                    />
                                                                                                                </div>
                                                                                                                <div>
                                                                                                                    <Field
                                                                                                                        name={`${key.name}.Region`}
                                                                                                                        render={({form, field}) => (
                                                                                                                            <IbxSelect
                                                                                                                                class="st-small"
                                                                                                                                options={classifications['ADDRESSES_REGION']}
                                                                                                                                {...field}
                                                                                                                                labelKey='text'
                                                                                                                                selectedOptions={field.value}
                                                                                                                                changeHandler={(item) => form.setFieldValue(field.name, item.value)
                                                                                                                                }
                                                                                                                            />
                                                                                                                        )}
                                                                                                                    />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div className="flex align-items-center custom-row">
                                                                                                                <div>
                                                                                                                    <IbxInput
                                                                                                                        errors={errors}
                                                                                                                        placeholder='City'
                                                                                                                        touched={touched}
                                                                                                                        class={`int-small`}
                                                                                                                        name={`${key.name}.City`}/>
                                                                                                                </div>
                                                                                                                <div>
                                                                                                                    <IbxInput
                                                                                                                        errors={errors}
                                                                                                                        placeholder='Zip Code'
                                                                                                                        touched={touched}
                                                                                                                        class={`int-small`}
                                                                                                                        name={`${key.name}.ZipCode`}/>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </>
                                                                    )
                                                                    :

                                                                    <span key={s_index}>
                                                                        {
                                                                            key.name === 'Address' ?
                                                                                <>
                                                                                {handleAddress(key)}
                                                                                    {/*{key.value.AddressLine1 + ', ' +
                                                                                    key.value.AddressLine2 + ', ' +
                                                                                    key.value.City + ', ' +
                                                                                    getBetterClassificationVal('ADDRESSES_REGION', key.value.Region) + ', ' +
                                                                                    getBetterClassificationVal('ADDRESSES_COUNTRY', key.value.Country) + ', ' +
                                                                                    key.value.ZipCode
                                                                                    }*/}
                                                                                </>
                                                                                :
                                                                                (key.name === 'CbaNets' || key.name === 'Emails' || key.name === 'Mobiles') ?
                                                                                    key.value.join(', ')
                                                                                    :
                                                                                    (key.name === 'ReportsDeliveryChannel' || key.name === 'StatementsDeliveryChannel' || key.name === 'StatementsFrequency')?
                                                                                        (convertToText(key, classifications[key['classificator_key']]) || '-')
                                                                                        :
                                                                                        key['classificator_key'] ?
                                                                                            getBetterClassificationVal(key['classificator_key'], key.value) || '-' :
                                                                                            key.value || '-'
                                                                        }
                                                                    </span>
                                                            }
                                                        </div>
                                                    </div>
                                                })
                                                }
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>
                            </Form>
                        }
                    </>
                )}
            </Formik>
    )
};
export default connect(mapStateToProps)(CommunicationTab)

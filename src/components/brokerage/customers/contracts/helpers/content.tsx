import { Link, withRouter} from "react-router-dom";
import React, {useState} from 'react';
import IbxUploader from "../../../shared/uploader";
import {ErrorMessage, Field, Form, Formik} from "formik";
import IbxButton from "../../../shared/button";
import IbxInput from "../../../shared/input";
import axios from "axios";
import Comments from "../../../shared/notes";
import { connect } from 'react-redux';
import IbxSelect from "../../../shared/select";
import IbxCheckbox from "../../../shared/checkbox";
import {convertToText} from "../../../../../helpers/filters";
import Accounts from './accounts';
import IbxRadio from "../../../shared/radio";
import {generateContractPassword} from "../../../../../helpers/methods";

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
        classifications: state.httpRequests.classifications,
    }
};

export default connect(mapStateToProps, null)(withRouter(function(
    { rootUrl, touched, errors, values, classifications, match, location,
        content, key_name: tab_name, getBetterClassificationVal, contractType,
        selectOptions, isIndividual,statusClosingSelect,initiallyClosed,
        editableFields, editMode, isTerminalContract, initiallyActive }) {

    content = content || {};

    const headers = content.headers || content.value;

    const [state, setState] = useState();

    /**
     * closing contract selectOptions reason
     * change depending on individual and legal clients
     * so we filter them out here
     * */

    let modifiedSelectOptions;

    if(tab_name !== 'accounts' && tab_name !== 'application history') {
        if (isIndividual) {
            modifiedSelectOptions = selectOptions.filter(item => item.value !== 4);
        } else {
            modifiedSelectOptions = selectOptions.filter(item => item.value !== 5);
        }
    }

    if(initiallyActive) {
        statusClosingSelect = statusClosingSelect.filter(item => item.value !== 1);
    }

    return (
        <>
            {tab_name === 'accounts' ?
                <Accounts contractType={contractType} isTerminalContract={isTerminalContract}/>
                :
                tab_name === 'application history' ?
                    <div className="block-row">
                        <div className="flex-table">
                            <div className="flex-row flex align-items-center">
                                <div className="flex-item">
                                        <span>
                                            Application ID
                                        </span>
                                </div>
                                <div className="flex-item mardotoMedium">
                                        <Link className="green-link" to={`/brokerage/applications/applicants-list/${content.value}`}>
                                            {content.value}
                                        </Link>
                                </div>

                            </div>
                        </div>
                    </div>
                    :
                    tab_name === 'notes' ?
                        <Comments touched={touched} editMode={editMode} errors={errors} values={values}/>
                     : tab_name === 'documents' ?
                        <>
                            <p className="block-title">Documents</p>
                            <IbxUploader name='Filess' accept='.pdf,.png,.jpg,.jpeg,.msg' Files={values.Files} isMultiple/>
                        </>

                :
            headers.map((item, index) => (
                <React.Fragment key={index}>

                    <div className="block-row">

                        <p className="block-title">{item}</p>

                        <div className="flex-table">
                            {Object.keys(content[item]).filter(_item => {
                                // hide "opening date" field when the contract is in "pending" status
                                return !(content[item][_item].name === 'OpeningDate' && content[item]['status'].value === 1)
                            }).map((s_item, s_index) => {
                                const key = content[item][s_item];
                                return <div key={s_index} className="flex-row flex align-items-center">
                                    <div className="flex-item">
                                        <span key={s_index}>
                                            {s_item}
                                        </span>
                                    </div>
                                    <div className="flex-item mardotoMedium">

                                        { editableFields.indexOf(key['name']) === -1 ?
                                            // None Editable Fields!!!
                                            <span key={key.name}>
                                                {
                                                    key.name === 'InstrumentsType' ?
                                                        (convertToText(key, classifications[key['classificator_key']]) || '-') +' '+values['InstrumentTypeOther']
                                                        :

                                                    key.name === 'TerminalAgreements' ?
                                                        (key.value.length ? key.value.map((item, index)=>
                                                    <React.Fragment key={item}>
                                                        <Link className="green-link" to={
                                                            {
                                                                pathname: `/brokerage/customers/${match.params.clientId}/${match.params.screen}/${item.replace(/\//g, '-')}`,
                                                                search: location.search,
                                                            }
                                                        }>{item}</Link>{index !== key.value.length - 1 && ', '}
                                                    </React.Fragment>) : '-') :
                                                        key.name === 'BrokerageContractNumber' ?
                                                            key.value ?
                                                            <Link className="green-link" to={
                                                                {
                                                                    pathname: `/brokerage/customers/${match.params.clientId}/${match.params.screen}/${key.value.replace(/\//g, '-')}`,
                                                                    search: location.search,
                                                                }
                                                            }>{key.value}</Link>
                                                                :
                                                                '-'
                                                    :
                                                    key && key['classificator_key'] ?
                                                    getBetterClassificationVal(key['classificator_key'], key.value) || '-' :
                                                    key.value || '-'
                                                }
                                            </span>
                                            :
                                            // Editable Fields!!!
                                            editMode ?

                                                key.name === 'Status' ?
                                                    <>
                                                        <Field name='Status'
                                                               render={({form, field}) => (
                                                                   <IbxSelect
                                                                       class="st-small"
                                                                       options={statusClosingSelect}
                                                                       {...field}
                                                                       labelKey='text'
                                                                       // valueKey={''}
                                                                       selectedOptions={values[field.name]}
                                                                       changeHandler={(item) => form.setFieldValue(field.name, item.value)
                                                                       }
                                                                   />
                                                               )}
                                                        />
                                                        {!initiallyClosed && values.Status == 3 &&
                                                        <>
                                                            <Field name='ClosingReason'
                                                                   render={({form, field}) => (
                                                                       <IbxSelect
                                                                           class="st-small"
                                                                           options={modifiedSelectOptions}
                                                                           {...field}
                                                                           labelKey='text'
                                                                           // valueKey={''}
                                                                           selectedOptions={values[field.name]}
                                                                           changeHandler={(item) => form.setFieldValue(field.name, item.value)
                                                                           }
                                                                       />
                                                                   )}
                                                            />
                                                            <ErrorMessage
                                                                name={'ClosingReason'}
                                                                component="span"
                                                                className="error-message"
                                                            />
                                                                </>
                                                            }
                                                    </>

                                                    :
                                                    key.name === 'Password' ?
                                                        <>
                                                        <Field name='Password'
                                                               render={({form, field}) => {
                                                                   return <>
                                                                       <label className={`ibx-input int-small`}>
                                                                           <div className="inner">
                                                                               <input {...field} />
                                                                           </div>
                                                                       </label>
                                                                       {!initiallyClosed &&
                                                                       <a className='green-link' onClick={() => {
                                                                           form.setFieldValue(field.name, generateContractPassword());
                                                                       }
                                                                       }>Generate Password</a>
                                                                       }
                                                                       </>
                                                               }}/>
                                                        </>
                                                    :
                                                    key.name === 'Margin' ?
                                                        <div className="flex-item">
                                                                    <span>
                                                                         <IbxRadio label="No" value='false' name='Margin'/>
                                                                    </span>
                                                            <span>
                                                                         <IbxRadio label="Yes" value='true' name='Margin'/>
                                                                    </span>
                                                        </div>
                                                    :
                                                    key.name === 'InstrumentsType' ?
                                                        <div className="flex-item">
                                                                    <span>
                                                                        <IbxCheckbox label="Stocks" numeric value='1' name='InstrumentsType'/>
                                                                    </span>
                                                            <span>
                                                                        <IbxCheckbox label="Bonds" numeric value='2' name='InstrumentsType'/>
                                                                    </span>
                                                            <span>
                                                                        <IbxCheckbox label="Options" numeric value='4' name='InstrumentsType'/>
                                                                    </span>
                                                            <span>
                                                                        <IbxCheckbox label="Futures" numeric value='8' name='InstrumentsType'/>
                                                                    </span>
                                                            <span>
                                                                        <IbxCheckbox label="Forex" numeric value='16' name='InstrumentsType'/>
                                                                    </span>
                                                            <span className="inline-elements">
                                                                        <IbxCheckbox label="Other" numeric clearOther='InstrumentsOther' value='1024' name='InstrumentsType'/>
                                                                {values.InstrumentsType.map(item => item.toString()).indexOf('1024') !== -1 &&
                                                                <span className="margin-left-10">
                                                                            <IbxInput errors={errors}
                                                                                      touched={touched}
                                                                                      class={`int-small`}
                                                                                      name='InstrumentsOther'/>
                                                                        </span>
                                                                }
                                                                    </span>

                                                            <ErrorMessage
                                                                name={key.name}
                                                                component="span"
                                                                className="error-message"
                                                            />
                                                        </div>
                                                    :
                                                    key.name === 'ClosingReason' ?

                                                        <Field name='ClosingReason'
                                                               render={({form, field}) => (
                                                                   <IbxSelect
                                                                       class="st-small"
                                                                       options={selectOptions}
                                                                       // isDisabled={state.formDisabled}
                                                                       {...field}
                                                                       labelKey='text'
                                                                       // valueKey={''}
                                                                       selectedOptions={values[field.name]}
                                                                       changeHandler={(item) => form.setFieldValue(field.name, item.value)
                                                                       }
                                                                   />
                                                               )}
                                                        />

                                                    :
                                                <IbxInput class={`int-small`} name={key['name']}/>

                                                :

                                                <span key={s_index}>
                                                {
                                                    key.name === 'TerminalAgreements' ?
                                                        (convertToText(key, classifications['INDIVIDUALCLIENTS_TRANSACTIONS']) || '-') +' '+values['TransactionsOther']
                                                        :
                                                    key.name === 'InstrumentsType' ?
                                                        (convertToText(key, classifications['TERMINALS_INSTRUMENTSTYPE']) || '-') +' '+values['InstrumentsOther']
                                                        :

                                                    key['classificator_key'] ?
                                                        getBetterClassificationVal(key['classificator_key'], key.value) || '-' :
                                                    key.value || '-'
                                                }
                                            </span>
                                        }
                                    </div>
                                </div>
                            })
                            }
                        </div>
                    </div>
                </React.Fragment>
            ))}
        </>
    )
}))
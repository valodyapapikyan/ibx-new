import { dateFilterObj } from "../../../../../helpers/filters";

export default function(data) {
console.log(data);
    data = dateFilterObj(data);

    const obj = {
        "extras": {
            'contract_number': data.ContractNumber,
            'type': data.Type,
        },
        "general": {
            'tariffs and fees': {
                'tariff type': {
                    'classificator_key': "TARIFFS_TYPE",
                    name: 'Tariff.Type', //TODO
                    value: data.Tariff.Type, //TODO
                },
                'tariff name': {
                    name: 'Tariff.Name', //TODO
                    value: data.Tariff.Name, //TODO
                },
            },
            'headers': ['tariffs and fees'],
        },

        "application history": {
            name: 'ApplicationId',
            value: data.ApplicationId,
        },

        "documents": {
            name: 'Files',
            value: data.Files || [],
        },
        "notes": {
            name: 'Comments',
            value: data.Comments || [],
        },
    } as any;

    /**
     * Brokerage Contract!!!
     */

    if (data.Type === 1) {
        obj['general']['general'] = {
            'status': {
                'classificator_key': "CONTRACTS_STATUS",
                name: 'Status',
                value: data.Status,
            },
            'contract number': {
                name: 'ContractNumber',
                value: data.ContractNumber,
            },
            'terminal agreements': {
                name: 'TerminalAgreements', // does not exist here
                value: data.__terminalAgreements,
            },
            'securities main account': {
                name: 'SecurityMainAccount',
                value: data.SecurityMainAccount,
            },
            'depo main account': {
                name: 'DepoMainAccount',
                value: data.DepoMainAccount,
            },
            'opening date': {
                name: 'OpeningDate',
                value: data.OpeningDate,
            },
        };

        obj['general']['limitations, restrictions and permissions'] = {
            'deposit size (USD)': {
                name: 'DepositSize',
                value: data.DepositSize.toNumberFormatString(),
            },
        };

        obj['general']['headers'] = ['general', 'limitations, restrictions and permissions', 'tariffs and fees'];

    }

    /**
     * Custody Contract!!!
     */

    else if (data.Type === 2) {
        obj['general']['general'] = {
            'status': {
                'classificator_key': "CONTRACTS_STATUS",
                name: 'Status',
                value: data.Status,
            },
            'contract number': {
                name: 'ContractNumber',
                value: data.ContractNumber,
            },
            'securities main account': {
                name: 'SecurityMainAccount',
                value: data.SecurityMainAccount,
            },
            'depo main account': {
                name: 'DepoMainAccount',
                value: data.DepoMainAccount,
            },
            'opening date': {
                name: 'OpeningDate',
                value: data.OpeningDate,
            },
        };

        obj['general']['headers'] = ['general', 'tariffs and fees'];

    }

    /**
     * Investment Advisory Contract!!!
     */

    else if (data.Type === 3) {
        obj['general']['general'] = {
            'status': {
                'classificator_key': "CONTRACTS_STATUS",
                name: 'Status',
                value: data.Status,
            },
            'contract number': {
                name: 'ContractNumber',
                value: data.ContractNumber,
            },
            'opening date': {
                name: 'OpeningDate',
                value: data.OpeningDate,
            },
        };

        obj['general']['limitations, restrictions and permissions'] = {
            'instrument types': {
                'classificator_key': "TERMINALS_INSTRUMENTSTYPE",
                name: 'InstrumentsType',
                value: data.InstrumentsType,
            },
        };

        obj['general']['headers'] = ['general', 'limitations, restrictions and permissions', 'tariffs and fees'];

    }

    /**
     * Terminal Contract!!!
     */

    else {
        obj['general']['general'] = {
            'status': {
                'classificator_key': "TERMINALS_STATUS",
                name: 'Status',
                value: data.Status,
            },
            'contract number': {
                name: 'ContractNumber',
                value: data.ContractNumber,
            },
            'brokerage contract': {
                name: 'BrokerageContractNumber',
                value: data.BrokerageContractNumber,
            },
            'client registration code': {
                name: 'ClientRegistrationCode',
                value: data.ClientRegistrationCode,
            },
            'username': {
                name: 'UserName',
                value: data.UserName,
            },
            'password': {
                name: 'Password',
                value: data.Password,
            },
            'opening date': {
                name: 'OpeningDate',
                value: data.OpeningDate,
            },
        };

        if(data.Status === 3) {
            obj['general']['general'] = {
                ...obj['general']['general'],
                'closing date': {
                    name: 'ClosingDate',
                    value: data.ClosingDate,
                },
                'closing reason': {
                    'classificator_key': "TERMINALS_CLOSINGREASON",
                    name: 'ClosingReason',
                    value: data.ClosingReason,
                },
            }
        }

        obj['general']['limitations, restrictions and permissions'] = {
            'deposit size (USD)': {
                name: 'DepositSize',
                value: data.DepositSize.toNumberFormatString(),
            },
            'margin': {
                name: 'Margin',
                value: data.Margin ? 'Yes' : 'No',
            },
            'leverage': {
                name: 'Leverage',
                value: data.Leverage,
            },
            'instrument types': {
                'classificator_key': "TERMINALS_INSTRUMENTSTYPE",
                name: 'InstrumentsType',
                value: data.InstrumentsType,
            },

        };

        obj['general']['headers'] = ['general', 'limitations, restrictions and permissions', 'tariffs and fees'];

    }

    /**
     * Terminal Contracts don't have type!!!
     */
    if(data.Type) {
        if(data.Status === 3) {
            obj['general']['general'] = {
                ...obj['general']['general'],
                'closing date': {
                    name: 'ClosingDate',
                    value: data.ClosingDate,
                },
                'closing reason': {
                    'classificator_key': "CONTRACTS_CLOSINGREASON",
                    name: 'ClosingReason',
                    value: data.ClosingReason,
                },
            }
        }

    }
console.log(obj);
    return obj;
}
import React from "react";
import Modal from 'react-modal';
import IbxSelect from "../../../shared/select";
import IbxButton from "../../../shared/button";
import {ErrorMessage, Field} from "formik";

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

export default function({closeModal, modalIsOpen, selectOptions, values, closeContract, isIndividual}) {

    /**
     * closing contract selectOptions reason
     * change depending on individual and legal clients
     * so we filter them out here
     * */

    let modifiedSelectOptions;

    if(isIndividual) {
        modifiedSelectOptions = selectOptions.filter(item => item.value !== 4);
    } else {
        modifiedSelectOptions = selectOptions.filter(item => item.value !== 5);
    }
    return (
        <Modal
            ariaHideApp={false}
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Closing Contract Modal"
        >
            <div className="ibx-modal history-modal">
                <div className="modal-top flex align-items-center">
                    <span className="modal-title">History  of change</span>
                    {/*<span>*/}
                    {/*<button onClick={props.closeModal}>close</button>*/}
                    {/*</span>*/}
                </div>

                <div className="modal-content">
                    <div className="with-head">
                        <Field name='ClosingReason'
                               render={({form, field}) => (
                                   <IbxSelect
                                       class="st-small"
                                       options={modifiedSelectOptions}
                                       // isDisabled={state.formDisabled}
                                       {...field}
                                       labelKey='text'
                                       // valueKey={''}
                                       selectedOptions={values[field.name]}
                                       changeHandler={(item) => form.setFieldValue(field.name, item.value)
                                       }
                                   />
                               )}
                        />
                        <div className='flex'>
                            <IbxButton btnText={'Cancel'} handleClick={() => {
                                values.ClosingReason = 0;
                                closeModal();
                            }} type='button' class="ibx-button btn-grey-dark"/>
                            <IbxButton btnText={'Save'} isDisabled={!values.ClosingReason} handleClick={() => closeContract(values)} class="ibx-button btn-grey-dark"/>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    );
}
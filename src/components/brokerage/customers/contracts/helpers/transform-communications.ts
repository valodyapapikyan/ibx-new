import { dateFilterObj } from "../../../../../helpers/filters";

export default function(data) {
console.log(data);
    data = dateFilterObj(data);

    const obj = {
        "reporting": {
            name: 'Reporting',
            value: data.Reporting ? 'Yes' : 'No',
        },
        "language": {
            name: 'Language',
            value: data.Language,
            'classificator_key': "COMMUNICATIONS_LANGUAGE",
        },
        "reports delivery channels": {
            name: 'ReportsDeliveryChannel',
            value: data.ReportsDeliveryChannel,
            'classificator_key': "COMMUNICATIONS_REPORTSDELIVERYCHANNEL",
        },
        "statements delivery channels": {
            name: 'StatementsDeliveryChannel',
            value: data.StatementsDeliveryChannel,
            'classificator_key': "COMMUNICATIONS_STATEMENTSDELIVERYCHANNEL",
        },
        "statements frequency": {
            name: 'StatementsFrequency',
            value: data.StatementsFrequency,
            'classificator_key': "COMMUNICATIONS_STATEMENTSFREQUENCY",
        },
        'mobile number': {
            name: 'Mobiles',
            value: data.Mobiles,
        },
        'e-mail address': {
            name: 'Emails',
            value: data.Emails,
        },
        'CBANet': {
            name: 'CbaNets',
            value: data.CbaNets,
        },
        'mailing address': {
            name: 'Address',
            value: data.Address,
        },
    };

console.log(obj);
    return obj;
}
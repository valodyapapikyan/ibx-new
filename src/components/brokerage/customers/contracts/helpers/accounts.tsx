import React, {useEffect, useState} from 'react';
import { connect } from 'react-redux';
import axios from "axios";
import {transformContractAccounts} from "../../../../../helpers/table-transform";
import tableMethods from "../../../../../helpers/table-methods";
import {getContractAccounts} from "../../../../../actions";
import { Link, withRouter} from "react-router-dom";
import IbxSearchAndSort from "../../shared/search-and-sort";
import IbxTableSort from "../../../shared/table-sort";
import IbxStatus from "../../../shared/status";
import IbxPagination from "../../../shared/pagination";
import getClassicationValues from "../../../../../helpers/classifications";
import IbxSelect from "../../../shared/select";
import {Field, Form, Formik} from "formik";
import IbxInput from "../../../shared/input";
import IbxButton from "../../../shared/button";
import IbxCheckbox from "../../../shared/checkbox";
import IbxConfirm from "../../../shared/confirm";
import {detectData} from "../../../../../helpers/methods";
import AssignAccounts from './assign-accounts-modal';

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
        classifications: state.httpRequests.classifications,
        fetchedData: state.httpRequests.fetchedData,
        contractAccountsFetched: state.httpRequests.contractAccountsFetched,
    }
};

export default tableMethods(connect(mapStateToProps)(
    getClassicationValues(withRouter(function(
        {
            fetchedData,
            contractAccountsFetched,
            handleSort,
            sortType,
            sortBy,
            activePage,
            contractType,
            getBetterClassificationVal,
            pageSize,
            classifications,
            rootUrl,
            handlePageChange,
            handlePageSize,
            formSubmit,
            isTerminalContract,
            // addParamsToUrl,
            match,

        }) {

        const [state, setState] = useState();

        const filters = {
            type: 4,
            onlyOpenAccount: [1],
            withBalanceAccount: [1],
            search: '',
        };

        let typesOfAccounts: object[];

        let [isBrokerageContract, isCustodyContract, isTerminalAgreement] = [contractType === 1, contractType === 2, contractType !== 2 && contractType !== 1];

        if(isBrokerageContract) { // Brokerage Contract
            typesOfAccounts = classifications['ACCOUNTS_TYPE'];
        } else if(isCustodyContract) { // Custody Contract
            typesOfAccounts = classifications['ACCOUNTS_TYPE'].filter(item => item.value === 3 || item.value === 4);
        } else { // Terminal Agreement Contract
            typesOfAccounts = classifications['ACCOUNTS_TYPE'].filter(item => item.value === 1 || item.value === 2 || item.value === 4);
        }
        const contractNumber = match.params.contractNumber.replace(/-/g, '/');

        useEffect(() => {
            // addParamsToUrl(contractNumber);
            formSubmit({form:{contractNumber: contractNumber, ...filters}});

        }, []);

        const openDepoAccount = () => {
            IbxConfirm({
                title: 'Open New Depo Account',
                message: 'Do you want to open depo account?',
                onBtnConfirm: () => axios.post(`${rootUrl}/Accounts/OpenDepoAccount`,
                    '"' + contractNumber + '"',
                    {headers: {"Content-Type": "application/json"}})
                    .then((response) => {
                        if(detectData(response.data)) {
                            formSubmit();
                        }
                    }),
            });
        };

        const [modalIsOpen, toggleModal] = useState(false);

        const [accounts, setAccounts] = useState();

        const assignAccounts = ({values}) => {
            values = values.bodyRow;
            for (let i = 0; i < values.length; i++) {
                axios.post(`${rootUrl}/Accounts/${isTerminalContract ? 
                    'AttachTerminalBalanceAccount' : 'AttachBrokerageBalanceAccount'}`,
                    {...values[i], ContractNumber: contractNumber})
                    .then(response => {
                        detectData(response.data);
                        closeModal();
                    });
            }
        };

        const getAccounts = () => {
            axios.get(`${rootUrl}/Accounts/${isTerminalContract ? 'GetTerminalBalanceAccounts' : 'GetBrokerageBalanceAccounts'}?contractNumber=${contractNumber}`)
                .then(response => {
                    setAccounts(response.data.Data);
                    openModal();
                })
        };

        const openModal = () => {
            toggleModal(true);
        };

        const closeModal = () => {
            toggleModal(false);
        };

        let { Total, Data } = fetchedData || {Total: 0, Data: {body: []}};

        return (
            <>
                <>
                    <div className="sub-tabs custom-sub-tabs flex align-items-center space-between">
                        <h2 className="block-title margin-bottom-0">Accounts</h2>
                        <div className="buttons flex align-items-center">
                            {isBrokerageContract || isTerminalAgreement ?
                            <IbxButton class="btn-grey-dark" type='button' btnText='Assign Accounts' handleClick={getAccounts}/>
                            : '' }
                            {isBrokerageContract || isCustodyContract ?
                            <IbxButton class="btn-grey-dark" type='button' btnText='Open Depo Account' handleClick={openDepoAccount}/>
                            : '' }
                        </div>
                    </div>
                    <div>
                        <Formik
                            initialValues={filters}
                            onSubmit={(values, {setSubmitting}) => {
                                formSubmit({
                                    form: {...values, pageNumber: 1, activePage: 1},
                                });
                                setSubmitting(false);
                            }}>
                            {({errors, touched, values, handleChange, isSubmitting, handleReset}) => (
                                <Form>
                                    <div className="ibx-search-sort-container custom-sort flex align-items-center space-between margin-top-0">

                                        <div className="flex left-part align-items-center">
                                            <div className="search-box">
                                                <IbxInput class="int-left-icon" name="search" icon="icon-search"
                                                          placeholder="Account Number or Ticker"/>
                                            </div>
                                            <div className="padding-left-20">
                                                <Field name='type'
                                                       render={({form, field}) => (
                                                           <IbxSelect
                                                               class="st-small"
                                                               options={typesOfAccounts}
                                                               {...field}
                                                               labelKey='text'
                                                               // valueKey={''}
                                                               selectedOptions={values[field.name]}
                                                               changeHandler={(item) => form.setFieldValue(field.name, item.value)
                                                               }
                                                           />
                                                       )}
                                                />
                                            </div>
                                        </div>
                                        <div className="text-right">
                                            <IbxCheckbox class="margin-right-20" label="With Balance Only" numeric name='withBalanceAccount' value='1'/>
                                            <IbxCheckbox label="Only Open Accounts" numeric name='onlyOpenAccount' value='1'/>
                                        </div>
                                        <div className="inline-elements">
                                            <span onClick={handleReset} className="green-link font-14 margin-right-15">Reset filter</span>
                                            <span>
                                                <IbxButton class="btn-grey-dark selenium-apply-filter-button-id max-width-170" btnText="Apply filter"/>
                                            </span>
                                        </div>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                    {!contractAccountsFetched ?
                        <div className="loading-box">
                            <span> Loading... </span>
                        </div>
                        :
                        <>
                            <div className="ibx-table-container">
                                <div className="ibx-table-row head">
                                    {Object.keys(Data.header).map((item, index) => (
                                            <div className={`ibx-table-cell`} key={index}>
                                                <span>{Data.header[item]}</span>
                                                <IbxTableSort sort={{key: sortBy, value: sortType}} _key={item} handleSort={handleSort}/>
                                            </div>
                                        ))}
                                </div>
                                {Data.body && Data.body.length ?
                                    Data.body.map((item, index) => (
                                        <div className="ibx-table-row" key={index}>
                                            {Object.keys(Data.header).map((_item, _index) => (
                                                    <div className={`ibx-table-cell`} key={_index}>
                                                        {
                                                            _item === 'Currency' ?
                                                                getBetterClassificationVal('ACCOUNTS_CURRENCY', item[_item])
                                                                :
                                                            _item === 'Status' ?
                                                                <IbxStatus text={item[_item]}
                                                                           class={item[_item] === 'New' ? 'st-new' :
                                                                               item[_item] === 'Approved' ? 'approved' :
                                                                                   item[_item] === 'Waiting for agreement' ? 'waiting' :
                                                                                       item[_item] === 'In review' ? 'in-review':
                                                                                           item[_item] === 'Rejected' ? 'rejected':
                                                                                               item[_item] === 'Closed' ? 'closed' : 'active'}/>
                                                                :
                                                                _item === 'Balance' ?
                                                                    item[_item].toNumberFormatString()
                                                                    :
                                                                typeof item[_item] === "number" ?
                                                                    item[_item] :
                                                                item[_item] || '-'
                                                        }
                                                    </div>
                                                ))}
                                        </div>
                                    ))
                                :
                                ''}
                            </div>
                            {Data.body && Data.body.length ?
                                <IbxPagination
                                    activePage={activePage}
                                    pageSize={pageSize}
                                    rows={Data.body.length}
                                    total={Total}
                                    handlePageChange={handlePageChange}
                                    handleResultsPP={handlePageSize}
                                />
                                :
                                ''
                            }

                            {Data.body && !Data.body.length &&
                                <div className="no-data">
                                    <img src="/assets/images/no-items.png"/>
                                    <p>Sorry, No result found :(</p>
                                </div>
                            }
                        </>
                    }

                </>
                <AssignAccounts
                    closeModal={closeModal}
                    modalIsOpen={modalIsOpen}
                    handleSubmit={assignAccounts}
                    data={accounts}
                />
            </>
        )
    }))), {
    actionToDispatch: getContractAccounts,
    url: 'Accounts/GetAccountsGridView',
    screen: 'contract_accounts',
    filters: {
        sortBy: 'OpeningDate',
        sortType: 'DESC',
    }
}
)
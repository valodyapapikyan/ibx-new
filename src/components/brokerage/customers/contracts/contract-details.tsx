import React, {useEffect, useState} from 'react';
import { Link, withRouter} from "react-router-dom";
import axios from "axios";
import getClassicationValues from "../../../../helpers/classifications";
import { connect } from 'react-redux';
import {Form, Formik} from "formik";
import Tabs from "../../shared/tabs";
import IbxButton from "../../shared/button";
import Content from "./helpers/content";
import {convertToArray} from "../../../../helpers/filters";
import transformData from './helpers/transform';
import 'react-confirm-alert/src/react-confirm-alert.css';
import * as Yup from 'yup';
import messages from "../../../../helpers/validation-constants";
import {buildRequiredCheckbox} from "../../applications/shared/validation-shapes";
import CommunicationTab from "./helpers/communication";

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
        classifications: state.httpRequests.classifications,
        editableFields: state.httpRequests.editableFields,
    }
};

export default getClassicationValues(withRouter(connect(mapStateToProps,null)(function(
    {
        rootUrl,
        clientId,
        match,
        getClassificationVal,
        getBetterClassificationVal,
        editableFields,
        classifications,
        location,
    }) {

    const {contractNumber} = match.params;

    /**
     * terminal contracts start with "TA",
     * that's a rule from god and Hayk,
     * someday when they decide no it's "AT" we're screeeeeewed!!!
     *
     * */

    const isTerminalContract = contractNumber.indexOf('TA') === 0;

    const [state, setState] = useState({
        isLoading: true,
        data: '' as any,
        visible_content: 'general',
        subContent: [] as string[],
    });

    const [editMode, setEditMode] = useState(false);

    useEffect(() => {
        getData();
        setEditMode(false);
    }, [contractNumber]);

    const getData = () => {
        let action = rootUrl +
            (isTerminalContract ? '/Terminal/GetTerminal' : '/Contract/GetContract') +
            `?contractNumber=${contractNumber.replace(/-/g, '/')}`;
        axios({
            method: 'get',
            url: action,
        }).then(response => {
            let data = response.data.Data;

            if(!response.data.Success) {
                window.history.back();
                alert(response.data.Message);
                return;
            }
            if(data.Type === 1) {
                axios.get(`${rootUrl}/Terminal/GetTerminalsByBrokerageContractNumber?contractNumber=${data.ContractNumber}`)
                    .then(response2 => {

                        setState(prevState => {

                            return {
                                ...prevState,
                                data: {
                                    Data: transformData({
                                        ...data,
                                        __terminalAgreements: response2.data.Data.map(item => item = item.ContractNumber)
                                    }),
                                    initialData: data,
                                },
                                isLoading: false,
                            }
                        })
                    });
            } else {
                setState(prevState => {

                    return {
                        ...prevState,
                        data: {
                            Data: transformData(data),
                            initialData: data,
                        },
                        isLoading: false,
                    }
                })
            }

        }).catch(error => {
            alert(`Something Went Wrong!! Try Again Later!!`);
        });
    };

    if(state.isLoading) return <div className="loading-box">
        <span> Loading... </span>
    </div>;

    const setEditable = (resetForm: Function | null, editMode: boolean) => {
        setState((prevState => (
                {
                    ...prevState,
                })
        ));
        setEditMode(editMode);
        if(resetForm) {
            resetForm();
        }
    };

    const getInitialValues = (fields: string[] = [], allValues: any = null): object => {
        let initialValues = {} as any;
        for (let i = 0; i < fields.length; i++) {
            initialValues = {
                ...initialValues,
                [fields[i]]: allValues ? allValues[fields[i]] : state.data.initialData[fields[i]],
            };
        }
        return initialValues;
    };

    const handleSubmit = ({values}) => {

        if(isTerminalContract) {
            let InstrumentsType = values.InstrumentsType.reduce((all, item) => parseInt(all) + parseInt(item), 0);
            values = {...state.data.initialData, ...values, InstrumentsType};
        } else {
            values = {...state.data.initialData, ...values};
        }

        axios({
            method: 'post',
            url: isTerminalContract ? `${rootUrl}/Terminal/UpdateTerminal` : `${rootUrl}/Contract/UpdateContract`,
            responseType: 'json',
            data: values,
        }).then(response => {
            if(response.data.Success) {
                setEditMode(false);
                getData();
                alert(`Contract Was Successfully Updated!!`);
            } else {
                alert(response.data.Message);
            }
        }).catch(error => {
            alert(`Something Went Wrong!! Try Again Later!!`);
        });
    };

    const {visible_content, data} = state;

    const {Data, initialData} = data;

    const _editableFields = editableFields[isTerminalContract? 'terminal' : 'contract'];

    let initialValues = {
        ...getInitialValues(_editableFields),
        Files: Data.documents.value.slice() as any[],// Always pass a copy "slice()" otherwise initial value would change because of the reference
        Comments: Data.notes.value as any[],// No need to pass a copy here, as "Formik" (or any sane form library) does that for us
    } as any;

    let tabs = ['general', 'communication', 'accounts', 'application history', 'documents', 'notes'];

    if(isTerminalContract) {
        initialValues = {
            ...initialValues,
            InstrumentsType: convertToArray(initialValues.InstrumentsType),
        };
    }

    if(Data.extras.type === 3) {
        tabs.splice(2,1);
    }

    const setVisibleContent = (setEditable: Function, item: string): void => {
        if(setEditable) {
            setEditable();
        }
        setState((prevState => ({
            ...prevState,
            visible_content: item
        })));
    };

    let Schema = Yup.object().shape({
        DepositSize: Yup.number()
            .typeError(messages.number).required(messages.required),
        ClosingReason: Yup.number().when('Status', {
            is: 3,
            then: Yup.number().test('ClosingReason', messages.required, val => val > 0)
        })
    });

    if(isTerminalContract) {
        Schema = Schema.concat(Yup.object().shape({
            InstrumentsType: Yup.array().of(Yup.string()).required(messages.required),
            InstrumentsOther: Yup.string().when('InstrumentsType', buildRequiredCheckbox('1024')),
        }))
    }

    return (
        <>
            <Link className="green-link with-icon back-link" to={
                {
                    pathname: `/brokerage/customers/${match.params.clientId}/${match.params.screen}`,
                    search: location.search,
                }
            }>
            	<i className="icon-arrow-left font-8"></i>
                <span>Back to Contracts</span>
            </Link>
            <h2 className="page-title uppercase margin-top-10">
               {Data.extras.type ? getBetterClassificationVal('CONTRACTS_TYPE', Data.extras.type) : 'Terminal Agreement'}
                <span className="mardotoRegular"> &nbsp;{Data.extras.contract_number}</span>
            </h2>

            <div className="tabs margin-top-20">
                {visible_content === 'accounts' || visible_content === 'application history' ?
                <div className='client-profile'>
                    <Tabs tabs={tabs} activeTab={visible_content}
                          handleClick={setVisibleContent.bind(null, setEditable.bind(null, null, false))}/>

                    <div className="tab-panel block-container">
                        <Content
                                 content={Data[visible_content]}
                                 isTerminalContract={isTerminalContract}
                                 contractType={Data.extras.type}
                                 key_name={visible_content}/>

                    </div>
                </div>
                :
                    visible_content === 'communication' ?
                        <>
                            <CommunicationTab editMode={editMode}
                                              getBetterClassificationVal={getBetterClassificationVal}
                                              getInitialValues={getInitialValues}
                                              editableFields={editableFields['communication']}
                                              visible_content={visible_content}
                                              tabs={tabs}
                                              isTerminalContract={isTerminalContract}
                                              setVisibleContent={setVisibleContent}
                                              setEditable={setEditable}
                                              initialData={initialData}
                                              contractNumber={contractNumber}
                                              />
                        </>
                        :
                    <Formik
                        enableReinitialize
                        initialValues={initialValues}
                        validationSchema={Schema}

                        onSubmit={(values, { setSubmitting }) => {

                            handleSubmit({
                                values,
                                // url: `${rootUrl}/Account/Login`
                            });
                            setSubmitting(false);

                        }}>
                        {({errors, touched, values, handleChange, isSubmitting, handleReset}) => (

                            <>
                                {/*{JSON.stringify(errors)}
                            {JSON.stringify(values)}*/}
                                <Tabs tabs={tabs} activeTab={visible_content}
                                      handleClick={setVisibleContent.bind(null, setEditable.bind(null, handleReset, false))}/>

                                <Form className='client-profile'>
                                    <div className="sub-tabs flex align-items-center justify-flex-end">
                                        <div className="buttons flex align-items-center">
                                            {editMode ?
                                                <>
                                                    <IbxButton class="btn-outline" type='button'
                                                               handleClick={setEditable.bind(null, handleReset, false)} btnText="Cancel"/>
                                                    <IbxButton class = "btn-grey-dark btn-left-icon" btnText="Save" iconClass="icon-save"/>
                                                </>
                                                :
                                                initialData['Status'] !== 3 &&
                                                <IbxButton class="btn-grey-dark btn-left-icon" type='button'
                                                           handleClick={setEditable.bind(null, null, true)} btnText="Edit" iconClass="icon-edit"/>
                                            }
                                        </div>
                                    </div>
                                    <div className="tab-panel block-container">
                                        <fieldset
                                            disabled={initialValues.Status === 3 && editMode}
                                            style={{textDecoration: 'none', outline: 'none', border: 'none'}}
                                        >
                                            <Content values={values}
                                                     errors={errors}
                                                     touched={touched}
                                                     editMode={editMode}
                                                     initiallyClosed={initialValues.Status === 3}
                                                     initiallyActive={initialValues.Status === 2}
                                                     editableFields={_editableFields}
                                                     selectOptions={classifications['CONTRACTS_CLOSINGREASON']}
                                                     statusClosingSelect={classifications[isTerminalContract ? 'TERMINALS_STATUS' : 'CONTRACTS_STATUS']}
                                                     isTerminalContract={isTerminalContract}
                                                     getBetterClassificationVal={getBetterClassificationVal}
                                                     content={Data[visible_content]}
                                                     isIndividual={location.search.indexOf('Individual') > -1}
                                                     key_name={visible_content}/>
                                        </fieldset>
                                    </div>
                                </Form>
                            </>
                        )}
                    </Formik>
                }

            </div>
        </>
    )
})))
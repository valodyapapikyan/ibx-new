import { dateFilterObj } from "../../../../../helpers/filters";

export default function(data) {

    data = dateFilterObj(data);

    const obj = {
        "extras": {
            'customer_code': data.Code,
            'type': data.Type,
        },
        "general": {
            'other details': {
                'registration date': {
                    name: 'RegistrationDate',
                    value: data.RegistrationDate,
                },
                'activation date': {
                    name: 'ActivationDate',
                    value: data.ActivationDate,
                },
                'closing date': {
                    name: 'ClosingDate',
                    value: data.ClosingDate,
                },
                'closing reason': {
                    'classificator_key': "CLIENTS_CLOSINGREASON",
                    name: 'ClosingReason',
                    'value': data.ClosingReason,
                },
                'broker': {
                    name: 'Broker',
                    value: data.Broker,
                },
            },
        },
        "address & contacts": {
            'registration address': {
                'address line 1': {
                    name: 'RegistrationAddress.AddressLine1',
                    value: data.RegistrationAddress.AddressLine1,
                },
                'address line 2': {
                    name: 'RegistrationAddress.AddressLine2',
                    value: data.RegistrationAddress.AddressLine2,
                },
                'city': {
                    name: 'RegistrationAddress.City',
                    value: data.RegistrationAddress.City,
                },
                'region': {
                    'classificator_key': "ADDRESSES_REGION",
                    name: 'RegistrationAddress.Region',
                    'value': data.RegistrationAddress.Region,
                },
                'country': {
                    'classificator_key': "ADDRESSES_COUNTRY",
                    name: 'RegistrationAddress.Country',
                    'value': data.RegistrationAddress.Country,
                },
                'zip': {
                    name: 'RegistrationAddress.ZipCode',
                    value: data.RegistrationAddress.ZipCode,
                },
            },
            'contact info': {
                'phone': {
                    name: 'Phone',
                    value: data.Phone,
                },
                'mobile phone': {
                    name: 'MobilePhone',
                    value: data.MobilePhone,
                },
                'e-mail': {
                    name: 'Email',
                    value: data.Email,
                },
                'fax': {
                    name: 'Fax',
                    value: data.Fax,
                },
            },
            'headers': [] as string[],
        },
        "finance & assets": {
            'financial details': {
                'assets': {
                    name: 'Assets',
                    value: data.Assets.toNumberFormatString(),
                },
            },
            'assets': {
                'real estate': {
                    name: 'RealEstate',
                    value: data.RealEstate,
                },
                'movable': {
                    name: 'Movable',
                    value: data.Movable,
                },
            },
            'headers': ['financial details', 'assets']
        },
        "documents": {
            name: 'Files',
            value: data.Files,
        },
        "notes": {
            name: 'Comments',
            value: data.Comments,
        },
    } as any;

    if (data.Type === 1) {
        obj['general']['general'] = {
            'surname': {
                name: 'Surname',
                value: data.Surname,
            },
            'name': {
                name: 'Name',
                value: data.Name,
            },
            'father\'s name': {
                name: 'FathersName',
                value: data.FathersName,
            },
            'full name(eng)': {
                name: 'NameEng',
                value: data.NameEng,
            },
            'date of birth': {
                name: 'BirthDate',
                value: data.BirthDate,
            },
        };

        obj['general']['identification document'] = {
            'ID type': {
                name: 'IDType',
                value: data.IDType,
            },
            'ID number': {
                name: 'IDNumber',
                value: data.IDNumber,
            },
            'issue date': {
                name: 'IDIssueDate',
                value: data.IDIssueDate,
            },
            'expiry date': {
                name: 'IDExpiryDate',
                value: data.IDExpiryDate,
            },
            'authority': {
                name: 'IDIssuer',
                value: data.IDIssuer,
            },
            'social services number/number of reference': {
                name: 'SocialNumber',
                value: data.SocialNumber,
            },
            'citizenship': {
                name: 'Citizenship',
                value: data.Citizenship,
            },
            'residency': {
                name: 'Residency',
                value: data.Residency ? 'Yes' : 'No',
            },
        };

        obj['general']['other details']['status'] = {
            'classificator_key': "CLIENTS_STATUS",
            name: 'Status',
            'value': data.Status,
        };

        obj['general']['headers'] = ['general', 'identification document', 'other details'];

        obj["education & experience"] = {
            'education and work': {
                'education': {
                    name: 'Education',
                    value: data.Education,
                },
                'employer': {
                    name: 'EmployerName',
                    value: data.EmployerName,
                },
                'position': {
                    name: 'Position',
                    value: data.Position,
                },
                'occupation': {
                    name: 'Occupation',
                    value: data.Occupation,
                },
            },
            'knowledge and experience': {
                'class': {
                    'classificator_key': "INDIVIDUALCLIENTS_CLASS",
                    name: 'Class',
                    'value': data.Class,
                },
                'risk rate': {
                    'classificator_key': "INDIVIDUALCLIENTS_RISKRATE",
                    name: 'RiskRate',
                    value: data.RiskRate,
                },
                'knowledge of capital market': {
                    'classificator_key': "INDIVIDUALCLIENTS_KNOWLEDGEOFCAPITALMARKET",
                    name: 'KnowledgeOfCapitalMarket',
                    value: data.KnowledgeOfCapitalMarket,
                },
                'experience': {
                    name: 'Experience',
                    value: data.Experience,
                },
                'used services': {
                    name: 'UsedServices',
                    value: data.UsedServices,
                },
                'transactions': {
                    name: 'Transactions',
                    value: data.Transactions,
                },
            },
            'headers': ['education and work', 'knowledge and experience']
        };

        obj['address & contacts'] = {
            ...obj['address & contacts'],
            'residence address': {
                'address line 1': {
                    name: 'ResidenceAddress.AddressLine1',
                    value: data.ResidenceAddress.AddressLine1,
                },
                'address line 2': {
                    name: 'ResidenceAddress.AddressLine2',
                    value: data.ResidenceAddress.AddressLine2,
                },
                'city': {
                    name: 'ResidenceAddress.City',
                    value: data.ResidenceAddress.City,
                },
                'region': {
                    'classificator_key': "ADDRESSES_REGION",
                    name: 'ResidenceAddress.Region',
                    'value': data.ResidenceAddress.Region,
                },
                'country': {
                    'classificator_key': "ADDRESSES_COUNTRY",
                    name: 'ResidenceAddress.Country',
                    'value': data.ResidenceAddress.Country,
                },
                'zip': {
                    name: 'ResidenceAddress.ZipCode',
                    value: data.ResidenceAddress.ZipCode,
                },
            },
            'headers': ['registration address', 'residence address', 'contact info'],
        };


        obj['finance & assets']['financial details']['average monthly income'] = {
            name: 'AverageMonthlyIncome',
            value: data.AverageMonthlyIncome.toNumberFormatString(),
        };


    } else if (data.Type === 2) {
        obj['general']['general'] = {
            'company name': {
                name: 'Name',
                value: data.Name,
            },
            'company name(eng)': {
                name: 'NameEng',
                value: data.NameEng,
            },
            'legal status': {
                name: 'LegalStatus',
                value: data.LegalStatus,
            },
            'director name': {
                name: 'DirectorName',
                value: data.DirectorName,
            },
            'registration number': {
                name: 'CompanyRegistrationNumber',
                value: data.CompanyRegistrationNumber,
            },
            'company registration date': {
                name: 'CompanyRegistrationDate',
                value: data.CompanyRegistrationDate,
            },
            'company tin': {
                name: 'TIN',
                value: data.TIN,
            },
            'residency': {
                name: 'Residency',
                value: data.Residency ? 'Yes' : 'No',
            },
            'business sphere': {
                name: 'BusinessSphere',
                value: data.BusinessSphere,
            },
        };

        obj['general']['other details']['status'] = {
            'classificator_key': "CLIENTS_STATUS",
            name: 'Status',
            'value': data.Status,
        };

        obj['general']['headers'] = ['general', 'other details'];

        obj['finance & assets']['financial details']['average monthly profit'] = {
            name: 'AverageMonthlyProfit',
            value: data.AverageMonthlyProfit.toNumberFormatString(),
        };

        obj['address & contacts'] = {
            ...obj['address & contacts'],
            'business address': {
                'address line 1': {
                    name: 'BusinessAddress.AddressLine1',
                    value: data.BusinessAddress.AddressLine1,
                },
                'address line 2': {
                    name: 'BusinessAddress.AddressLine2',
                    value: data.BusinessAddress.AddressLine2,
                },
                'city': {
                    name: 'BusinessAddress.City',
                    value: data.BusinessAddress.City,
                },
                'region': {
                    'classificator_key': "ADDRESSES_REGION",
                    name: 'BusinessAddress.Region',
                    'value': data.BusinessAddress.Region,
                },
                'country': {
                    'classificator_key': "ADDRESSES_COUNTRY",
                    name: 'BusinessAddress.Country',
                    'value': data.BusinessAddress.Country,
                },
                'zip': {
                    name: 'BusinessAddress.ZipCode',
                    value: data.BusinessAddress.ZipCode,
                },
            },

            'headers': ['registration address', 'business address', 'contact info'],
        };


    }
console.log(obj);
    return obj;
}
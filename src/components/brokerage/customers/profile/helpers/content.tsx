import React, {useState} from 'react';
import IbxUploader from "../../../shared/uploader";
import {Field, Form, Formik} from "formik";
import IbxButton from "../../../shared/button";
import IbxInput from "../../../shared/input";
import axios from "axios";
import Comments from "../../../shared/notes";
import { connect } from 'react-redux';
import IbxSelect from "../../../shared/select";
import IbxCheckbox from "../../../shared/checkbox";
import {convertToText} from "../../../../../helpers/filters";

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
        classifications: state.httpRequests.classifications,
    }
};

export default connect(mapStateToProps, null)(function(
    { rootUrl, touched, errors, values, classifications, content, key_name, getClassificationVal, editableFields, editMode }) {

    const headers = content.headers || content.value;

    // Documents and Notes don't have titles
    const hasTitle = key_name !== 'notes' && key_name !== 'documents';

    const [state, setState] = useState();

    /*axios({
        method: 'get',
        url: rootUrl + '/Common/GetBrokerageContractsList',
        responseType: 'json',
    }).then(response => {
        console.log(response.data);
    }).catch(error => {
        console.error(error);
    });*/


    return (
        <>
            {hasTitle && headers.map((item, index) => (
                <React.Fragment key={index}>

                    <div className="block-row">

                        <p className="block-title">{item}</p>

                        <div  className="flex-table">
                            {Object.keys(content[item]).map((s_item, s_index) => {
                                const key = content[item][s_item];
                                return <div key={s_index} className="flex-row flex align-items-center">
                                    <div className="flex-item">
                                        <span>
                                            {s_item}
                                        </span>
                                    </div>
                                    <div className="flex-item mardotoMedium">

                                        { editableFields.indexOf(key['name']) === -1 ?
                                            // None Editable Fields!!!
                                            <span>
                                                {key && key['classificator_key'] ?
                                                    getClassificationVal(key) || '-' :
                                                    key.value || '-'
                                                }
                                            </span>
                                            :
                                            // Editable Fields!!!
                                            editMode ?

                                                key.name === 'RiskRate' ?
                                                    <Field name={key.name} render={({field, form}) =>
                                                        <IbxSelect
                                                            {...field}
                                                            class="st-small"
                                                            changeHandler={(item) => form.setFieldValue(key.name, item.value)}
                                                            selectedOptions={values[key.name]}
                                                            labelKey='text'
                                                            options={classifications['INDIVIDUALCLIENTS_RISKRATE']}
                                                        />
                                                    }/>
                                                    :
                                                key.name === 'KnowledgeOfCapitalMarket' ?
                                                    <Field name={key.name} render={({field, form}) =>
                                                        <IbxSelect
                                                            {...field}
                                                            class="st-small"
                                                            changeHandler={(item) => form.setFieldValue(key.name, item.value)}
                                                            selectedOptions={values[key.name]}
                                                            labelKey='text'
                                                            options={classifications['INDIVIDUALCLIENTS_KNOWLEDGEOFCAPITALMARKET']}
                                                        />
                                                    }/>

                                                    :

                                                key.name === 'Class' ?
                                                    <Field name={key.name} render={({field, form}) =>
                                                        <IbxSelect
                                                            {...field}
                                                            class="st-small"
                                                            changeHandler={(item) => form.setFieldValue(key.name, item.value)}
                                                            selectedOptions={values[key.name]}
                                                            labelKey='text'
                                                            options={classifications['INDIVIDUALCLIENTS_CLASS']}
                                                        />
                                                    }/>

                                                    :

                                                key.name === 'UsedServices' ?
                                                    <div className="flex-item">
                                                        <span>
                                                            <IbxCheckbox label="Brokerage" numeric value='1' name='UsedServices'/>
                                                        </span>
                                                        <span>
                                                            <IbxCheckbox label="Trust" numeric value='2' name='UsedServices'/>
                                                        </span>
                                                        <span>
                                                            <IbxCheckbox label="Investment Advisory" numeric value='4' name='UsedServices'/>
                                                        </span>
                                                        <span className="inline-elements">
                                                            <IbxCheckbox label="Other" numeric clearOther='UsedServiceOther' value='8' name='UsedServices'/>
                                                            {values.UsedServices.map(item => item.toString()).indexOf('8') !== -1 &&
                                                            <span className="margin-left-10">
                                                                <IbxInput errors={errors}
                                                                          touched={touched}
                                                                          class={`int-small`}
                                                                          name='UsedServiceOther'/>
                                                            </span>
                                                            }
                                                        </span>

                                                    </div>
                                                    :

                                                key.name === 'Transactions' ?
                                                    <div className="flex-item">
                                                        <span>
                                                            <IbxCheckbox label="Stocks" numeric value='1' name='Transactions'/>
                                                        </span>
                                                        <span>
                                                            <IbxCheckbox label="Bonds" numeric value='8' name='Transactions'/>
                                                        </span>
                                                        <span>
                                                            <IbxCheckbox label="Options" numeric value='4' name='Transactions'/>
                                                        </span>
                                                        <span>
                                                            <IbxCheckbox label="Futures" numeric value='2' name='Transactions'/>
                                                        </span>
                                                        <span className="inline-elements">
                                                            <IbxCheckbox label="Other" numeric clearOther='TransactionsOther' value='16' name='Transactions'/>
                                                            {values.Transactions.map(item => item.toString()).indexOf('16') !== -1 &&
                                                            <span className="margin-left-10">
                                                                <IbxInput errors={errors}
                                                                          touched={touched}
                                                                          class={`int-small`}
                                                                          name='TransactionsOther'/>
                                                            </span>
                                                            }
                                                        </span>

                                                    </div>

                                                    :

                                                <IbxInput errors={errors} touched={touched} class={`int-small`} name={key['name']}/>

                                                :

                                                <span>
                                                {
                                                    key.name === 'Transactions' ?
                                                        (convertToText(key, classifications['INDIVIDUALCLIENTS_TRANSACTIONS']) || '-') +' '+values['TransactionsOther']
                                                        :
                                                    key.name === 'UsedServices' ?
                                                        (convertToText(key, classifications['INDIVIDUALCLIENTS_USEDSERVICES']) || '-') +' '+values['UsedServiceOther']
                                                        :

                                                    key['classificator_key'] ?
                                                    getClassificationVal(key) || '-' :
                                                    key.value || '-'
                                                }
                                            </span>
                                        }
                                    </div>
                                </div>
                            })
                            }
                        </div>
                    </div>
                </React.Fragment>
            ))}
            { key_name === 'notes' ? (
                <Comments touched={touched} editMode={editMode} errors={errors} values={values}/>
            ) : key_name === 'documents' && (
                <>
                    <p className="block-title">Documents</p>
                    <IbxUploader name='Filess' accept='.pdf,.png,.jpg,.jpeg,.msg' Files={values.Files} isMultiple/>
                </>
            )}
        </>
    )
})
import React, { Component } from "react";
import getClassicationValues from '../../../../helpers/classifications';
import { Route, Link, withRouter, Switch } from "react-router-dom";

// import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import {fetchingEditableFields} from "../../../../actions";
import { PropsInterface } from "../../../../models/props.inteface";
import Tabs from '../../shared/tabs';
import Content from './helpers/content';
import {keyIsSet} from "../../../../helpers/localhost";
import IbxButton from "../../shared/button";
import {Form, Formik} from "formik";
import axios from "axios";
import {convertToArray} from "../../../../helpers/filters";
import * as Yup from "yup";
import messages from '../../../../helpers/validation-constants';


interface state {
    visible_content: string;
    isLoading: boolean;
    editMode: boolean;
}


class individualCustomer extends Component<PropsInterface & {getData(): Function}, state> {

    constructor(props) {
        super(props);

        if(this.props.error) {
            this.props.history.push('/');
            return;
        }

        this.state = {
            visible_content: 'general',
            isLoading: true,
            editMode: false,
        };

    }

    componentDidMount() {

    }

    setEditable(resetForm: Function | null, editMode: boolean) {
        this.setState({
            editMode,
        });
        if(resetForm) {
            resetForm();
        }
    }

    getInitialValues(fields: []): object {
        let initialValues = {} as any;
        for (let i = 0; i < fields.length; i++) {
            initialValues = {
                ...initialValues,
                [fields[i]]: this.props.individualClientResponse.initialData[fields[i]],
            };
        }
        return initialValues;
    }

    handleSubmit = ({values}) => {
        const {type} = this.props.individualClientResponse.Data.extras;

        if( type === 1) {
            let Transactions = values.Transactions.reduce((all, item) => parseInt(all) + parseInt(item), 0);
            let UsedServices = values.UsedServices.reduce((all, item) => parseInt(all) + parseInt(item), 0);

            values = {...this.props.individualClientResponse.initialData, ...values, Transactions, UsedServices};
        } else {
            values = {...this.props.individualClientResponse.initialData, ...values};
        }
        axios({
            method: 'post',
            url: `${this.props.rootUrl}${type === 1 ?
                '/Client/UpdateIndividualClient' : '/Client/UpdateLegalClient'}`,
            responseType: 'json',
            data: values,
        }).then(response => {
            this.setState({editMode: false});
            this.props.getData();
            alert(`Client ${response.data.Data} Was Successfully Updated!!`);
        }).catch(error => {
            alert(`Something Went Wrong!! Try Again Later!!`);
        });
    };

    Schema = Yup.object().shape({
        AverageMonthlyIncome: Yup.number()
            .typeError(messages.number),
        AverageMonthlyProfit: Yup.number()
            .typeError(messages.number),
        Assets: Yup.number()
            .typeError(messages.number),
    });

    render() {

        const {visible_content, editMode} = this.state;

        const {Data} = this.props.individualClientResponse;

        const editableFields = this.props.editableFields[Data.extras.type === 1 ? 'individual_client' : 'legal_client'];

        let initialValues = {
            ...this.getInitialValues(editableFields),
            Files: Data.documents.value.slice() as any[],// Always pass a copy "slice()" otherwise initial value would change because of the reference
            Comments: Data.notes.value as any[],// No need to pass a copy here, as "Formik" (or any sane form library) does that for us
        } as any;

        let tabs = ['general', 'address & contacts', 'finance & assets', 'documents', 'notes'];

        if (Data.extras.type === 1) {
            tabs.splice(2, 0, 'education & experience');
            initialValues = {
                ...initialValues,
                Transactions: convertToArray(Data["education & experience"]['knowledge and experience']['transactions'].value),
                UsedServices: convertToArray(Data["education & experience"]['knowledge and experience']['used services'].value)
            }
        }

        const setVisibleContent = (setEditable: Function, item: string): void => {
            if(setEditable) {
                setEditable();
            }
            this.setState({
                visible_content: item
            });
        };

        return (
            <div className="tabs">

                <Formik
                    enableReinitialize
                    initialValues={initialValues}
                    validationSchema={this.Schema}

                    onSubmit={(values, { setSubmitting }) => {

                        this.handleSubmit({
                            values,
                            // url: `${rootUrl}/Account/Login`
                        });
                        setSubmitting(false);

                    }}>
                    {({errors, touched, values, handleChange, isSubmitting, handleReset}) => (

                        <>
                            {/*{JSON.stringify(values)}*/}
                            <Tabs tabs={tabs} activeTab={visible_content}
                                  handleClick={setVisibleContent.bind(this, this.setEditable.bind(this, handleReset, false))}/>

                            <Form className='client-profile'>
                                <div className="sub-tabs flex align-items-center justify-flex-end">
                                    <div className="buttons flex align-items-center">
                                        {editMode ?
                                            <>
                                                <IbxButton class="btn-outline" type='button'
                                                           handleClick={this.setEditable.bind(this, handleReset, false)} btnText="Cancel"/>
                                                <IbxButton class = "btn-grey-dark btn-left-icon" btnText="Save" iconClass="icon-save"/>
                                            </>
                                            :
                                            //remove edit button when the client's status is closed
                                            this.props.individualClientResponse['initialData']['Status'] !== 2 &&
                                            <IbxButton class="btn-grey-dark btn-left-icon" type='button'
                                                       handleClick={this.setEditable.bind(this, null, true)} btnText="Edit" iconClass="icon-edit"/>
                                        }
                                    </div>
                                </div>
                                <div className="tab-panel block-container">
                                    <fieldset
                                        disabled={!editMode}
                                        style={{textDecoration: 'none', outline: 'none', border: 'none'}}
                                    >
                                        <Content values={values}
                                                 errors={errors}
                                                 touched={touched}
                                                 editMode={editMode}
                                                 editableFields={editableFields}
                                                 getClassificationVal={this.props.getClassificationVal}
                                                 content={Data[visible_content]}
                                                 key_name={visible_content}/>
                                    </fieldset>
                                </div>
                            </Form>
                        </>
                    )}
                </Formik>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
        individualClientResponse: state.httpRequests.individualClientResponse,
        error: state.httpRequests.error,
        clientFetched: state.httpRequests.clientFetched,
        editableFields: state.httpRequests.editableFields,
    }
};

export default getClassicationValues(connect(mapStateToProps, {fetchingEditableFields})(individualCustomer));
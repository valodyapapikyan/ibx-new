import React, {Component} from "react";
import getClassicationValues from '../../../helpers/classifications';
import { Redirect, Route, Link, NavLink, withRouter, Switch } from "react-router-dom";

// import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import {fetchingData, fetchingEditableFields, getEditableFields, getIndividualCustomer} from "../../../actions";
import { PropsInterface } from "../../../models/props.inteface";
import Profile from "./profile";
import Contracts from "./contracts/index";
import Error from "../../error";
import ContractDetails from './contracts/contract-details';


interface state {
    visible_content: string;
    isLoading: boolean;
    editMode: boolean;
    activeRouteName: string;
}


class individualCustomer extends Component<PropsInterface, state> {

    constructor(props) {
        super(props);

        props.fetchingData();
        props.fetchingEditableFields();

        if(props.match) {
            this.getData();
        }

        props.getEditableFields(`${this.props.rootUrl}/Common/GetEditableFields`);

        if(props.error) {
            props.history.push('/');
            return;
        }

        this.state = {
            visible_content: 'general',
            isLoading: true,
            editMode: false,
            activeRouteName: 'unknown',
        };

    }

    setActive(match) {
        if (!match) {
            return false
        } else {
            this.setState({activeRouteName: match.params.screen});
        }
    }

    clientId = this.props.match.params.clientId;

    getData() {
        if (this.props.location.search.indexOf('Individual') > -1) {
            this.props.getIndividualCustomer(`${this.props.rootUrl}/Client/GetIndividualClient?code=${this.clientId}`)
        } else {
            this.props.getIndividualCustomer(`${this.props.rootUrl}/Client/GetLegalClient?code=${this.clientId}`)
        }
    }

    componentDidMount() {

    }

    render() {

        if(!this.props.clientFetched || !this.props.editableFieldsFetched) return <h2> Loading... </h2>;

        const {Data} = this.props.individualClientResponse;

        const {match} = this.props;
        const {screen} = match.params;
        // console.log(match);
        const getClassificationVal = (obj) => {
            return this.props.getClassificationVal(this.props.classifications[obj['classificator_key']], obj['value']);
        };

        const isActive = false;

        return (
            <>
                <div className="page-top">
                    <Link to="/brokerage/customers" className="green-link with-icon back-link">
                        <i className="icon-arrow-left font-8"></i>
                        <span>Back to Customers</span>
                    </Link>
                    <div className="inner-page-top flex align-items-center">
                        <h2 className="page-title"> { Data.extras.type === 1 ?
                            Data['general']['general']['full name(eng)']['value'] :
                            Data['general']['general']['company name(eng)']['value']
                        }</h2>
                        <i className="icon-dot"></i>
                        <span className="blue">{`${getClassificationVal(Data['general']['other details']['status'])}`}</span>
                    </div>
                    <p className="sub-title">
                        {`Customer code: ${Data['extras']['customer_code']}`} {Data.extras.type === 1 && ` / ${getClassificationVal(Data["education & experience"]['knowledge and experience']['class'])}`}
                    </p>
                </div>
                <div className="inner-container">
                    <div className="row clearfix">

                        <div className="col-9">
                            {screen === 'profile' ?
                                <Profile getClassificationVal={getClassificationVal} getData={this.getData.bind(this)}/>
                                :
                                screen === 'contracts' ?
                                    match.params['contractNumber'] ?
                                        <ContractDetails/>
                                        :
                                    <Contracts clientId={this.clientId} rootUrl={this.props.rootUrl}/>
                                    :
                                <Error/>
                            }
                        </div>
                        <div className="col-3">
                            <ul className="customer-navigation">
                                <li className={isActive ? 'active' : ''}>
                                    <NavLink to={
                                        {
                                            pathname: `/brokerage/customers/${match.params.clientId}/profile`,
                                            search: this.props.location.search,
                                        }
                                    } >
                                        <span className="icon">
                                           <img src={screen === 'profile' ? '/assets/icons/customer_active.svg' : '/assets/icons/customer_inactive.svg'} />
                                        </span>
                                        <span className="text">Customer profile</span>
                                    </NavLink>
                                </li>

                                <li>
                                    <NavLink to={
                                        {
                                            pathname: `/brokerage/customers/${match.params.clientId}/contracts`,
                                            search: this.props.location.search,
                                        }
                                    }>
                                        <span className="icon">
                                           <img src={screen === 'contracts' ? '/assets/icons/contracts_active.svg' : '/assets/icons/contracts_inactive.svg'} />
                                        </span>
                                        <span className="text">Contracts</span>
                                    </NavLink>
                                </li>
                                <li>
                                    <span className="icon">
                                       <img src={isActive ? '/assets/icons/history_active.svg' : '/assets/icons/history_inactive.svg'} />
                                    </span>
                                    <span className="text">Identification & tokens history</span></li>
                                <li>
                                     <span className="icon">
                                       <img src={isActive ? '/assets/icons/transactions_active.svg' : '/assets/icons/transactions_inactive.svg'} />
                                    </span>
                                    <span className="text">Orders, trades & transactions</span></li>
                                <li>
                                     <span className="icon">
                                       <img src={isActive ? '/assets/icons/reports_active.svg' : '/assets/icons/reports_inactive.svg'} />
                                    </span>
                                    <span className="text">Reports & statements</span></li>
                                <li>
                                     <span className="icon">
                                       <img src={isActive ? '/assets/icons/notifications_active.svg' : '/assets/icons/notifications_inactive.svg'} />
                                    </span>
                                    <span className="text">Notifications & account communication</span></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        rootUrl: state.httpRequests.rootUrl,
        individualClientResponse: state.httpRequests.individualClientResponse,
        error: state.httpRequests.error,
        clientFetched: state.httpRequests.clientFetched,
        editableFieldsFetched: state.httpRequests.editableFieldsFetched,
    }
};

export default getClassicationValues(connect(mapStateToProps, {getIndividualCustomer, fetchingData, getEditableFields, fetchingEditableFields})(individualCustomer));
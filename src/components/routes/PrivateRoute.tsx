import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function PrivateRoute({ component: Component, isAuthenticated, ...rest }) {
    // console.log(isAuthenticated, 'isAuthenticated');
    return (
        <Route
            {...rest}
            render={props => {

                return isAuthenticated ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/",
                            state: { from: props.location }
                        }}
                    />
                )
            }}
        />
    );
}

export default PrivateRoute;
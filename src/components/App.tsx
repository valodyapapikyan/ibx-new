import React, { Component } from 'react';
import '../assets/styles/App.scss';
import { connect } from 'react-redux'
import LoginForm from "./site-dev/LoginForm";
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from "react-router-dom";
import PrivateRoute from './routes/PrivateRoute';
import { keyIsSet } from "../helpers/localhost";
import Container from "./brokerage";
import Error from './error';
import { PropsInterface } from "../models/props.inteface";


class App extends Component<PropsInterface> {

    render() {

        return (
            <Router>
                <Switch>

                    <Route path='/' exact component={() => (
                        keyIsSet('token') ? <Redirect to='/brokerage'/> : <Redirect to='/login'/>
                    )}/>

                    <PrivateRoute
                        path='/brokerage'
                        component={ Container }
                        isAuthenticated={this.props.authenticated || keyIsSet('token')}/>

                    <PrivateRoute
                        path='/login'
                        component={LoginForm}
                        isAuthenticated={!keyIsSet('token')}/>

                    <Route path='*' exact component={ Error }/>
                </Switch>
            </Router>
        );
    }
}

const mapStateToProps = (state) => {
    return { authenticated: state.httpRequests.authenticated }
};

export default connect(mapStateToProps, null)(App);
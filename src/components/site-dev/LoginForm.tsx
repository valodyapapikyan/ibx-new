import React, {Component} from 'react';
import {Formik, Form, Field} from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux'
import { loginHandler } from '../../actions'
import {PropsInterface} from "../../models/props.inteface";
import IbxButton from "../brokerage/shared/button";
import IbxInput from "../brokerage/shared/input";

const LoginSchema = Yup.object().shape({
    username: Yup.string()
        .required('This Field Is Required'),
    password: Yup.string()
        .min(2, 'Too Short!')
        .required('This Field Is Required')
});


class LoginForm extends Component<PropsInterface> {

    handleLogin = (obj) => {
        this.props.loginHandler(obj);
    };

    render() {
        return (
            <div className="login-container flex align-items-center">
                <div className="left-part"></div>
                <div className="right-part">
                    <div className="login-inner-container">
                        <div className="logo">
                            <a  href=""><img src="assets/images/logo.png" /> </a>
                        </div>
                        <div>
                            <p className="font-24 mardotoBold blue-dark">Hello!</p>
                            <p className="font-16 grey-middle-light">Sign in with your<br/>credentials.</p>
                        </div>
                        <Formik
                            initialValues={{
                                username: '',
                                password: ''
                            }}
                            validationSchema={LoginSchema}
                            onSubmit={(values , {setErrors, setSubmitting})=> {
                                this.handleLogin({
                                    values,
                                    url: `${this.props.rootUrl}/Account/Login`,
                                    history: this.props.history,
                                    setErrors,
                                    setSubmitting,
                                });
                            }}
                        >
                            {({values, errors, touched, handleChange, isSubmitting}) => (
                                <Form>
                                    <div className="form-group">
                                        <IbxInput value={values.username}
                                                  class={`int-small int-custom ${errors.username && touched.username && 'is-invalid'}`}
                                                  id="formUsername" name="username" placeholder="Name Surname" />
                                    </div>
                                    <div className="form-group">
                                        <IbxInput handleChange={handleChange} class={`int-small int-custom ${errors.password && touched.password && 'is-invalid'}`}
                                                  value={values.password}
                                                  id="formPassword" type="password" name="password" placeholder="Password" />
                                    </div>

                                    <IbxButton class="btn-grey-dark" isDisabled={isSubmitting} btnText="Login"/>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { rootUrl: state.httpRequests.rootUrl }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginHandler: (payload) => dispatch(loginHandler(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
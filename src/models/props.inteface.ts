export interface PropsInterface {
    rootUrl?: any;
    getIndividualCustomer?: any;
    loginHandler?: any;
    history?: any;
    fetchingData(): any;
    fetchingEditableFields(): any;
    getEditableFields(url: string): any;
    editableFields: any;
    contractAccountsFetched: any;
    editableFieldsFetched: boolean;
    clientFetched: boolean;
    screen: string;
    location: any;
    authenticated?: boolean | undefined;
    individualClientResponse?: any;
    error?: any;
    classifications?: any;
    getApplications?: any;
    handleSort: any;
    sortType: string;
    sortBy: string;
    dispatch: any;
    getIndividualApplicant: any;
    getTableData: any;
    individualApplicant: any;
    fetched: boolean;
    getClassificationVal?: any;
    applications: any;
    formSubmit: any;
    handlePageChange: any;
    handlePageSize: any;
    activePage: any;
    Data: any;
    pageSize: any;
    url: any;
    allowEdit: boolean;
    match: {
        path?: string;
        url: string;
        params: {
            clientId?: string;
            screen: string;
        };
    };
}